﻿using System.Collections.Generic;
using System.Data;
using FH.User.Entities;
using Dapper;
using System;

namespace FH.User.Data
{
    public class PropertyLeadDA: IPropertyLeadDA
    {
        private IDbTransaction _transaction;
        public PropertyLeadDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public IEnumerable<PropertyLead> GetAllLead(int propertyId,int userId,DateTime? visitDate=null)
        {            
            try
            {                
                IEnumerable<PropertyLead> lstPropertyLead;
                var param = new DynamicParameters();
                param.Add("@CreatedBy",userId);
                param.Add("@PropertyId",propertyId);
                param.Add("@QueryType", 2);
                if (visitDate !=null && visitDate !=DateTime.MinValue)
                param.Add("@VisitDate", ((DateTime)visitDate).ToString("dd MMM yyyy"));
                
               lstPropertyLead = SqlMapper.Query<PropertyLead>(_transaction.Connection, "usp_my_lead_agent", param, _transaction, commandType: CommandType.StoredProcedure);                
                return lstPropertyLead;
            }
            catch
            {
                return null;
            }
        }
        public bool UpdateScheduleVisit(PropertyLead entity,int userId)
        {
            try
            {                
                var param = new DynamicParameters();
                param.Add("@UserId", userId);
                param.Add("@LeadId",entity.LeadId);
                param.Add("@VisitDate", entity.VisitDate);
                param.Add("@VisitTime", entity.VisitTime);
                string query = "IF EXISTS(SELECT * FROM Property_Master PM WITH(NOLOCK) INNER JOIN Property_Leads PL WITH(NOLOCK) ON PM.PropertyId=PL.PropertyId WHERE PL.LeadId=@LeadId AND PM.CreatedBy=@UserId) ";                
                query += "UPDATE Property_Leads SET VisitDate=@VisitDate,VisitTime=@VisitTime WHERE LeadId=@LeadId";
                int result=SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
                if (result > -1)
                    return true;
                else
                    return false;
            }
            catch(Exception e)
            {
                throw;
            }
        }
        public IEnumerable<PropertyLead> GetAllMyLead(int userId)
        {
            try
            {
                IEnumerable<PropertyLead> lstPropertyLead;
                var param = new DynamicParameters();
                param.Add("@CreatedBy", userId);
                param.Add("@QueryType",5);            
                lstPropertyLead = SqlMapper.Query<PropertyLead>(_transaction.Connection, "usp_my_lead_agent", param, _transaction, commandType: CommandType.StoredProcedure);
                return lstPropertyLead;
            }
            catch
            {
                return null;
            }
        }
        
        public IEnumerable<PropertyLead> GetLeadLog(int agentId,int visitorId)
        {
            try
            {
                IEnumerable<PropertyLead> lstPropertyLead;
                var param = new DynamicParameters();
                param.Add("@CreatedBy", agentId);
                param.Add("@ClientId",visitorId);
                param.Add("@QueryType", 4);
                lstPropertyLead = SqlMapper.Query<PropertyLead>(_transaction.Connection, "usp_my_lead_agent", param, _transaction, commandType: CommandType.StoredProcedure);
                return lstPropertyLead;
            }
            catch
            {
                return null;
            }
        }

        public string AddNewLead(PropertyLead entity)
        {
                var param = new DynamicParameters();
                param.Add("@Name", entity.VisitorName);
                param.Add("@Mobile", entity.VisitorMobile);
                param.Add("@Email", entity.VisitorEmail);
            param.Add("@ClientRequirement", entity.VisitorRequirement);
                param.Add("@CreatedBy", entity.UserId);
                param.Add("@QueryType", 1);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_my_lead_agent", param, _transaction, commandType: CommandType.StoredProcedure);                   
        }        
        
    }

    public interface IPropertyLeadDA
    {
        IEnumerable<PropertyLead> GetAllLead(int propertyId, int userId, DateTime? visitDate=null);
        bool UpdateScheduleVisit(PropertyLead entity, int userId);
        IEnumerable<PropertyLead> GetAllMyLead(int userId);
        IEnumerable<PropertyLead> GetLeadLog(int agentId, int visitorId);
        string AddNewLead(PropertyLead entity);
    }
}
