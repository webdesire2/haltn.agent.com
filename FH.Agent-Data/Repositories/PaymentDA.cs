﻿
using Dapper;
using FH.User.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FH.User.Data.Repositories
{
    public class PaymentDA
    {
        string _connectionString = string.Empty;

        public PaymentDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }


     

        public IEnumerable<T> Execute<T>(PlanMaster de, PaymentCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_plan_mgmt", PaymentUpdate(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }




        public IEnumerable<T> Execute<T>(PaymentDE de, PaymentCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_payment", PaymentParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }


        public IEnumerable<T> Execute<T>(PaymentDetail de, PaymentCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_payment", UpdatePaymentUser(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        private DynamicParameters PaymentUpdate(PlanMaster de, PaymentCallValue callValue)
        { 
            var param = new DynamicParameters();
            param.Add("@PlanOrderID", de.PlanOrderID);
            param.Add("@Status", de.Status);
            param.Add("@RorderId", de.RorderId);
            param.Add("@RPaymentId", de.RPaymentId);
            param.Add("@Payment", de.Payment);
            param.Add("@PaymentMode", de.PaymentMode);
            param.Add("@StatusCode", de.StatusCode);
            param.Add("@StatusMessage", de.StatusMessage);
            param.Add("@QueryType", (int)callValue);
            return param; 
        }

        public DynamicParameters UpdatePaymentUser(PaymentDetail de, PaymentCallValue callValue) 
        {
            var param = new DynamicParameters();
            param.Add("@PaymentId", de.PaymentId);
            param.Add("@Status", de.Status);
            param.Add("@TracingId", de.TracingId);
            param.Add("@BankRefNo", de.BankRefNo);
            param.Add("@Amount", de.Amount);
            param.Add("@PaymentMode", de.PaymentMode);
            param.Add("@StatusCode", de.StatusCode);
            param.Add("@StatusMessage", de.StatusMessage);
            param.Add("@QueryType", (int)callValue);
            return param;
        }
        private DynamicParameters PaymentParam(PaymentDE de,PaymentCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", de.UserId);
            param.Add("@TenantId", de.TenantId);
            param.Add("@FromDate", de.FromDate);
            param.Add("@ToDate", de.ToDate); 
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@PaymentId", de.PaymentId);
            param.Add("@QueryType", (int)callValue);
            return param;
        }
    }

    public enum PaymentCallValue
    {
        GetCurrentMonthReceivedPayment=3, 
        GetCurrentMonthReceivedPaymentTotal=4,
        UpdatePayment = 14,
        GetTenantInvoice = 2,
        GetCurrentMonthReceivedPaymentPen = 5,
        UpdatePaymentUser = 1,
    }
}
 