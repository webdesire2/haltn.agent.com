﻿using FH.User.Entities;
using System.Data;
using Dapper;
using System.Collections.Generic;

namespace FH.User.Data
{
    public class AmenitiesMasterDA : IAmenitiesMasterDA
    {
        IDbTransaction _transaction;
        public AmenitiesMasterDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public IEnumerable<AmenityMaster> GetAll(string amenityType=null)
        {
            if(amenityType=="PG")
                return SqlMapper.Query<AmenityMaster>(_transaction.Connection, "SELECT * FROM Amenity_Master WHERE AmenityType IN ('PG','PGRoom')", null, _transaction);
            else
                return SqlMapper.Query<AmenityMaster>(_transaction.Connection, "SELECT * FROM Amenity_Master WHERE AmenityType IS NULL", null, _transaction);

        }
    }

    public interface IAmenitiesMasterDA
    {
        IEnumerable<AmenityMaster> GetAll(string amenityType = null);
    }
}
