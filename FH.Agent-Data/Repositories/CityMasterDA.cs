﻿using System.Data;
using FH.User.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;

namespace FH.User.Data
{
    public class CityMasterDA
    {
        string _connectionString = string.Empty;
        public CityMasterDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(CityMasterCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_city_master",CityMasterParam(callValue), commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        private DynamicParameters CityMasterParam(CityMasterCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@CallValue",(int)callValue);
            return param;
        }
    }

    public enum CityMasterCallValue
    {       
        GetAllCity =1
    }
}
