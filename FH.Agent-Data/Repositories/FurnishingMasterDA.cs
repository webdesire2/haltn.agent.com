﻿using FH.User.Entities;
using System.Data;
using Dapper;
using System.Collections.Generic;

namespace FH.User.Data
{
   public class FurnishingMasterDA:IFurnishingMasterDA
    {
        IDbTransaction _transaction;
        public FurnishingMasterDA(IDbTransaction transaction) 
        {
            _transaction=transaction;
        }

        public IEnumerable<FurnishingMaster> GetAll()
        {
           return SqlMapper.Query<FurnishingMaster>(_transaction.Connection, "SELECT * FROM Furnishing_Master", null, _transaction);
        }
    }

    public interface IFurnishingMasterDA
    {
        IEnumerable<FurnishingMaster> GetAll();
    }
}
