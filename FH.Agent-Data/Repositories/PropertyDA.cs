﻿using Dapper;
using FH.User.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FH.User.Data
{
    public class PropertyDA : GenericRepository<PropertyMaster>, IPropertyDA
    {
        private IDbTransaction _transaction;
        public PropertyDA(IDbTransaction transaction) : base(transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, int UserId, out int totalRecord)
        {
            totalRecord = 0;
            try
            {
                IEnumerable<PropertyList> lstProperty;

                var param = new DynamicParameters();
                param.Add("@UserId",UserId);
                    param.Add("@PageIndex", input.PageIndex);
                    param.Add("@PageSize", input.PageSize);
                param.Add("@PropertyStatus", input.PropertyStatus);
                    param.Add("@TotalRecord", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    lstProperty = SqlMapper.Query<PropertyList>(_transaction.Connection, "usp_GetAllPropertyByAgent", param, _transaction, commandType: CommandType.StoredProcedure);
                    totalRecord = param.Get<int>("@TotalRecord");
                
                return lstProperty;
            }
            catch(Exception e)
            {
                return null;
            }            
        }
        public Property GetProperty(int propertyId,int UserId)
        {
            Property property = new Property();

            var param = new DynamicParameters();
            param.Add("@PropertyId", propertyId);
            param.Add("@AgentId",UserId);

            using (var multiRecord = SqlMapper.QueryMultiple(_transaction.Connection, "usp_GetPropertyById", param, _transaction, commandType: CommandType.StoredProcedure))
            {
                string propertyType=multiRecord.ReadSingleOrDefault<string>();
               
                if(propertyType=="FLAT" || propertyType=="ROOM" || propertyType=="FLATMATE")
                {
                    PropertyFlatRoom propertyFlatRoom = new PropertyFlatRoom();
                     propertyFlatRoom.PropertyMaster= multiRecord.ReadSingleOrDefault<PropertyMaster>();
                     propertyFlatRoom.PropertyDetail= multiRecord.ReadSingleOrDefault<PropertyDetailFlatRoom>();
                     propertyFlatRoom.PropertyDetail.FurnishingDetails= multiRecord.Read<PropertyFurnishingDetails>();
                     propertyFlatRoom.AmenityDetails= multiRecord.Read<PropertyAmenitiesDetails>();
                     propertyFlatRoom.GalleryDetails = multiRecord.Read<PropertyGalleryDetails>();                    
                    property.FurnishMaster = multiRecord.Read<FurnishingMaster>();
                     property.AmenityMaster = multiRecord.Read<AmenityMaster>();
                    property.PropertyFlatRoomFlatmate = propertyFlatRoom;
                }

                if (propertyType=="PG")
                {
                    PropertyPG propertyPG = new PropertyPG();
                    propertyPG.PropertyMaster = multiRecord.ReadSingleOrDefault<PropertyMaster>();
                    propertyPG.PgDetail = multiRecord.ReadSingleOrDefault<PGDetail>();
                    propertyPG.RoomRentalDetail = multiRecord.Read<PGRoomRentalDetail>().AsList();
                    propertyPG.RoomAmenities = multiRecord.Read<PGRoomAmenity>();
                    propertyPG.pgRules = multiRecord.Read<PGRule>();
                    propertyPG.AmenityDetails = multiRecord.Read<PropertyAmenitiesDetails>();
                    propertyPG.GalleryDetails = multiRecord.Read<PropertyGalleryDetails>();
                    propertyPG.PGRoomData = multiRecord.Read<PGRoomData>();
                    propertyPG.UAccount = multiRecord.Read<UAccount>();
                    property.PropertyPG = propertyPG;

                    property.AmenityMaster = multiRecord.Read<AmenityMaster>();
                }
            }

            return property;
        }
        public int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity)
        {
            try
            {
                var param = new DynamicParameters();                
                param.Add("@PropertyType",entity.PropertyMaster.PropertyType);               
                param.Add("@CreatedBy",entity.PropertyMaster.CreatedBy);
                param.Add("@City", entity.PropertyMaster.City);
                param.Add("@Locality", entity.PropertyMaster.Locality);
                param.Add("@Street", entity.PropertyMaster.Street);
                param.Add("@Latitude", entity.PropertyMaster.Latitude);
                param.Add("@Longitude", entity.PropertyMaster.Longitude);
                param.Add("@ContactPersonName", entity.PropertyMaster.ContactPersonName);
                param.Add("@ContactPersonMobile", entity.PropertyMaster.ContactPersonMobile);

                param.Add("@ApartmentType",entity.PropertyDetail.ApartmentType);
                param.Add("@ApartmentName", entity.PropertyDetail.ApartmentName);
                param.Add("@BhkType", entity.PropertyDetail.BhkType);
                param.Add("@RoomType", entity.PropertyDetail.RoomType);
                param.Add("@TenantGender", entity.PropertyDetail.TenantGender);
                param.Add("@PropertySize",entity.PropertyDetail.PropertySize);
                param.Add("@Facing", entity.PropertyDetail.Facing);
                param.Add("@PropertyAge",entity.PropertyDetail.PropertyAge);
                param.Add("@FloorNo", entity.PropertyDetail.FloorNo);
                param.Add("@TotalFloor", entity.PropertyDetail.TotalFloor);
                param.Add("@BathRooms", entity.PropertyDetail.BathRooms);
                param.Add("@Balconies", entity.PropertyDetail.Balconies);
                param.Add("@WaterSupply", entity.PropertyDetail.WaterSupply);
                param.Add("@GateSecurity", entity.PropertyDetail.GateSecurity);
                param.Add("@IsAgentAllowed", entity.PropertyDetail.IsAgentAllowed);
                /*******Rental detail ********/
                param.Add("@ExpectedRent", entity.PropertyDetail.ExpectedRent);
                param.Add("@ExpectedDeposit", entity.PropertyDetail.ExpectedDeposit);
                param.Add("@IsRentNegotiable", entity.PropertyDetail.IsRentNegotiable);
                param.Add("@AvailableFrom", entity.PropertyDetail.AvailableFrom);
                param.Add("@PreferredTenant", entity.PropertyDetail.PreferredTenant);
                param.Add("@Parking", entity.PropertyDetail.Parking);
                param.Add("@Furnishing", entity.PropertyDetail.Furnishing);
                param.Add("@Description", entity.PropertyDetail.Description);

                if (entity.PropertyDetail.FurnishingDetails != null)
                {
                    DataTable dtFurnishingDetail = Convert.ToDataTable<PropertyFurnishingDetails>(entity.PropertyDetail.FurnishingDetails);
                    param.Add("@PropertyFurnishingDetail", dtFurnishingDetail.AsTableValuedParameter("PropertyFurnishingDetails"));
                }

                if (entity.GalleryDetails != null)
                {                    
                    DataTable dtGallery = Convert.ToDataTable<PropertyGalleryDetails>(entity.GalleryDetails);
                    dtGallery.Columns.Remove("PropertyGalleryId");                    
                    param.Add("@GalleryDetails", dtGallery.AsTableValuedParameter("GalleryDetails"));
                }

                if (entity.AmenityDetails != null)
                {
                    DataTable dtAmenity = Convert.ToDataTable<PropertyAmenitiesDetails>(entity.AmenityDetails);
                    param.Add("@PropertyAmenityDetails", dtAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.ExecuteScalar<int>(_transaction.Connection, "usp_InsertPropertyFlatRoomFlatMate", param, _transaction, commandType: CommandType.StoredProcedure);               
            }
            catch(Exception e)
            {
                throw;
            }
        }
        public int AddPropertyPG(PropertyPG entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyType","PG");
                param.Add("@CreatedBy", entity.PropertyMaster.CreatedBy);
                param.Add("@PropertyName", entity.PropertyMaster.PropertyName);
                param.Add("@ContactPersonName", entity.PropertyMaster.ContactPersonName);
                param.Add("@ContactPersonMoible", entity.PropertyMaster.ContactPersonMobile);
                param.Add("@City", entity.PropertyMaster.City);
                param.Add("@Locality", entity.PropertyMaster.Locality);
                param.Add("@Street", entity.PropertyMaster.Street);
                param.Add("@Latitude", entity.PropertyMaster.Latitude);
                param.Add("@Longitude", entity.PropertyMaster.Longitude);
                 
                /********PG Room Detail **********/
                if (entity.RoomRentalDetail != null)
                {
                    DataTable dtRoomRental = Convert.ToDataTable<PGRoomRentalDetail>(entity.RoomRentalDetail);
                    param.Add("@RoomRentalDetail", dtRoomRental.AsTableValuedParameter("PGRoomDetails1"));
                }

                if(entity.RoomAmenities!=null)
                {
                    DataTable dtRoomAmenity = Convert.ToDataTable<PGRoomAmenity>(entity.RoomAmenities);
                    param.Add("@RoomAmenity", dtRoomAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }
                
                /*******Rental detail ********/                
                param.Add("@AvailableFrom", entity.PgDetail.AvailableFrom);
                param.Add("@TenantGender", entity.PgDetail.TenantGender);
                param.Add("@PreferredGuest", entity.PgDetail.PreferredGuest);
                param.Add("@Fooding", entity.PgDetail.Fooding);
                param.Add("@Laundry", entity.PgDetail.Laundry);
                param.Add("@RoomCleaning", entity.PgDetail.RoomCleaning);                
                param.Add("@IsAgentAllowed", entity.PgDetail.IsAgentAllowed);
                
                if(entity.pgRules!=null)
                {
                    DataTable dtRules = Convert.ToDataTable<PGRule>(entity.pgRules);
                    param.Add("@PGRules", dtRules.AsTableValuedParameter("PGRules"));
                }

                if (entity.GalleryDetails != null)
                {
                    DataTable dtGallery = Convert.ToDataTable<PropertyGalleryDetails>(entity.GalleryDetails);
                    dtGallery.Columns.Remove("PropertyGalleryId");
                    param.Add("@GalleryDetails", dtGallery.AsTableValuedParameter("GalleryDetails"));
                }

                if (entity.AmenityDetails != null)
                {
                    DataTable dtAmenity = Convert.ToDataTable<PropertyAmenitiesDetails>(entity.AmenityDetails);
                    param.Add("@PropertyAmenityDetails", dtAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.ExecuteScalar<int>(_transaction.Connection, "usp_InsertPropertyPG", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch(Exception e)
            {
                throw;
            }
        } 
        
        public string UpdatePropertyDetail(int propertyId,string propertyType,int UserId,PropertyDetailFlatRoom entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId", UserId);
                param.Add("@PropertyType", propertyType);
                param.Add("@ApartmentType", entity.ApartmentType);
                param.Add("@ApartmentName", entity.ApartmentName);
                param.Add("@BhkType", entity.BhkType);
                param.Add("@RoomType", entity.RoomType);
                param.Add("@TenantGender", entity.TenantGender);
                param.Add("@PropertySize", entity.PropertySize);
                param.Add("@Facing", entity.Facing);
                param.Add("@PropertyAge", entity.PropertyAge);
                param.Add("@FloorNo", entity.FloorNo);
                param.Add("@TotalFloor", entity.TotalFloor);
                param.Add("@BathRooms",(entity.BathRooms<=0?null:entity.BathRooms.ToString()));
                param.Add("@Balconies", (entity.Balconies <= 0 ? null : entity.Balconies.ToString()));
                param.Add("@WaterSupply", entity.WaterSupply);
                param.Add("@GateSecurity", entity.GateSecurity);
                param.Add("@IsAgentAllowed", entity.IsAgentAllowed);

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_Property_detail_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
                   
            }
            catch
            {
                throw;
            }
        }
        public string UpdateLocalityDetail(int propertyId, int UserId, PropertyMaster entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId", UserId);
                param.Add("@ContactPersonName", entity.ContactPersonName);
                param.Add("@ContactPersonMobile", entity.ContactPersonMobile);
                param.Add("@City", entity.City);
                param.Add("@Locality", entity.Locality);
                param.Add("@Street", entity.Street);
                param.Add("@Latitude", entity.Latitude);
                param.Add("@Longitude", entity.Longitude);
                param.Add("@PropertyName", entity.PropertyName);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_locality_detail_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string UpdateRentalDetail(int propertyId, string propertyType, int UserId, PropertyDetailFlatRoom entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId", UserId);
                param.Add("@PropertyType",propertyType);
                param.Add("@ExpectedRent", entity.ExpectedRent);
                param.Add("@ExpectedDeposit", entity.ExpectedDeposit);
                param.Add("@AvailableFrom", entity.AvailableFrom);
                param.Add("@RentNegotiable", entity.IsRentNegotiable);
                param.Add("@PreferredTenant", entity.PreferredTenant);
                param.Add("@Parking", entity.Parking);
                param.Add("@Furnish", entity.Furnishing);
                param.Add("@Description", entity.Description);
                if (entity.FurnishingDetails != null)
                {
                    DataTable dtFurnishingDetail = Convert.ToDataTable<PropertyFurnishingDetails>(entity.FurnishingDetails);
                    param.Add("@PropertyFurnishingDetail", dtFurnishingDetail.AsTableValuedParameter("PropertyFurnishingDetails"));
                }
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_rental_detail_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string AddNewGallery(int propertyId, int UserId, string file)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId",UserId);
                param.Add("@FilePath", file);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_add_gallery_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch(Exception e)
            {
                throw;
            }
        }
        public string DeleteGallery(int propertyId,int UserId,int imageId)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId", UserId);
                param.Add("@ImageId",imageId);
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_delete_gallery_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string UpdatePropertyAmenity(int propertyId,int UserId,IEnumerable<PropertyAmenitiesDetails> entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId", UserId);
                if (entity != null)
                {
                    DataTable dtAmenity = Convert.ToDataTable<PropertyAmenitiesDetails>(entity);
                    param.Add("@PropertyAmenityDetails", dtAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_amenity_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }
        }
        public string EditRoom(PGRoomData PGRoomData)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PGRoomInventoryId", PGRoomData.PGRoomInventoryId);
                param.Add("@AccountId", PGRoomData.AccountId);
                param.Add("@RoomType", PGRoomData.RoomType);
                param.Add("@FloorNo", PGRoomData.FloorNo);
                param.Add("@QueryType", 1);

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_roomdetails", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }

        }

        public string AddRoom(PGRoomData PGRoomData)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@RoomNo", PGRoomData.RoomNo);
                param.Add("@PropertyId", PGRoomData.PropertyId);
                param.Add("@AccountId", PGRoomData.AccountId);
                param.Add("@RoomType", PGRoomData.RoomType);
                param.Add("@FloorNo", PGRoomData.FloorNo);
                param.Add("@QueryType", 2);

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_roomdetails", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }

        }

        public string UpdatePGRoomDetail(int propertyId,int UserId,IEnumerable<PGRoomRentalDetail> roomRentalDetail,IEnumerable<PGRoomAmenity> roomAmenities)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId", propertyId);
                param.Add("@AgentId",UserId);

                if (roomRentalDetail != null)
                {
                    DataTable dtRoomRental = Convert.ToDataTable<PGRoomRentalDetail>(roomRentalDetail);
                    param.Add("@RoomRentalDetail", dtRoomRental.AsTableValuedParameter("PGRoomDetails1"));
                }

                if (roomAmenities != null)
                {
                    DataTable dtRoomAmenity = Convert.ToDataTable<PGRoomAmenity>(roomAmenities);
                    param.Add("@RoomAmenity", dtRoomAmenity.AsTableValuedParameter("PropertyAmenityDetails"));
                }

                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_pg_room_detail_Agent", param, _transaction, commandType: CommandType.StoredProcedure);
            }
            catch
            {
                throw;
            }

        }
        public string UpdatePGDetail(int propertyId,int UserId,PGDetail pgDetail,IEnumerable<PGRule> pgRules)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@PropertyId",propertyId);
                param.Add("@AgentId", UserId);
                param.Add("@AvailableFrom",pgDetail.AvailableFrom);
                param.Add("@TenantGender",pgDetail.TenantGender);
                param.Add("@PreferredGuest",pgDetail.PreferredGuest);
                param.Add("@Fooding", pgDetail.Fooding);
                param.Add("@Laundry", pgDetail.Laundry);
                param.Add("@RoomCleaning",pgDetail.RoomCleaning);
                param.Add("@IsAgentAllowed", pgDetail.IsAgentAllowed);
                if (pgRules != null)
                {
                    DataTable dtRules = Convert.ToDataTable<PGRule>(pgRules);
                    param.Add("@PGRules", dtRules.AsTableValuedParameter("PGRules"));
                }
                return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection, "usp_update_pg_detail_Agent", param, _transaction, commandType: CommandType.StoredProcedure);

            }
            catch
            {
                throw;
            }
        }
        public IEnumerable<PropertyList> MyPropertyVisit(int userId)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", userId);
            return SqlMapper.Query<PropertyList>(_transaction.Connection, "usp_GetPropertyByVisitDate", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public string GetPropertyStatus(int propertyId)
        {
            var param = new DynamicParameters();
            param.Add("PropertyId", propertyId);
            string query = "SELECT PropertyStatusByFH FROM Property_Master WHERE PropertyId=@PropertyId";  
            return SqlMapper.QueryFirstOrDefault<string>(_transaction.Connection,query, param, _transaction, commandType: CommandType.Text);
        }        

        public bool UpdatePropertyStatus(PropertyMaster de)
        {
            var param = new DynamicParameters();
            param.Add("@PropertyId",de.PropertyId);
            param.Add("@Status",de.PropertyStatusByFH);
            param.Add("@CreatedBy", de.CreatedBy);
            string query = "UPDATE Property_Master SET PropertyStatusByFH=@Status WHERE PropertyId=@PropertyId AND CreatedBy=@CreatedBy";
             int result=SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            if (result > 0)
                return true;
            return false;
        }
    }

    public interface IPropertyDA
    {       
        Property GetProperty(int propertyId, int UserId);
        int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity);
        int AddPropertyPG(PropertyPG entity);
        string UpdatePropertyDetail(int propertyId, string propertyType, int UserId, PropertyDetailFlatRoom entity);
        string UpdateLocalityDetail(int propertyId, int UserId, PropertyMaster entity);
        string UpdateRentalDetail(int propertyId,string propertyType,int UserId, PropertyDetailFlatRoom entity);
        string AddNewGallery(int propertyId, int UserId, string file);
        string DeleteGallery(int propertyId, int UserId, int imageId);
        string UpdatePropertyAmenity(int propertyId, int UserId, IEnumerable<PropertyAmenitiesDetails> entity);
        string UpdatePGRoomDetail(int propertyId, int UserId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities);
        string EditRoom(PGRoomData PGRoomData);
        string AddRoom(PGRoomData PGRoomData);
        string UpdatePGDetail(int propertyId, int UserId, PGDetail pgDetail, IEnumerable<PGRule> pgRules);
        IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, int UserId, out int totalRecord);
        IEnumerable<PropertyList> MyPropertyVisit(int userId);

        string GetPropertyStatus(int propertyId);
        bool UpdatePropertyStatus(PropertyMaster de);
    }
}