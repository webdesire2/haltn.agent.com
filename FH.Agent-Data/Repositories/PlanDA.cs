﻿using System.Data;
using FH.User.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;
using System.Linq;

namespace FH.User.Data
{
    public class PlanDA
    {
        string _connectionString = string.Empty;
        public PlanDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(PlanMaster de, PlanCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_plan_mgmt", TenantParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(PlanMaster de, PlanCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result = con.QueryFirstOrDefault<T>("udsp_plan_mgmt", TenantParam(de, param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                }
            }
        }

        private DynamicParameters TenantParam(PlanMaster de, PlanCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@PlanId", de.PlanId);
            param.Add("@Name", de.Name);
            param.Add("@Description", de.Description);
            param.Add("@Price", de.Price);
            param.Add("@Discount", de.Discount);
            param.Add("@Validity", de.Validity);
            param.Add("@NumberOfLeads", de.NumberOfLeads);
            param.Add("@Status", de.Status);
            param.Add("@VarifiedTag", de.VarifiedTag);
            param.Add("@Category", de.Category);
            param.Add("@Color", de.Color);
            param.Add("@SEOData", de.SEOData);
            param.Add("@CreatedBy", de.CreatedBy);
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@Payment", de.Payment);
            param.Add("@PlanOrderID", de.PlanOrderID);
            param.Add("@QueryType", (int)callValue);

            return param;
        }
    }

    public enum PlanCallValue
    {
        GetMyPlans = 6,
        GetPackages = 7,
        GetProperty = 8,
        AddPlanToVendor = 9,
        AddPlanOrder = 10,
        GetMyOder = 11,
        GetMyPropertyNotByPackage = 12,
        FetchOrder = 13,
        DeleteOrder = 16
    }
}
