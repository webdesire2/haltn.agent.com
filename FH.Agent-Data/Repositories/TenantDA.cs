﻿using System.Data;
using FH.User.Entities;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System;
using System.Linq;

namespace FH.User.Data
{
    public class TenantDA
    {
        string _connectionString = string.Empty;
        public TenantDA()
        {
            _connectionString =ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(TenantMaster de,TenantCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();                
                try
                {
                   var result=con.Query<T>("udsp_tenent_mgmt", TenantParam(de,callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }                
                finally 
                {  
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(TenantMaster de,TenantCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result=con.QueryFirstOrDefault<T>("udsp_tenent_mgmt", TenantParam(de,param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch(Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                } 
            }
        }

        private DynamicParameters TenantParam(TenantMaster de,TenantCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@TenantId", de.TenantId);
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@Name",de.Name);
            param.Add("@Mobile", de.Mobile);
            param.Add("@Email", de.Email);            
            param.Add("@SecurityAmount", de.SecurityAmount);
            param.Add("@MonthlyRent", de.MonthlyRent);
            param.Add("@SecurityDeposit", de.SecurityDeposit);
            param.Add("@CheckInDate", de.CheckInDate);
            param.Add("@CheckOutDate", de.CheckOutDate);
            param.Add("@CreatedBy", de.CreatedBy); 
            param.Add("@RoomNo", de.RoomNo);
            param.Add("@RoomId", de.RoomId);
            param.Add("@Occupancy", de.Occupancy);
            param.Add("@IsActive", de.IsActive);
            param.Add("@IsActive1", de.IsActive1);
            
            param.Add("@QueryType", (int)callValue);

            param.Add("@Payment", de.Payment);
            param.Add("@UserId", de.UserId);
            param.Add("@UserType", de.UserType);
            param.Add("@PaymentType", de.PaymentType);
            param.Add("@RentForMonthStartDate", de.RentForMonthStartDate);
            param.Add("@RentForMonthEndDate", de.RentForMonthEndDate);
            param.Add("@StatusMessage", de.StatusMessage);

            if (de.PaymentDetails!=null)
            {
                var paymentDetail = de.PaymentDetails.FirstOrDefault();
                param.Add("@PaymentId", paymentDetail.PaymentId);
                param.Add("@Amount", paymentDetail.Amount);
                param.Add("@Status", paymentDetail.Status);                
                param.Add("@PaymentMode", paymentDetail.PaymentMode);
                
                // DataTable dtPaymentDetail = Convert.ToDataTable<PaymentDetail>(de.PaymentDetails);
                //param.Add("@PaymentDetail", dtPaymentDetail.AsTableValuedParameter("PaymentDetail"));
            }

            if(de.TenantPaymentDetails!=null)
            {
                var tenantPayment = de.TenantPaymentDetails.FirstOrDefault();
                param.Add("@DepositAmount", tenantPayment.DepositAmount);
                param.Add("@PaymentType", tenantPayment.PaymentType);
                param.Add("@RentForMonthStartDate", tenantPayment.RentForMonthStartDate);
                param.Add("@RentForMonthEndDate", tenantPayment.RentForMonthEndDate);
                param.Add("@PaymentStatus", tenantPayment.PaymentStatus);
                //DataTable dtTenantPaymentDetail = Convert.ToDataTable<TenantPaymentDetail>(de.TenantPaymentDetails);
                //param.Add("@TenantPaymentDetail", dtTenantPaymentDetail.AsTableValuedParameter("TenantPaymentDetail"));
            } 

            return param;
        }       
    }

    public enum TenantCallValue
    {
        AddTenant=1,
        GetAllTenant=33,
        GetTenantPaymentLog=3,
        GetSingleTenant = 17,
        EditTenant = 13,
        GetTanentStayDetails = 15,
        CollectPayment = 16,
        CollectSecurity = 18,
        RefundSecurity=22,
        GetRentPaidTanent =19,
        RSendPaymentLink=20,
        SSendPaymentLink= 20,
        getAvailableRooms = 28,
        getTenantRooms = 29,
        GetRentPaidTanentTest = 34,
        getMyProperties = 36,
        GetAllTenantDetails = 37,
        GetMyTeam = 42,
        UpdateMyTeam = 43,
        AddMyTeam=44,
    }
}
