﻿using Dapper;
using FH.User.Entities;
using System;
using System.Data;

namespace FH.User.Data
{
    public class UserDA : GenericRepository<UserDetail>, IUserDA
    {
        IDbTransaction _transaction;
        public UserDA(IDbTransaction transaction) : base(transaction)
        {
            _transaction = transaction;
        }
        public UserDetail FindByEmailMobile(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 1);
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile);
            var data = Get("Usp_UserDetails", param);
            return data;
        }
        public string GetUserByMobile(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 8);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@OTP", entity.OTP);
            param.Add("@OTPExpiredOn", entity.OTPExpiredOn);

            var data = Get("Usp_UserDetails", param);
            if (data != null)
                return data.UserId;

            return string.Empty;
        }
        public int AddUser(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 2);
            param.Add("@Name", entity.Name);
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Password", entity.Password);
            param.Add("@OTP", entity.OTP);
            param.Add("@OTPExpiredOn", entity.OTPExpiredOn);
            param.Add("@UserType", entity.UserType);
            return SqlMapper.QueryFirstOrDefault<int>(_transaction.Connection, "usp_UserDetails", param, _transaction, commandType: CommandType.StoredProcedure);
        }
        public int UpdateUser(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 14);
            param.Add("@Name", entity.Name);
            param.Add("@UserId", Int32.Parse(entity.UserId));
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile); 
            param.Add("@Password", entity.Password);
            param.Add("@OTP", entity.OTP);
            param.Add("@OTPExpiredOn", entity.OTPExpiredOn);
            param.Add("@UserType", entity.UserType);
            return SqlMapper.QueryFirstOrDefault<int>(_transaction.Connection, "usp_UserDetails", param, _transaction, commandType: CommandType.StoredProcedure);
        }
        public UserDetail IsValidLogin(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 3);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Password", entity.Password);
            var data = Get("usp_UserDetails", param);
            return data;
        }
        public UserDetail ResetPassword(UserDetail user)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 6);
            param.Add("@UserId", user.UserId);
            param.Add("@Password", user.Password);
            param.Add("@OTP", user.OTP);
            param.Add("@OTPExpiredOn", user.OTPExpiredOn);

            return SqlMapper.QueryFirstOrDefault<UserDetail>(_transaction.Connection, "usp_UserDetails", param, _transaction, commandType: CommandType.StoredProcedure);
        }
        public UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 7);
            param.Add("@UserId", userId);
            param.Add("@OTP", otp);
            param.Add("@OTPExpiredOn", otpExpiredOn.ToString("yyyy-MM-dd HH:mm:ss"));
            var data = Get("usp_UserDetails", param);
            return data;
        }
        
        //public UserDetail GetUserByUserId(int userId)
        //{
        //    var param = new DynamicParameters();
        //    param.Add("@UserId", userId);
        //    string query = "SELECT UserId,Name,Email,Mobile,UserType FROM User_Details WITH(NOLOCK) WHERE UserId=@UserId";
        //    return SqlMapper.QueryFirstOrDefault<UserDetail>(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
        //}

       
    }


    public interface IUserDA
    {
        UserDetail FindByEmailMobile(UserDetail entity);
        int AddUser(UserDetail entity);
        int UpdateUser(UserDetail entity);
        UserDetail IsValidLogin(UserDetail entity);
        UserDetail ResetPassword(UserDetail user);
        UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn);
        string GetUserByMobile(UserDetail entity);
        // UserDetail GetUserByUserId(int userId);
        
    }
}
