﻿using Dapper;
using FH.User.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace FH.User.Data
{
    public class UserProfileDA:IUserProfileDA
    {
        IDbTransaction _transaction;
        public UserProfileDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public UserProfile GetUserDetail(int userId)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", userId);
            string query = "SELECT UD.UserId,UD.Name UserName,UD.Email UserEmail,UD.Mobile UserMobile,BD.BankAccountHolderName,BD.BankAccountNo,BD.BankIFSCCode,BD.CompanyName,BD.OwnerName,BD.OfficeAddress,BD.OfficialEmail,BD.GSTNo,BD.PANNo FROM User_Details UD WITH(NOLOCK) LEFT JOIN Dealer_Business_Detail BD WITH(NOLOCK) ON UD.UserId=BD.UserId WHERE UD.UserId=@UserId";
            return SqlMapper.QueryFirstOrDefault<UserProfile>(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
        }

        public IEnumerable<UAccount> GetUserAccount(int userId)
        {
            var param = new DynamicParameters();
            param.Add("@UserId", userId);
            string query = "SELECT * FROM [dbo].[UAccount] WHERE UserId =@UserId ORDER BY AccountId DESC";
            return SqlMapper.Query<UAccount>(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
        }
        public bool UpdatePersonalDetail(UserProfile entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@UserId", entity.UserId);
                param.Add("@Name", entity.UserName);
                param.Add("@Email", entity.UserEmail);
                string query = "UPDATE User_Details SET Name=@Name,Email=@Email WHERE UserId=@UserId";
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
                return true;
            }
            catch
            {
                throw;
            }
        }
        
        public bool UpdateBusinessDetail(UserProfile entity)
        {
            try
            {
                string query = "IF EXISTS(SELECT UserId FROM Dealer_Business_Detail WHERE UserId=@UserId) ";
                query += "UPDATE Dealer_Business_Detail SET CompanyName=@CompanyName,OwnerName=@OwnerName,OfficialEmail=@OfficeMail,OfficeAddress=@OfficeAddress,GSTNo=@GSTNo,PANNo=@PANNo WHERE UserId=@UserId ";
                query += "ELSE INSERT INTO Dealer_Business_Detail(UserId,CompanyName,OwnerName,OfficialEmail,OfficeAddress,GSTNo,PANNo) VALUES(@UserId,@CompanyName,@OwnerName,@OfficeMail,@OfficeAddress,@GSTNo,@PANNo)";
                var param = new DynamicParameters();
                param.Add("@UserId", entity.UserId);
                param.Add("@CompanyName", entity.CompanyName);
                param.Add("@OwnerName", entity.OwnerName);
                param.Add("@OfficeMail", entity.OfficialEmail);
                param.Add("@OfficeAddress", entity.OfficeAddress);
                param.Add("GSTNo", entity.GSTNo);
                param.Add("PANNo", entity.PANNo);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
                return true;
            }
            catch
            {
                throw;
            }
        }

        public bool AccountAdd(UAccount entity, string[] uploadedFilePaths)
        {
            try
            {

                string query = "INSERT INTO [dbo].[UAccount]  ([UserId] ,[AccountPan],[AccountNo] ,[Ifsccode] ,[BankName] ,[AccountHolderName] ,[PaytmMID] ,[CreatedAt] ,[UpdatedAt],[IsActive], [AccountName],[AccountType])";
                query += " VALUES (@UserId,@AccountPan,@AccountNo,@Ifsccode,@BankName,@AccountHolderName,@PaytmMID,getdate(),getdate(),0,@AccountName,@AccountType) ";
                var param = new DynamicParameters();
                param.Add("@UserId", entity.UserId);
                param.Add("@AccountNo", entity.AccountNo);
                param.Add("@Ifsccode", entity.Ifsccode);
                param.Add("@BankName", entity.BankName);
                param.Add("@AccountHolderName", entity.AccountHolderName);
                param.Add("@AccountName", entity.AccountName);
                param.Add("@PaytmMID", entity.PaytmMID);
                param.Add("@AccountPan", entity.AccountPan);
                param.Add("@AccountType", entity.AccountType);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);

                if (uploadedFilePaths != null && uploadedFilePaths.Length > 0)
                {
                    foreach (var filePath in uploadedFilePaths)
                    {
                        string galleryquery = @" WITH MaxAccountId AS (SELECT ISNULL(MAX(AccountId), 0) AS AccountId FROM UAccount)
                                              INSERT INTO [dbo].[Account_Gallery_Detail] ([UserId],[FilePath],[AccountId])  
                                                       SELECT @UserId,@FilePath ,AccountId FROM MaxAccountId";
                        var galleryParams = new DynamicParameters();
                        galleryParams.Add("@UserId", entity.UserId);
                        galleryParams.Add("@FilePath", filePath);
                        SqlMapper.Execute(_transaction.Connection, galleryquery, galleryParams, _transaction, commandType: CommandType.Text);

                    }

                }

                
                return true;
            } 
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool AccountUpdate(UAccount entity)
        {
            try
            {
                string query = "UPDATE [dbo].[UAccount] SET ";
                query += " [AccountNo] = @AccountNo, [Ifsccode] = @Ifsccode,[BankName] = @BankName,[AccountHolderName] = @AccountHolderName, [IsActive] = 0 WHERE [AccountId] = @AccountId";
                var param = new DynamicParameters();
                param.Add("@AccountId", entity.AccountId);
                param.Add("@UserId", entity.UserId);
                param.Add("@AccountNo", entity.AccountNo);
                param.Add("@Ifsccode", entity.Ifsccode);
                param.Add("@BankName", entity.BankName); 
                param.Add("@AccountHolderName", entity.AccountHolderName);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
                return true;
            }
            catch (Exception e)
            {
                throw;
            }
        }
         
    }

    public interface IUserProfileDA
    {
        UserProfile GetUserDetail(int userId);
        IEnumerable<UAccount> GetUserAccount(int userId);
        bool UpdatePersonalDetail(UserProfile entity);        
        bool UpdateBusinessDetail(UserProfile entity);
        bool AccountAdd(UAccount entity, string[] uploadedFilePaths);
        bool AccountUpdate(UAccount entity);
    }
}
