﻿using FH.User.Entities;
using System;
using FH.Agent_Web.ViewModel;

namespace FH.Agent_Web
{
    public class AddTenantValidation
    {
        public static string Validate(AddTenantVM req)
        {
            if (req.PropertyId <= 0)
                return "Invalid input.";

            if (Validation.RequiredField(req.Name))
                return "Tenant name required.";
            else if (Validation.MaxLength(req.Name, 50))
                return "Tenant name can't be more than 50 characters.";

            if (Validation.RequiredField(req.Mobile))
                return "Mobile no required.";
            else if (Validation.MobileNo(req.Mobile))
                return "Invalid mobile no.";

            if (Validation.EmailAddress(req.Email))
                return "Invalid email id.";
            
            if (req.SecurityAmount < -1)
                return "Invalid security amount.";

            if (req.MonthlyRent <= 0)
                return "Monthly rent required.";

            if (req.CheckInDate == DateTime.MinValue)
                return "Check-In date required.";

            if (req.SecurityDeposit < 0)
                return "Invalid security deposit amount.";

            if (req.RoomId <= 0)
                return "Please Select Room.";

            if (req.Occupancy <= 0)
                return "Please Select Room.";
 

            //if (req.RentDeposit < 0)
            //    return "Invalid rent deposit amount.";

            //if (req.RentDeposit > 0 && req.RentForMonthStartDate == DateTime.MinValue)
            //    return "Rent From date required.";

            //if (req.RentDeposit > 0 && req.RentForMonthEndDate == DateTime.MinValue)
            //    return "Rent Till date required.";

            return string.Empty;
        }
    }
}