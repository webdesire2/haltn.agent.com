﻿
namespace FH.Agent_Web
{
    public class ValidationMessage
    {
        public const string required = "Required Field.";
        public const string invalidMobileNo = "Invalid Mobile No.";
        public const string invalidEmail = "Invalid Email.";
        public const string exceedLimit = "Cannot Exceed the Limit.";        
        public const string allowInt = "Valid Only Integer Type Value.";
        public const string invalidInput = "Invalid Input.";
        public const string userAlreadyExist = "User Already Exists.";
        public const string maxLength50 = "length can't be more than 50.";
        public const string maxLength100 = "length can't be more than 100.";

    }
}