﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Newtonsoft.Json;
using FH.Web.Security;
using FH.User.Entities;
using System.Web.Optimization;
using System.Security.Policy;
using System.Net;

namespace FH.User_Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // BundleConfig.RegisterBundles(BundleTable.Bundles);
            MvcHandler.DisableMvcResponseHeader = true;
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            try
            {
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    UserDetail serializeModel = JsonConvert.DeserializeObject<UserDetail>(authTicket.UserData);
                    CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                    newUser.UserId = serializeModel.UserId.ToString();
                    newUser.Name = serializeModel.Name;
                    newUser.Email = serializeModel.Email;
                    newUser.Mobile = serializeModel.Mobile;
                    newUser.Roles = new string[1] { serializeModel.UserType };

                    HttpContext.Current.User = newUser;
                }
               
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write("<script>console.log('Error occurred in PostAuthenticateRequest: " + ex.Message.Replace("'", "\\'") + "')</script>");
            }
        }

    }
}
