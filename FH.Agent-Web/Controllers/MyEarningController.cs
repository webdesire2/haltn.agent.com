﻿using FH.User_Web;
using FH.Web.Security;
using System.Web.Mvc;
using FH.User.Entities;
using FH.User.Service;
using System;
using FH.Agent_Web.ViewModel;
using Paytm;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Globalization;
using FH.Util;

namespace FH.Agent_Web.Controllers
{
    [CustomAuthorize]
    public class MyEarningController : BaseController
    {
        private readonly PaymentBL _paymentBL;
        public MyEarningController()
        {
            _paymentBL = new PaymentBL();
        }

        [HttpGet]
        public ActionResult List()
        {
            MyEarningListVM model = new MyEarningListVM();

            PaymentDE de = new PaymentDE()
            {
                UserId = Validation.DecryptToInt(CurrentUser.User.UserId),
                FromDate=new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),
                ToDate=new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month))
            }; 

            model.PaymentCollections=_paymentBL.GetCurrentMonthReceivedPayment(de);
            model.TotalPayment = _paymentBL.GetTotalofPayment(de);
            model.FromDate = de.FromDate;
            model.ToDate = de.ToDate;
            return View(model);
        }

        [HttpPost]
        public ActionResult List(MyEarningListVM model)
        {
            if (model.FromDate == null && model.ToDate != null)
                ModelState.AddModelError("FromDate", "From date required.");
            else if (model.FromDate != null && model.ToDate == null)
                ModelState.AddModelError("ToDate", "To date required.");
            else if (model.FromDate != null && model.ToDate != null && model.FromDate > model.ToDate)
                ModelState.AddModelError("FromDate","From date should be less than To date");
            else if(model.PropertyId==0 && model.FromDate ==null && model.ToDate==null)
            {
                model.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                model.ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            }
            PaymentDE de = new PaymentDE()
            {
                UserId = Validation.DecryptToInt(CurrentUser.User.UserId),
                FromDate = model.FromDate,
                ToDate = model.ToDate,
                PropertyId = model.PropertyId
            };
            model.TotalPayment = _paymentBL.GetTotalofPayment(de);
            model.PaymentCollections = _paymentBL.GetCurrentMonthReceivedPayment(de);
            return View(model);
        }

        [HttpGet]
        public ActionResult PaymentList()
        {
            MyEarningListVM model = new MyEarningListVM();

            PaymentDE de = new PaymentDE()
            {
                UserId = Validation.DecryptToInt(CurrentUser.User.UserId),
                FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),
                ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month))
            };

            model.PaymentCollections = _paymentBL.GetCurrentMonthReceivedPaymentPen(de);
            model.TotalPayment = _paymentBL.GetTotalofPayment(de);
            model.FromDate = de.FromDate;
            model.ToDate = de.ToDate;
            return View(model);
        }

        [HttpPost]
        public ActionResult PaymentList(MyEarningListVM model)
        {
            if (model.FromDate == null && model.ToDate != null)
                ModelState.AddModelError("FromDate", "From date required.");
            else if (model.FromDate != null && model.ToDate == null)
                ModelState.AddModelError("ToDate", "To da   te required.");
            else if (model.FromDate != null && model.ToDate != null && model.FromDate > model.ToDate)
                ModelState.AddModelError("FromDate", "From date should be less than To date");
            else if (model.PropertyId == 0 && model.FromDate == null && model.ToDate == null)
            {
                model.FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                model.ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            }
            PaymentDE de = new PaymentDE()
            {
                UserId = Validation.DecryptToInt(CurrentUser.User.UserId),
                FromDate = model.FromDate,
                ToDate = model.ToDate,
                TenantId = model.TenantId,
                PropertyId = model.PropertyId
            };
            model.TotalPayment = _paymentBL.GetTotalofPayment(de);
            model.PaymentCollections = _paymentBL.GetCurrentMonthReceivedPaymentPen(de);
            return View(model);
        }

        [HttpPost]
        public ActionResult CheckPayment(PaymentDetailsData model) {
            if (model.PaymentId == 0) {
                return JsonResultHelper.Error("Something went wrong.Please try again.");
            }
             
            Dictionary<string, string> body = new Dictionary<string, string>();
            Dictionary<string, string> head = new Dictionary<string, string>();
            Dictionary<string, Dictionary<string, string>> requestBody = new Dictionary<string, Dictionary<string, string>>();

            string PaytmKey = System.Configuration.ConfigurationManager.AppSettings["PaytmKey"].ToString();
            string PaytmMID = System.Configuration.ConfigurationManager.AppSettings["PaytmMID"].ToString();
            body.Add("mid", PaytmMID);
            body.Add("orderId", model.PaymentId.ToString());

            string paytmChecksum = Checksum.generateSignature(JsonConvert.SerializeObject(body), PaytmKey);
            head.Add("signature", paytmChecksum);
            requestBody.Add("body", body);
            requestBody.Add("head", head);
            string post_data = JsonConvert.SerializeObject(requestBody);
            string  url  =  "https://securegw.paytm.in/v3/order/status";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = post_data.Length;

            using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                requestWriter.Write(post_data);
            }

            string responseData = string.Empty;

            using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                responseData = responseReader.ReadToEnd();
                Console.WriteLine(responseData);
            }

            var JsonData = JsonConvert.DeserializeObject<JObject>(responseData);

            string transationStatus = JsonData["body"]["resultInfo"]["resultStatus"].ToString();
            string resultMsg = JsonData["body"]["resultInfo"]["resultMsg"].ToString();
            string resultCode = JsonData["body"]["resultInfo"]["resultCode"].ToString();
            if (resultCode != "334") {
                string TXNID = JsonData["body"]["txnId"].ToString();
                string BANKTXNID = "";
                string TXNAMOUNT = JsonData["body"]["txnAmount"].ToString();
                string PAYMENTMODE = "";

                string PaymentStatus = "FAILURE";
                if (transationStatus == "TXN_SUCCESS")
                {
                    PaymentStatus = "SUCCESS";
                    PAYMENTMODE = JsonData["body"]["paymentMode"].ToString();
                }
                PaymentDetail de = new PaymentDetail()
                {
                    PaymentId = model.PaymentId,
                    Status = PaymentStatus,  //responseCode == "E000" ? "SUCCESS" : "FAILURE"
                    TracingId = TXNID,
                    BankRefNo = BANKTXNID,
                    Amount = float.Parse(TXNAMOUNT, CultureInfo.InvariantCulture.NumberFormat),
                    PaymentMode = PAYMENTMODE, //method
                    StatusCode = resultCode,
                    StatusMessage = resultMsg
                };
                PaymentBL _paymentBL = new PaymentBL();

                var responce = _paymentBL.UpdatePaymentUser(de);

            } else {
                string TXNID = "";//JsonData["body"]["txnId"].ToString();
                string BANKTXNID = "";
                //string TXNAMOUNT = JsonData["body"]["txnAmount"].ToString();
                string PAYMENTMODE = "";

                string PaymentStatus = "FAILURE";
                if (transationStatus == "TXN_SUCCESS")
                {
                    PaymentStatus = "SUCCESS";
                    PAYMENTMODE = JsonData["body"]["paymentMode"].ToString();
                }
                PaymentDetail de = new PaymentDetail()
                {
                    PaymentId = model.PaymentId,
                    Status = PaymentStatus,  //responseCode == "E000" ? "SUCCESS" : "FAILURE"
                    TracingId = TXNID,
                    BankRefNo = BANKTXNID,
                    //Amount = float.Parse(TXNAMOUNT, CultureInfo.InvariantCulture.NumberFormat),
                    PaymentMode = PAYMENTMODE, //method
                    StatusCode = resultCode,
                    StatusMessage = resultMsg
                };
                PaymentBL _paymentBL = new PaymentBL();

                var responce = _paymentBL.UpdatePaymentUser(de);

            }


            return JsonResultHelper.Success("Payment Update Successfully");
        } 

    }
}