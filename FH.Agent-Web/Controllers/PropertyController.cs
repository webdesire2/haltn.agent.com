﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.User.Entities;
using System.Collections.Generic;
using FH.User.Service;
using System;
using System.Linq;
using System.Web;
using System.IO;
using System.Globalization;
using FH.Util;
using FH.Agent_Web;

namespace FH.User_Web.Controllers
{
    [CustomAuthorize]
    public class PropertyController : Controller
    {
        [HttpGet]     
        public ActionResult PostYourProperty()
        {
            if (CurrentUser.User != null)
                ViewBag.IsLoggedInUser ="yes";
            else
                ViewBag.IsLoggedInUser ="no";
            return View();
        }

        [HttpGet]
        public ActionResult Post()
        {
           // CityMasterBL _cityBL = new CityMasterBL();

            using (var uow = new UnitOfWork())
            {
                ViewBag.furnishingMaster=uow.FurnishingMaster.GetAll();
                ViewBag.amenityMaster=uow.AmenityMaster.GetAll();
            }
           // ViewBag.cityMaster = _cityBL.GetAllCity();
            return View();
        }

        [HttpGet]
        public ActionResult PostPG()
        {
           // CityMasterBL _cityBL = new CityMasterBL();

            using (var uow = new UnitOfWork())
            {                
                var amenities = uow.AmenityMaster.GetAll("PG");
                ViewBag.roomAmenity = amenities.Where(x => x.AmenityType == "PGRoom");
                ViewBag.pgAmenity = amenities.Where(x => x.AmenityType == "PG");               
            }

             // ViewBag.cityMaster=_cityBL.GetAllCity();

            return View();
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PostFlatRoom(PropertyFlatRoom property)
        {   
            /*********Property Detail **********/
            if(string.IsNullOrEmpty(property.PropertyMaster.PropertyType))
             return ValidationError("Property type required."); 

            string propertyType = property.PropertyMaster.PropertyType.ToUpper();
            if (propertyType != EnPropertyType.Flat.ToString().ToUpper() && propertyType != EnPropertyType.Room.ToString().ToUpper() && propertyType != EnPropertyType.Flatmate.ToString().ToUpper())
                return ValidationError("Invalid Property type.");
            
            if(string.IsNullOrWhiteSpace(property.PropertyDetail.ApartmentType))
                return ValidationError("Apartment type required.");
            if (!ApartmentType.GetAll().ContainsKey(property.PropertyDetail.ApartmentType))
                return ValidationError("Invalid apartment type.");

            if (propertyType=="FLAT" ||propertyType=="FLATMATE")
            {
                if(string.IsNullOrWhiteSpace(property.PropertyDetail.BhkType))
                    return ValidationError("BHK type required.");
                if(!BHKType.GetAll().ContainsKey(property.PropertyDetail.BhkType))
                    return ValidationError("Invalid BHK type.");

            }   

            if(propertyType == "FLATMATE" || propertyType == "ROOM")
             {
                 if(string.IsNullOrWhiteSpace(property.PropertyDetail.RoomType))
                    return ValidationError("Room type required.");
                 if(!RoomType.GetAll().ContainsKey(property.PropertyDetail.RoomType))
                    return ValidationError("Invalid Room type.");
             }

            if (propertyType == "FLATMATE")
            {                
                if (!string.IsNullOrWhiteSpace(property.PropertyDetail.TenantGender) && property.PropertyDetail.TenantGender.ToLower() != "male" && property.PropertyDetail.TenantGender.ToLower() != "female")
                    return ValidationError("Invalid gender type.");
            }

            else if (!string.IsNullOrWhiteSpace(property.PropertyDetail.PropertySize) && (property.PropertyDetail.PropertySize.Length > 5 || !Validation.IsInt(property.PropertyDetail.PropertySize)))
                return ValidationError("Invalid Property size.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.Facing) && !FacingType.GetAll().ContainsKey(property.PropertyDetail.Facing))
                return ValidationError("Invalid facing type.");
            
            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.PropertyAge) && !PropertyAgeType.GetAll().ContainsKey(property.PropertyDetail.PropertyAge))
                return ValidationError("Invalid property age.");
            
            if(property.PropertyDetail.FloorNo!=null && property.PropertyDetail.TotalFloor==null)
                return ValidationError("Total floor required.");
            if (property.PropertyDetail.FloorNo == null && property.PropertyDetail.TotalFloor != null)
                return ValidationError("Floor no required.");
            if (property.PropertyDetail.FloorNo != null && property.PropertyDetail.TotalFloor != null && property.PropertyDetail.FloorNo > property.PropertyDetail.TotalFloor)
                return ValidationError("Floor can't be greater than Total Floor.");
            
            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.WaterSupply) && ! WaterSupplyType.GetAll().ContainsKey(property.PropertyDetail.WaterSupply))
                return ValidationError("Invalid water supply type.");

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.GateSecurity) && property.PropertyDetail.GateSecurity != "Yes" && property.PropertyDetail.GateSecurity != "No")
                return ValidationError("Invalid Gate security.");

            /*********** Locality Detail ***********/

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonName))
                return ValidationError("Contact person name required.");
            else if(property.PropertyMaster.ContactPersonName.Length>50)
                return ValidationError("Contact person name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonMobile))
                return ValidationError("Contact person mobile no required.");
            else if (Validation.MobileNo(property.PropertyMaster.ContactPersonMobile))
                return ValidationError("Invalid contact person mobile no.");
            
            //if (string.IsNullOrWhiteSpace(property.PropertyMaster.City))
            //    return ValidationError("City required.");
           // if (!City.GetAll().ContainsKey(property.PropertyMaster.City))
             //   return ValidationError("Invalid city.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Locality))
                return ValidationError("Locality required.");
            if(property.PropertyMaster.Locality.Length > 100)
                return ValidationError("Locality can't be greater than 100 characters.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Latitude) || string.IsNullOrEmpty(property.PropertyMaster.Latitude))
                return ValidationError("Please select Locality from list.");

            if (!string.IsNullOrWhiteSpace(property.PropertyMaster.Street) && property.PropertyMaster.Street.Length > 100)
                return ValidationError("Property address can't be more than 100 characters.");

            /**************** Rental Detail **********/
            if (property.PropertyDetail.ExpectedRent == 0)
                return ValidationError("Expected Rent required.");
           

            if (!string.IsNullOrEmpty(property.PropertyDetail.AvailableFrom))
            {
                string availableFrom = property.PropertyDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.PropertyDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Invalid available from date format.");
            }
            
           if (!string.IsNullOrWhiteSpace(property.PropertyDetail.PreferredTenant) && !PreferredTenant.GetAll().ContainsKey(property.PropertyDetail.PreferredTenant))
                return ValidationError("Invalid preferred tenant type.");
 
            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.Parking) && !ParkingType.GetAll().ContainsKey(property.PropertyDetail.Parking))
                return ValidationError("Invalid parking type.");

            if (string.IsNullOrWhiteSpace(property.PropertyDetail.Furnishing))
                return ValidationError("Furnishing type required.");
            if (!FurnishingType.GetAll().ContainsKey(property.PropertyDetail.Furnishing))
                return ValidationError("Invalid furnishing type.");
            else
            {
                if(property.PropertyDetail.Furnishing=="FF" || property.PropertyDetail.Furnishing == "SF")
                {
                    if (property.PropertyDetail.FurnishingDetails==null)
                        return ValidationError("Furnishing details required.");
                }
            }

            if (!string.IsNullOrWhiteSpace(property.PropertyDetail.Description) && property.PropertyDetail.Description.Length > 500)
                return ValidationError("Description can't be more than 500 characters.");

            property.PropertyDetail.ApartmentType =(!string.IsNullOrWhiteSpace(property.PropertyDetail.ApartmentType)?ApartmentType.GetAll().Where(x => x.Key == property.PropertyDetail.ApartmentType).Select(x => x.Value).FirstOrDefault():null);

            if (propertyType == "FLAT" || propertyType == "FLATMATE")
                property.PropertyDetail.BhkType =(!string.IsNullOrWhiteSpace(property.PropertyDetail.BhkType)?BHKType.GetAll().Where(x => x.Key == property.PropertyDetail.BhkType).Select(x => x.Value).FirstOrDefault():null);
            else
                property.PropertyDetail.BhkType = null;

            if (propertyType == "FLATMATE" || propertyType == "ROOM")
                property.PropertyDetail.RoomType =(!string.IsNullOrWhiteSpace(property.PropertyDetail.RoomType)?RoomType.GetAll().Where(x => x.Key == property.PropertyDetail.RoomType).Select(x => x.Value).FirstOrDefault():null);
            else
                property.PropertyDetail.RoomType = null;

            if (propertyType != "FLATMATE")
                property.PropertyDetail.TenantGender = null;

            property.PropertyDetail.Facing =(!string.IsNullOrWhiteSpace(property.PropertyDetail.Facing)?FacingType.GetAll().Where(x => x.Key == property.PropertyDetail.Facing).Select(x => x.Value).FirstOrDefault():null);
            property.PropertyDetail.PropertyAge =(!string.IsNullOrWhiteSpace(property.PropertyDetail.PropertyAge)?PropertyAgeType.GetAll().Where(x => x.Key == property.PropertyDetail.PropertyAge).Select(x => x.Value).FirstOrDefault():null);
            property.PropertyDetail.WaterSupply =(!string.IsNullOrWhiteSpace(property.PropertyDetail.WaterSupply)?WaterSupplyType.GetAll().Where(x => x.Key == property.PropertyDetail.WaterSupply).Select(x => x.Value).FirstOrDefault():null);
            property.PropertyDetail.PreferredTenant =(!string.IsNullOrWhiteSpace(property.PropertyDetail.PreferredTenant)?PreferredTenant.GetAll().Where(x => x.Key == property.PropertyDetail.PreferredTenant).Select(x => x.Value).FirstOrDefault():null);
            property.PropertyDetail.Parking =(!string.IsNullOrWhiteSpace(property.PropertyDetail.Parking)?ParkingType.GetAll().Where(x => x.Key == property.PropertyDetail.Parking).Select(x => x.Value).FirstOrDefault():null);
            property.PropertyDetail.Furnishing = FurnishingType.GetAll().Where(x => x.Key == property.PropertyDetail.Furnishing).Select(x => x.Value).FirstOrDefault();
            property.PropertyMaster.CreatedBy =Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));

            using (var uow = new UnitOfWork())
            {
                try
                {
                   int propertyId=uow.PropertyBL.AddPropertyFlatRoomFlatMate(property);
                    uow.Commit();
                    return Json(new { Success = true, Data = RenderRazorViewToString(ControllerContext, "_Congratulation",null) });
                    
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again" });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PostPG(PropertyPG property)
        {
            /****************PG Room Detail*************/
            if (property.RoomRentalDetail == null || property.RoomRentalDetail.Count() <= 0)
                return ValidationError("Rental detail required.");
            
            foreach(var room in property.RoomRentalDetail)
            {
                if (!string.IsNullOrEmpty(room.RoomType) && room.RoomType.ToLower() != "single" && room.RoomType.ToLower() != "double" && room.RoomType.ToLower() != "three" && room.RoomType.ToLower() != "four")
                    return ValidationError("Invalid room type.");

                if (room.ExpectedRent <= 0)
                    return ValidationError("Expected rent required.");
                
               // if (room.TotalSeat <= 0 || room.AvailableSeat <= 0 || room.TotalSeat < room.AvailableSeat)
                   // return ValidationError("Total seat should be more than available seat.");
            }

            if(property.RoomAmenities!=null)
            {
                foreach(var roomAmenity in property.RoomAmenities)
                {
                    if (!Validation.IsInt(roomAmenity.AmenityId))
                        return ValidationError("Invalid room amenities.");
                }
            }

            /*********** Locality Detail ***********/
            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonName))
                return ValidationError("Contact person name required.");
            else if (property.PropertyMaster.ContactPersonName.Length > 50)
                return ValidationError("Contact person name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.PropertyName))
                return ValidationError("Property Name required.");
            else if (property.PropertyMaster.PropertyName.Length > 100)
                return ValidationError("Property Name can't be more than 100 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.ContactPersonMobile))
                return ValidationError("Contact person mobile no required.");
            else if (Validation.MobileNo(property.PropertyMaster.ContactPersonMobile))
                return ValidationError("Invalid contact person mobile no.");

            if (string.IsNullOrWhiteSpace(property.PropertyMaster.City))
                return ValidationError("City required.");

            //if (!City.GetAll().ContainsKey(property.PropertyMaster.City))
              //  return ValidationError("Invalid city.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Locality))
                return ValidationError("Locality required.");
            if(property.PropertyMaster.Locality.Length > 100)
                return ValidationError("Locality can't be more than 100 characters.");

            if (string.IsNullOrEmpty(property.PropertyMaster.Latitude) || string.IsNullOrEmpty(property.PropertyMaster.Latitude))
                return ValidationError("Please select locality from list.");

            if (!string.IsNullOrWhiteSpace(property.PropertyMaster.Street) && property.PropertyMaster.Street.Length > 100)
                return ValidationError("Address can't be more than 100 characters.");

            /**************** PG Detail ****************/
            if (string.IsNullOrEmpty(property.PgDetail.TenantGender))
                return ValidationError("PG available for required.");
            else if(property.PgDetail.TenantGender.ToLower() != "male" && property.PgDetail.TenantGender.ToLower() != "female" && property.PgDetail.TenantGender.ToLower() != "co-living" && property.PgDetail.TenantGender.ToLower() != "roomrk")
                return ValidationError("Invalid PG available for type.");
            
             if (!string.IsNullOrEmpty(property.PgDetail.PreferredGuest) && !PreferredGuest.GetAll().ContainsKey(property.PgDetail.PreferredGuest))
                return ValidationError("Invalid preferred guest.");

            if (!string.IsNullOrEmpty(property.PgDetail.AvailableFrom))
            {
                string availableFrom = property.PgDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.PgDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Invalid available from date format.");
            }
            
            /*************** PG Rules ******************/
            
            if (property.pgRules != null)
            {
                foreach (var rule in property.pgRules)
                { if (!PGRulesMaster.GetAll().ContainsKey(rule.RuleId))
                        return ValidationError("Invalid PG rules.");                                  
                }
            }

            List<PGRule> pgRules = new List<PGRule>();
            if(property.pgRules !=null)
            {
                foreach(var rule in property.pgRules)
                {
                    var item = PGRulesMaster.GetAll().Where(x => x.Key == rule.RuleId).FirstOrDefault();
                    pgRules.Add(new PGRule {RuleId=item.Key,RuleName=item.Value });
                }
            }

            property.pgRules = pgRules;            
            property.PgDetail.PreferredGuest =(!string.IsNullOrWhiteSpace(property.PgDetail.PreferredGuest)?PreferredGuest.GetAll().Where(x => x.Key.ToLower() == property.PgDetail.PreferredGuest.ToLower()).FirstOrDefault().Value:null);            
            property.PropertyMaster.CreatedBy = Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId));

            using(var uow=new UnitOfWork())
            {
                try
                {
                    int propertyId = uow.PropertyBL.AddPropertyPG(property);
                    if (propertyId > 0)
                    {
                        uow.Commit();
                        return Json(new { Success = true, Data = RenderRazorViewToString(ControllerContext, "_Congratulation", null) });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
                catch(Exception e)
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }            
        }
        
        [HttpGet]
        public ActionResult Edit(string pid, string lavel="")
        {            
            if(Validation.DecryptToInt(pid)==0)
                return PartialView("_Error");

           // CityMasterBL _cityBL = new CityMasterBL();
            using (var uow = new UnitOfWork())
            {
                Property propertyDetail = uow.PropertyBL.GetProperty(Validation.DecryptToInt(pid), Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)));
                propertyDetail.lavel = lavel;
                if (propertyDetail==null)
                    return PartialView("_Error");                 
                 else if(propertyDetail.PropertyFlatRoomFlatmate!=null)
                 {
                   // ViewBag.cityMaster = _cityBL.GetAllCity();
                    return View("Edit", propertyDetail);
                 }
                else if (propertyDetail.PropertyPG != null)
                {
                    //ViewBag.cityMaster = _cityBL.GetAllCity();
                    return View("EditPG", propertyDetail);
                }
            }

            return PartialView("_Error");
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdatePropertyDetail(string pid,string ptype,PropertyDetailFlatRoom property)
        {   
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return ValidationError("Invalid input.");

            if (string.IsNullOrWhiteSpace(ptype))
                return ValidationError("Invalid property type.");

            string propertyType = ptype.ToUpper();
            
            if(propertyType != EnPropertyType.Flat.ToString().ToUpper() && propertyType != EnPropertyType.Room.ToString().ToUpper() && propertyType != EnPropertyType.Flatmate.ToString().ToUpper())
                return ValidationError("Invalid property type.");

            if (string.IsNullOrWhiteSpace(property.ApartmentType))
                return ValidationError("Apartment type required.");
            if (!ApartmentType.GetAll().ContainsKey(property.ApartmentType))
                return ValidationError("Invalid apartment type.");

            if (propertyType == "FLAT" || propertyType == "FLATMATE")
            {
                if (string.IsNullOrWhiteSpace(property.BhkType))
                    return ValidationError("BHK type required.");
                if (!BHKType.GetAll().ContainsKey(property.BhkType))
                    return ValidationError("Invalid BHK type.");
            }

            if (propertyType == "FLATMATE" || propertyType == "ROOM")
            {
                if (string.IsNullOrWhiteSpace(property.RoomType))
                    return ValidationError("Room type required.");
                if (!RoomType.GetAll().ContainsKey(property.RoomType))
                    return ValidationError("Invalid Room type.");
            }

            if (propertyType == "FLATMATE")
            {
                if (!string.IsNullOrWhiteSpace(property.TenantGender) && property.TenantGender.ToLower() != "male" && property.TenantGender.ToLower() != "female")
                    return ValidationError("Invalid gender type.");
            }

            else if (!string.IsNullOrWhiteSpace(property.PropertySize) && (property.PropertySize.Length > 5 || !Validation.IsInt(property.PropertySize)))
                return ValidationError("Invalid Property size.");

            if (!string.IsNullOrWhiteSpace(property.Facing) && !FacingType.GetAll().ContainsKey(property.Facing))
                return ValidationError("Invalid facing type.");

            if (!string.IsNullOrWhiteSpace(property.PropertyAge) && !PropertyAgeType.GetAll().ContainsKey(property.PropertyAge))
                return ValidationError("Invalid property age.");

            if (property.FloorNo != null && property.TotalFloor == null)
                return ValidationError("Total floor required.");
            if (property.FloorNo == null && property.TotalFloor != null)
                return ValidationError("Floor no required.");
            if (property.FloorNo != null && property.TotalFloor != null && property.FloorNo > property.TotalFloor)
                return ValidationError("Floor can't be greater than Total Floor.");

            if (!string.IsNullOrWhiteSpace(property.WaterSupply) && !WaterSupplyType.GetAll().ContainsKey(property.WaterSupply))
                return ValidationError("Invalid water supply type.");

            if (!string.IsNullOrWhiteSpace(property.GateSecurity) && property.GateSecurity != "Yes" && property.GateSecurity != "No")
                return ValidationError("Invalid Gate security.");


            property.ApartmentType = ApartmentType.GetAll().Where(x => x.Key == property.ApartmentType).Select(x => x.Value).FirstOrDefault();

            if (propertyType == "FLAT" || propertyType == "FLATMATE")
                property.BhkType = BHKType.GetAll().Where(x => x.Key == property.BhkType).Select(x => x.Value).FirstOrDefault();
            else
                property.BhkType = null;

            if (propertyType == "FLATMATE" || propertyType == "ROOM")
                property.RoomType = RoomType.GetAll().Where(x => x.Key == property.RoomType).Select(x => x.Value).FirstOrDefault();
            else
                property.RoomType = null;

            if (propertyType != "FLATMATE")
                property.TenantGender = null;

            property.Facing = (!string.IsNullOrWhiteSpace(property.Facing) ? FacingType.GetAll().Where(x => x.Key == property.Facing).Select(x => x.Value).FirstOrDefault() : null);
            property.PropertyAge = (!string.IsNullOrWhiteSpace(property.PropertyAge) ? PropertyAgeType.GetAll().Where(x => x.Key == property.PropertyAge).Select(x => x.Value).FirstOrDefault() : null);
            property.WaterSupply = (!string.IsNullOrWhiteSpace(property.WaterSupply) ? WaterSupplyType.GetAll().Where(x => x.Key == property.WaterSupply).Select(x => x.Value).FirstOrDefault() : null);
            
            using (var uow = new UnitOfWork())
            {
                try
                {
                    string result = uow.PropertyBL.UpdatePropertyDetail(propertyId, propertyType, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), property);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage ="Something went wrong.Please try again." });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateLocalityDetail(string pid,PropertyMaster property)
        {   
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return ValidationError("Invalid input.");

            if (string.IsNullOrWhiteSpace(property.ContactPersonName))
                return ValidationError("Contact person name required.");
            else if (property.ContactPersonName.Length > 50)
                return ValidationError("Contact person name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.PropertyName))
                return ValidationError("Property Name required.");
            else if (property.PropertyName.Length > 200)
                return ValidationError("Property Name can't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(property.ContactPersonMobile))
                return ValidationError("Contact person mobile no required.");
            else if (Validation.MobileNo(property.ContactPersonMobile))
                return ValidationError("Invalid contact person mobile no.");

            //if (string.IsNullOrWhiteSpace(property.City))
            //    return ValidationError("City required.");

            //if (!City.GetAll().ContainsKey(property.City))
              //  return ValidationError("Invalid city.");

            if (string.IsNullOrEmpty(property.Locality))
                return ValidationError("Locality required.");
            if (property.Locality.Length > 100)
                return ValidationError("Locality can't be greater than 100 characters.");

            if (string.IsNullOrEmpty(property.Latitude) || string.IsNullOrEmpty(property.Latitude))
                return ValidationError("Please select Locality from list.");

            if (!string.IsNullOrWhiteSpace(property.Street) && property.Street.Length > 100)
                return ValidationError("Property address can't be more than 100 characters.");
            
            using (var uow = new UnitOfWork())
            {
                try
                {
                    string result = uow.PropertyBL.UpdateLocalityDetail(propertyId,Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), property);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateRentalDetail(string pid,string ptype,PropertyDetailFlatRoom property)
        {  
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return ValidationError("Invalid input.");

            if (string.IsNullOrWhiteSpace(ptype))
                return ValidationError("Property type required.");

            string propertyType =ptype.ToUpper();

            if (propertyType != EnPropertyType.Flat.ToString().ToUpper() && propertyType != EnPropertyType.Room.ToString().ToUpper() && propertyType != EnPropertyType.Flatmate.ToString().ToUpper())
                return ValidationError("Invalid property type.");

            if (property.ExpectedRent == 0)
                return ValidationError("Expected Rent required.");
            
            if (!string.IsNullOrEmpty(property.AvailableFrom))
            {
                string availableFrom = property.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    property.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Invalid available from date format.");
            }

            if (!string.IsNullOrWhiteSpace(property.PreferredTenant) && !PreferredTenant.GetAll().ContainsKey(property.PreferredTenant))
                return ValidationError("Invalid preferred tenant type.");

            if (!string.IsNullOrWhiteSpace(property.Parking) && !ParkingType.GetAll().ContainsKey(property.Parking))
                return ValidationError("Invalid parking type.");

            if (string.IsNullOrWhiteSpace(property.Furnishing))
                return ValidationError("Furnishing type required.");
            if (!FurnishingType.GetAll().ContainsKey(property.Furnishing))
                return ValidationError("Invalid furnishing type.");
            else
            {
                if (property.Furnishing == "FF" || property.Furnishing == "SF")
                {
                    if (property.FurnishingDetails == null)
                        return ValidationError("Furnishing details required.");
                }
            }

            if (!string.IsNullOrWhiteSpace(property.Description) && property.Description.Length > 500)
                return ValidationError("Description can't be more than 500 characters.");

            property.PreferredTenant = (!string.IsNullOrWhiteSpace(property.PreferredTenant) ? PreferredTenant.GetAll().Where(x => x.Key == property.PreferredTenant).Select(x => x.Value).FirstOrDefault() : null);
            property.Parking = (!string.IsNullOrWhiteSpace(property.Parking) ? ParkingType.GetAll().Where(x => x.Key == property.Parking).Select(x => x.Value).FirstOrDefault() : null);
            property.Furnishing = FurnishingType.GetAll().Where(x => x.Key == property.Furnishing).Select(x => x.Value).FirstOrDefault();
            using (var uow = new UnitOfWork())
            {
                try
                {
                    string result = uow.PropertyBL.UpdateRentalDetail(propertyId,propertyType,Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), property);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                        return Json(new { Success = false, ErrorMessage = result });
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }

        }

        [HttpPost]
        public ActionResult UploadGallery()
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                //var supportedTypes = new[] { ".png", ".jpg", "jpeg", ".gif", ".bmp" };
                string fileName = Path.GetFileNameWithoutExtension(postedFile.FileName);
                string fileExt = Path.GetExtension(postedFile.FileName);

                //if (!supportedTypes.Contains(fileExt.ToLower()))
                //    return Json(new { Success = false, Error = "Only PNG/JPG/JPEG/GIF/BMP image is allowed." });

                //if (postedFile.ContentLength > (1048576 * 5)) // 5MB
                //    return Json(new { Success = false, Error = "File size cannot be exceed 5 MB." });
                if (!string.IsNullOrEmpty(CurrentUser.User.UserId))
                {
                    // string filePath = FileManager.FilePath(fileName);
                    FileManager.S3Response reponse = FileManager.Upload(postedFile.InputStream, FileManager.S3Folder.Property, fileName, fileExt);
                    //   postedFile.SaveAs(Server.MapPath("~/" + filePath));

                    return Json(new { Success = true, Id = Request.Form["Id"], Url = reponse.FileUrl, File = reponse.FileName });
                }

            }
            return Json(new { Success = false, Error = "File not found." });
        }

        //AddRoom
        [HttpPost]
        public ActionResult AddRoom(PGRoomData req)
        {
            using (var uow = new UnitOfWork())
            {
                req.PropertyId = Validation.DecryptToInt(req.Pid);
                string result = uow.PropertyBL.AddRoom(req);
                if (string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new { Success = true, SuccessMessage = "Add Successfully." });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result });
            }
        }

        //
        [HttpPost]
        public ActionResult EditRoom(PGRoomData req)
        {
            using (var uow = new UnitOfWork())
            {
                string result = uow.PropertyBL.EditRoom(req);
                if (string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new { Success = true, SuccessMessage = "Update Successfully." });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result });
            }
        }

        [HttpPost]
        public ActionResult AddNewGallery()
        {
            if (Request.Params["pid"] == null && string.IsNullOrWhiteSpace(Request.Params["pid"]))
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            int propertyId = Validation.DecryptToInt(Request.Params["pid"]);
            if (propertyId == 0)
                return Json(new { Success = false, ErrorMessage = "Invalid input." });

            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                var supportedTypes = new[] { ".png", ".jpg", "jpeg", ".gif", ".bmp" };
                string fileName = postedFile.FileName;
                string fileExt = Path.GetExtension(postedFile.FileName);

                if (!supportedTypes.Contains(fileExt.ToLower()))
                    return Json(new { Success = false, ErrorMessage = "Only PNG/JPG/JPEG/GIF/BMP image is allowed." });

                if (postedFile.ContentLength > (1048576 * 5)) // 5MB
                    return Json(new { Success = false, ErrorMessage = "File size cannot be exceed 5 MB." });

                if (!string.IsNullOrEmpty(CurrentUser.User.UserId))
                {
                    FileManager.S3Response reponse = FileManager.Upload(postedFile.InputStream, FileManager.S3Folder.Property, fileName, fileExt);
                    // if (reponse.Success)
                    //     return Json(new { Success = true, Path =reponse.FileUrl, FileId = Request.Form["Id"] });                    
                    if (!reponse.Success)
                        return Json(new { Success = false, Id = Request.Form["Id"] });

                    using (var uow = new UnitOfWork())
                    {
                        var fileId = uow.PropertyBL.AddNewGallery(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), reponse.FileUrl);
                        if (!string.IsNullOrEmpty(fileId))
                        {
                            uow.Commit();
                            // postedFile.SaveAs(Server.MapPath("~/" + filePath));
                            return Json(new { Success = true, Path = reponse.FileUrl, FileId = fileId });
                        }
                        else
                            return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                    }
                }
            }
            return Json(new { Success = false, ErrorMessage = "File not found." });
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult DeleteGallery(string pid,int image)
        {
            var errorMessage = Json(new { Success = false, ErrorMessage = "Invalid input." });
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return errorMessage;
            if(image==0)
                return errorMessage;

            using (var uow = new UnitOfWork())
            {
                try
                {
                   string filePath=uow.PropertyBL.DeleteGallery(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), image);
                    if (string.IsNullOrEmpty(filePath))
                    {
                        return Json(new { Success = false, ErrorMessage = "Invalid request." });
                    }
                    else
                    {
                        uow.Commit();                       
                       FileManager.Delete(filePath);
                        return Json(new { Success = true });
                    }
                }
                catch
                {
                    return Json(new { Success = false, ErrorMessage = "Something went wrong.Please try again." });
                }
            }                
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult RemoveGalleryFile(string imagePath)
        {
            if(!string.IsNullOrEmpty(imagePath) && !string.IsNullOrEmpty(CurrentUser.User.UserId))
            {
                if(FileManager.Delete(imagePath))
                    return Json(new { Success = true});
            }
            return Json(new { Success = false, ErrorMessage = "Invalid request." });
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdateAmenity(string pid,IEnumerable<PropertyAmenitiesDetails> amenities)
        {
            var errorMessage = Json(new { Success = false, ErrorMessage = "Invalid input." });
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return errorMessage;

            using (var uow = new UnitOfWork())
            {                
                    string result = uow.PropertyBL.UpdatePropertyAmenity(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), amenities);
                    if (string.IsNullOrEmpty(result))
                    {
                        uow.Commit();
                        return Json(new { Success = true });
                    }
                    else
                    return Json(new { Success = false, ErrorMessage =result});
            }
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdatePGRoomDetail(string pid,List<PGRoomRentalDetail> rentalDetail,IEnumerable<PGRoomAmenity> roomAmenity)
        {   
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return ValidationError("Invalid input.");

            if (rentalDetail == null || rentalDetail.Count() <= 0)
                return ValidationError("Rental detail required.");

            foreach (var room in rentalDetail)
            {
                if (!string.IsNullOrEmpty(room.RoomType) && room.RoomType.ToLower() != "single" && room.RoomType.ToLower() != "double" && room.RoomType.ToLower() != "three" && room.RoomType.ToLower() != "four")
                    return ValidationError("Invalid room type.");

                if (room.ExpectedRent <= 0)
                    return ValidationError("Expected rent required.");
                if(room.TotalSeat<room.AvailableSeat)
                    return ValidationError("Total Seat should be more than available seat.");
            }

            if (roomAmenity != null)
            {
                foreach (var amenity in roomAmenity)
                {
                    if (!Validation.IsInt(amenity.AmenityId))
                        return ValidationError("Invalid room amenity.");
                }
            }

            using (var uow = new UnitOfWork())
            {                
               string result= uow.PropertyBL.UpdatePGRoomDetail(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), rentalDetail, roomAmenity);
                if(string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new { Success = true});
                }
                else
                    return Json(new { Success = false,ErrorMessage=result});
            }
        }
         
        [HttpPost]   
        [AjaxRequestValidation]
        public ActionResult UpdatePGDetail(string pid,PGDetail pgDetail,IEnumerable<PGRule> pgRules)
        {   
            int propertyId = Validation.DecryptToInt(pid);
            if (propertyId == 0)
                return ValidationError("Invalid input.");

            if (string.IsNullOrEmpty(pgDetail.TenantGender))
                return ValidationError("PG available for required.");
            else if (pgDetail.TenantGender.ToLower() != "male" && pgDetail.TenantGender.ToLower() != "female" && pgDetail.TenantGender.ToLower() != "co-living" && pgDetail.TenantGender.ToLower() != "roomrk")
                return ValidationError("Invalid input PG available for.");
            
             if (!string.IsNullOrEmpty(pgDetail.PreferredGuest) && !PreferredGuest.GetAll().ContainsKey(pgDetail.PreferredGuest))
                return ValidationError("Invalid input preferred guest.");

            if (!string.IsNullOrEmpty(pgDetail.AvailableFrom))
            {
                string availableFrom = pgDetail.AvailableFrom;
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    pgDetail.AvailableFrom = DateTime.ParseExact(availableFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Invalid available from date format.");
            }

            if (pgRules != null)
            {
                foreach (var rule in pgRules)
                {
                    if (!string.IsNullOrWhiteSpace(rule.RuleId.ToString()) && !PGRulesMaster.GetAll().ContainsKey(rule.RuleId))
                        return ValidationError("Invalid input PG rules.");
                }
            }

            List<PGRule> lstRules = new List<PGRule>();
            if (pgRules != null)
            {
                foreach (var rule in pgRules)
                {
                    var item = PGRulesMaster.GetAll().Where(x => x.Key == rule.RuleId).FirstOrDefault();
                    lstRules.Add(new PGRule { RuleId = item.Key, RuleName = item.Value });
                }
            }

            pgDetail.PreferredGuest =(!string.IsNullOrWhiteSpace(pgDetail.PreferredGuest)?PreferredGuest.GetAll().Where(x => x.Key.ToLower() == pgDetail.PreferredGuest.ToLower()).FirstOrDefault().Value:null);

            using (var uow = new UnitOfWork())
            {
                string result = uow.PropertyBL.UpdatePGDetail(propertyId, Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)), pgDetail, lstRules);
                if(string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new {Success=true});
                }
                else
                    return Json(new { Success = false,ErrorMessage=result});
            }
        }

        private JsonResult ValidationError(string message)
        {
            return Json(new {Success=false,ErrorMessage=message});
        }

        private string RenderRazorViewToString(ControllerContext controllerContext, string viewName, object model)
        {
            controllerContext.Controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                ViewResult.View.Render(ViewContext, sw);
                ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpGet]
        public ActionResult ContactUs()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Aboutus()
        {
            if (CurrentUser.User != null)
                ViewBag.IsLoggedInUser = "yes";
            else
                ViewBag.IsLoggedInUser = "no";
            return View();
        }
        [HttpGet]
        public ActionResult termscondition()
        {
            return View();
        }
        [HttpGet]
        public ActionResult privacypoicy()
        {
            return View();
        }

    }
}