﻿using FH.User_Web;
using FH.Web.Security;
using System.Web.Mvc;
using FH.User.Entities;
using FH.User.Service;
using FH.Util;
using FH.Agent_Web.ViewModel;
using System.Collections.Generic;
using System;
using System.IO;
using Amazon.DynamoDBv2.DocumentModel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Linq;
using Newtonsoft.Json;

namespace FH.Agent_Web.Controllers
{
    public class AccountsController : BaseController
    {
        // GET: Accounts
        public ActionResult Index()
        {
            return View();
        }
    }
}