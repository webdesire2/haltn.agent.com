﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.User.Entities;
using FH.User.Service;
using System;
using FH.Agent_Web;
using System.Web;
using System.IO;
using FH.Util;
using Razorpay.Api;

namespace FH.User_Web.Controllers
{
    [CustomAuthorize]
    public class UserProfileController : BaseController
    {
        [HttpGet]
        public ActionResult MyProfile()
        {
            UserProfile UserProfile = new UserProfile();
            UserData UserData = new UserData();
            using (UnitOfWork uow = new UnitOfWork())
            {
                UserProfile = uow.UserProfileBL.GetUserDetail(Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)));
                UserData.UAccount = uow.UserProfileBL.GetUserAccount(Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)));
            }
            if (UserProfile == null)
                return PartialView("_UnAuthorize");
            
            UserData.UserProfile = UserProfile;
            return View(UserData);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult UpdatePersonalDetail(UserProfile entity)
        {
            if (string.IsNullOrWhiteSpace(entity.UserName))
                return ValidationError("User name required.");
            else if(entity.UserName.Length>50)
                return ValidationError("User name cann't be more than 50 characters.");

            if (string.IsNullOrWhiteSpace(entity.UserEmail))
                return ValidationError("Email required.");
            else if (Validation.EmailAddress(entity.UserEmail))
                return ValidationError("Invalid email id.");

            if (CurrentUser.User != null && !string.IsNullOrWhiteSpace(CurrentUser.User.UserId))
            {
                int userId = Validation.DecryptToInt(CurrentUser.User.UserId);
                if (userId <= 0)
                    return ValidationError("Something went wrong.Please try again.");
                entity.UserId = userId;
            }

            using (var uow = new UnitOfWork())
            {
                uow.UserProfileBL.UpdatePersonalDetail(entity);
                uow.Commit();
                return Json(new { Success = true,Message="Personal Details Update Successfully."});
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult AccountAdd(UAccount entity)
        {
            string[] uploadfilepaths = Request.Form.GetValues("uploadfilepaths");

            using (var uow = new UnitOfWork())
            {
                if (uploadfilepaths != null && uploadfilepaths.Length > 0)
                {
                    //foreach (var filePath in uploadfilepaths)
                    //{ }
                }
                entity.UserId = Validation.DecryptToInt(CurrentUser.User.UserId);
                Random rnd = new Random();
                int unique = rnd.Next(10000, 99999);
                var MID = "HALTNP" + entity.UserId + unique;
                entity.PaytmMID = MID;
                uow.UserProfileBL.AccountAdd(entity, uploadfilepaths);
                uow.Commit();
                return Json(new { Success = true, Message = "New Account added successfully." });
            }
        }

      
        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult AccountUpdate(UAccount entity)
        {
            using (var uow = new UnitOfWork())
            {
                entity.UserId = Validation.DecryptToInt(CurrentUser.User.UserId);
                uow.UserProfileBL.AccountUpdate(entity);
                uow.Commit();
                return Json(new { Success = true, Message = "Account Update Successfully." });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult UpdateBusinessDetail(UserProfile entity)
        {
            if (!string.IsNullOrWhiteSpace(entity.CompanyName) && entity.CompanyName.Length > 50)
                return ValidationError("Account holder name cann't be more than 50 characters.");
            
            if (!string.IsNullOrWhiteSpace(entity.OwnerName) && entity.OwnerName.Length >50)
                return ValidationError("Owner name cann't be more than 50 characters.");

            if (!string.IsNullOrWhiteSpace(entity.OfficialEmail) && Validation.EmailAddress(entity.OfficialEmail))
                return ValidationError("Invalid Official email Id.");
            if (!string.IsNullOrWhiteSpace(entity.OfficialEmail) && entity.OfficialEmail.Length > 50)
                return ValidationError("Official email cann't be more than 50 characters.");
            if (!string.IsNullOrWhiteSpace(entity.OfficeAddress) && entity.OfficeAddress.Length > 100)
                return ValidationError("Office address cann't be more than 50 characters.");
            
            if (CurrentUser.User != null && !string.IsNullOrWhiteSpace(CurrentUser.User.UserId))
            {
                int userId = Validation.DecryptToInt(CurrentUser.User.UserId);
                if (userId <= 0)
                    return ValidationError("Something went wrong.Please try again.");
                entity.UserId = userId;
            }

            using (var uow = new UnitOfWork())
            {
                uow.UserProfileBL.UpdateBusinessDetail(entity);
                uow.Commit();
                return Json(new { Success = true, Message = "Business Details Update Successfully." });
            }
        }

        private JsonResult ValidationError(string message)
        {
            return Json(new { Success = false, ErrorMessage = message });
        }


        [HttpPost]
        public ActionResult UploadGallery()
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
              //var supportedTypes = new[] { ".png", ".jpg", "jpeg", ".gif", ".bmp" };
                string fileName = Path.GetFileNameWithoutExtension(postedFile.FileName);
                string fileExt = Path.GetExtension(postedFile.FileName);
                    // string filePath = FileManager.FilePath(fileName);
                    FileManager.S3Response reponse = FileManager.Upload(postedFile.InputStream, FileManager.S3Folder.Property, fileName, fileExt);
                    //   postedFile.SaveAs(Server.MapPath("~/" + filePath));

                    return Json(new { Success = true, Id = Request.Form["Id"], Url = reponse.FileUrl, File = reponse.FileName });
                

            }
            return Json(new { Success = false, Error = "File not found." });
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult RemoveGalleryFile(string imagePath)
        {
            if (!string.IsNullOrEmpty(imagePath) && !string.IsNullOrEmpty(CurrentUser.User.UserId))
            {
                if (FileManager.Delete(imagePath))
                    return Json(new { Success = true });
            }
            return Json(new { Success = false, ErrorMessage = "Invalid request." });
        }


    }
}