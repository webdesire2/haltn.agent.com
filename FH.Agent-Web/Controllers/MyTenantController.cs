﻿using FH.User_Web;
using FH.Web.Security;
using System.Web.Mvc;
using FH.User.Entities;
using FH.User.Service;
using FH.Util;
using FH.Agent_Web.ViewModel;
using System.Collections.Generic;
using System;
using System.IO;
using Amazon.DynamoDBv2.DocumentModel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Linq;
using Newtonsoft.Json;

namespace FH.Agent_Web.Controllers
{
    [CustomAuthorize]
    public class MyTenantController : BaseController
    {
        private readonly TenantBL _tenantBL;
        public MyTenantController()
        {
            _tenantBL = new TenantBL();
        }


        [HttpGet]
        public ActionResult GetAddTenantModal(int pid)
        {
            if (pid > 0)
            {
                AddTenantVM de = new AddTenantVM()
                {
                    PropertyId = pid
                };

                TenantMaster de1 = new TenantMaster()
                {
                    PropertyId = pid,
                    CheckInDate = DateTime.Now
                };
                 
                de.PGRoomData = _tenantBL.getAvailableRooms(de1);

                if (Request.Browser.IsMobileDevice)
                return PartialView("_AddNewTenant_Mobile", de);
                else
                    return PartialView("_AddNewTenant_Desktop", de);
            }
            else
                return JsonResultHelper.Error("Something went wrong.");
        }

        [HttpPost]
        public ActionResult GetAvailableRooms(TenantMaster req)
        {
            TenantMaster de = new TenantMaster()
            { 
                PropertyId = req.PropertyId,
                CheckInDate = req.CheckInDate
            }; 
            var result = _tenantBL.getAvailableRooms(de);
            if (result != null)
            {
                var result1 = JsonConvert.SerializeObject(result);
                return JsonResultHelper.Success(result1);
            }
            else
            { 
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }


        [HttpGet]
        public ActionResult AddNew()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddNewTenant(AddTenantVM req)
        {
            string validationMessage = AddTenantValidation.Validate(req);
            if (validationMessage != string.Empty)
                return JsonResultHelper.Error(validationMessage);

            int currentUser = Validation.DecryptToInt(CurrentUser.User.UserId);
            List<PaymentDetail> lstPaymentDetail = new List<PaymentDetail>();
            List<TenantPaymentDetail> lstTenantPayment = new List<TenantPaymentDetail>();

            if (req.SecurityDeposit > 0)
            {
                lstPaymentDetail.Add(new PaymentDetail {
                    CreatedBy = currentUser, Amount = req.SecurityDeposit,
                    Status = PaymentStatusEnum.SUCCESS.ToString(), PaymentMode = "CASH", CreatedOn = DateTime.Now,
                    PaymentType ="SECURITY"
                });

                lstTenantPayment.Add(new TenantPaymentDetail {
                    DepositAmount=req.SecurityDeposit,
                    PaymentType=TenantPaymentTypeEnum.SECURITY.ToString(),
                    PaymentStatus=(int)PaymentStatusEnum.SUCCESS,
                    CreatedBy=currentUser,CreatedOn=DateTime.Now
                });
            } 
            
            TenantMaster de = new TenantMaster()
            {
                PropertyId=req.PropertyId,
                Name=req.Name,
                Mobile=req.Mobile,
                RoomId = req.RoomId,
                Occupancy = req.Occupancy,
                Email =req.Email, 
                SecurityAmount=req.SecurityAmount,
                MonthlyRent=req.MonthlyRent,
                CheckInDate=req.CheckInDate,
                SecurityDeposit=req.SecurityDeposit,
                CreatedBy=currentUser,
                PaymentDetails=lstPaymentDetail,
                TenantPaymentDetails=lstTenantPayment
            };

            if (req.SecurityDeposit == 0) {
                de.PaymentDetails = null;
                de.TenantPaymentDetails = null;
            }
            
            TenantBL bl = new TenantBL();            
               int id=bl.AddTenant(de);

            if (id > 0)
                return JsonResultHelper.Success("Tenant added successfully.");
           
                return JsonResultHelper.Error("Something went wrong.");
        }

        [HttpGet]
        public ActionResult PropertyMap(PropertyMapVM req)
        {
            TenantMaster de = new TenantMaster()
            {
                PropertyId = req.Pid,
                CheckInDate = DateTime.Now,
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
            };
            var result = _tenantBL.getAvailableRooms(de);
            var property = _tenantBL.getMyProperties(de);
            //var tenant = _tenantBL.GetAllTenant(de);
            if (result.Count() > 0) {
                var ddd = result.FirstOrDefault();
                req.Pid = ddd.PropertyId; 
            }  

            req.PropertyList = property;
            req.PGRoomData = result;
             
            return View(req);
        }

        [HttpGet]
        public ActionResult TenantList(TenantMaster req)
        {
            ViewBag.PropertyId = req.PropertyId;
            ViewBag.Mobile = req.Mobile;
            ViewBag.IsActive1 = req.IsActive1;
            ViewBag.RoomNo = req.RoomNo;
            TenantMaster de = new TenantMaster() {CreatedBy= Validation.DecryptToInt(CurrentUser.User.UserId),PropertyId=req.PropertyId,Mobile=req.Mobile, IsActive1 = req.IsActive1, RoomNo = req.RoomNo };

            TenantMaster owner = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId) };
            ViewBag.PorpertyList = _tenantBL.getMyProperties(owner);
             
            var data=_tenantBL.GetAllTenant(de);

            var mytanent = _tenantBL.GetRentPaidTanent(de);

            foreach (var singleData in data)
            {
                singleData.RentPaid = 0;
                foreach (var singletanent in mytanent)
                {
                    if (singletanent.TenantId == singleData.TenantId)
                    {
                        singleData.RentPaid = 1;
                    }
                }
            }

            return View(data);
        }


        [HttpGet]
        public ActionResult TenantListTest(TenantMaster req)
        {
            ViewBag.PropertyId = req.PropertyId;
            ViewBag.Mobile = req.Mobile;
            ViewBag.IsActive1 = req.IsActive1;
            ViewBag.RoomNo = req.RoomNo;
            TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), PropertyId = req.PropertyId, Mobile = req.Mobile, IsActive1 = req.IsActive1, RoomNo = req.RoomNo };
            var data = _tenantBL.GetAllTenant(de);
            var mytanent = _tenantBL.GetRentPaidTanentTest(de);

            foreach (var singleData in data)
            { 
                singleData.RentPaid = 0;
                foreach (var singletanent in mytanent)
                {
                    if (singletanent.TenantId == singleData.TenantId)
                    {
                        singleData.RentPaid = 1;
                    }
                }
            }

            return View(data);
        }

        [HttpGet]
        public ActionResult MyBalance(TenantMaster req)
        {
            ViewBag.PropertyId = req.PropertyId;
            ViewBag.Mobile = req.Mobile;
            ViewBag.IsActive1 = req.IsActive1;
            ViewBag.RoomNo = req.RoomNo;
            ViewBag.SearchTotalRentAmount = 0;
            ViewBag.SearchPaidRentAmount = 0;
            ViewBag.SearchTotalSecurityAmount = 0;
            ViewBag.SearchPaidSecurityAmount = 0;

            TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), PropertyId = req.PropertyId, Mobile = req.Mobile, IsActive1 = req.IsActive1, RoomNo = req.RoomNo };
            var data = _tenantBL.GetAllTenantDetails(de);
           // var mytanent = _tenantBL.GetRentPaidTanent(de);

            foreach (var singleData in data)
            {
                singleData.RentPaid = 0;
                var checkOutDate = DateTime.ParseExact("0001-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //DateTime('01/01/0001 00:00:00');
                var nocheckout = true;
                if (singleData.CheckOutDate != null)
                {
                    nocheckout = false;
                    checkOutDate = (DateTime)singleData.CheckOutDate;
                }
                var CheckInDate = (DateTime)singleData.CheckInDate;




                if (CheckInDate.Month == DateTime.Now.Month && CheckInDate.Year == DateTime.Now.Year)
                {
                    var rentPerDay = singleData.MonthlyRent > 0 ? singleData.MonthlyRent / 30 : 0;
                    var fromDate = CheckInDate;

                    singleData.FromDate = fromDate;
                    singleData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                    int days = (30 - singleData.FromDate.Day) + 1;
                    //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                    singleData.ThisMonthRent = Math.Round(rentPerDay * days, 2);
                    if (singleData.ThisMonthRent < 1) { singleData.ThisMonthRent = 1; }
                }
                else if (checkOutDate.Month == DateTime.Now.Month && checkOutDate.Year == DateTime.Now.Year)
                {
                    var lastDay = new DateTime(checkOutDate.Year, checkOutDate.Month, DateTime.DaysInMonth(checkOutDate.Year, checkOutDate.Month));
                    var rentPerDay = singleData.MonthlyRent > 0 ? singleData.MonthlyRent / lastDay.Day : 0;
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    singleData.FromDate = fromDate;
                    singleData.ToDate = checkOutDate;
                    int days = singleData.ToDate.Day;
                    singleData.ThisMonthRent = Math.Round(rentPerDay * days, 2);
                    if (singleData.ThisMonthRent < 1) { singleData.ThisMonthRent = 1; }
                }
                else
                {
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    singleData.FromDate = fromDate;
                    singleData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                    singleData.ThisMonthRent = singleData.MonthlyRent;
                }
                if (singleData.ThisMonthRent == singleData.ThisMonthRentPaid)
                {
                    singleData.RentPaid = 1;
                }


                if ((CheckInDate.Month > DateTime.Now.Month || CheckInDate.Year > DateTime.Now.Year))
                {
                    singleData.ThisMonthRent = 0;
                }

                if (nocheckout == false && (checkOutDate.Month < DateTime.Now.Month || checkOutDate.Year < DateTime.Now.Year))
                {
                    singleData.ThisMonthRent = 0;
                }

                ViewBag.SearchTotalRentAmount += singleData.ThisMonthRent;
                ViewBag.SearchPaidRentAmount += singleData.ThisMonthRentPaid;
                ViewBag.SearchTotalSecurityAmount += singleData.SecurityAmount;
                ViewBag.SearchPaidSecurityAmount += singleData.SecurityDeposit;
            }

            ViewBag.SearchTotalRentAmount = Math.Round(ViewBag.SearchTotalRentAmount, 2);
            ViewBag.SearchPaidRentAmount = Math.Round(ViewBag.SearchPaidRentAmount, 2);
            ViewBag.SearchRemaingRentAmount = Math.Round((ViewBag.SearchTotalRentAmount - ViewBag.SearchPaidRentAmount), 2);
            ViewBag.SearchTotalSecurityAmount = Math.Round(ViewBag.SearchTotalSecurityAmount, 2);
            ViewBag.SearchPaidSecurityAmount = Math.Round(ViewBag.SearchPaidSecurityAmount, 2);
            ViewBag.SearchRemaingSecurityAmount = Math.Round((ViewBag.SearchTotalSecurityAmount - ViewBag.SearchPaidSecurityAmount), 2);

            return View(data);
        }

        [HttpGet]
        public ActionResult TenantListNew(TenantMaster req)
        {
            ViewBag.PropertyId = req.PropertyId;
            ViewBag.Mobile = req.Mobile;
            ViewBag.IsActive1 = req.IsActive1;
            ViewBag.RoomNo = req.RoomNo;
            ViewBag.SearchTotalRentAmount = 0;
            ViewBag.SearchPaidRentAmount = 0;
            ViewBag.SearchTotalSecurityAmount = 0;
            ViewBag.SearchPaidSecurityAmount = 0;
            ViewBag.PorpertyList = "fffs";
            TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), PropertyId = req.PropertyId, Mobile = req.Mobile, IsActive1 = req.IsActive1, RoomNo = req.RoomNo };
            TenantMaster owner = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)};
            var data = _tenantBL.GetAllTenant(de);
            ViewBag.PorpertyList = _tenantBL.getMyProperties(owner);
            var mytanent = _tenantBL.GetRentPaidTanent(de);
            if (data.Count() > 0) {
                foreach (var singleData in data)
                {
                    singleData.RentPaid = 0;
                    var checkOutDate = DateTime.ParseExact("0001-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //DateTime('01/01/0001 00:00:00');
                    var nocheckout = true;
                    if (singleData.CheckOutDate != null)
                    {
                        nocheckout = false;
                        checkOutDate = (DateTime)singleData.CheckOutDate;
                    }
                    var CheckInDate = (DateTime)singleData.CheckInDate; 

                    if (CheckInDate.Month == DateTime.Now.Month && CheckInDate.Year == DateTime.Now.Year)
                    {
                        var rentPerDay = singleData.MonthlyRent > 0 ? singleData.MonthlyRent / 30 : 0;
                        var fromDate = CheckInDate;

                        singleData.FromDate = fromDate;
                        singleData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                        int days = (30 - singleData.FromDate.Day) + 1;
                        //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                        singleData.ThisMonthRent = Math.Round(rentPerDay * days, 2);
                        if (singleData.ThisMonthRent < 1) { singleData.ThisMonthRent = 1; }
                    }
                    else if (checkOutDate.Month == DateTime.Now.Month && checkOutDate.Year == DateTime.Now.Year)
                    {
                        var lastDay = new DateTime(checkOutDate.Year, checkOutDate.Month, DateTime.DaysInMonth(checkOutDate.Year, checkOutDate.Month));
                        var rentPerDay = singleData.MonthlyRent > 0 ? singleData.MonthlyRent / lastDay.Day : 0;
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        singleData.FromDate = fromDate;
                        singleData.ToDate = checkOutDate;
                        int days = singleData.ToDate.Day;
                        singleData.ThisMonthRent = Math.Round(rentPerDay * days, 2);
                        if (singleData.ThisMonthRent < 1) { singleData.ThisMonthRent = 1; }
                    }
                    else
                    {
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        singleData.FromDate = fromDate;
                        singleData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                        singleData.ThisMonthRent = singleData.MonthlyRent;
                    }
                    if (singleData.ThisMonthRent == singleData.ThisMonthRentPaid)
                    {
                        singleData.RentPaid = 1;
                    }

                    var NoOfDayAdd = ((DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day) + 1);
                    var NextDate = DateTime.Now.AddDays(NoOfDayAdd);
                    var LastDate = DateTime.Now.AddDays(-(DateTime.Now.Day));
                    if ((CheckInDate >= NextDate))
                    {
                        singleData.ThisMonthRent = 0;
                    }

                    if (nocheckout == false && (checkOutDate <= LastDate))
                    {
                        singleData.ThisMonthRent = 0;
                    }

                    ViewBag.SearchTotalRentAmount += singleData.ThisMonthRent;
                    ViewBag.SearchPaidRentAmount += singleData.ThisMonthRentPaid;
                    ViewBag.SearchTotalSecurityAmount += singleData.SecurityAmount;
                    ViewBag.SearchPaidSecurityAmount += singleData.SecurityDeposit;
                }
            
                
            ViewBag.SearchTotalRentAmount = Math.Round(ViewBag.SearchTotalRentAmount, 2);
            ViewBag.SearchPaidRentAmount = Math.Round(ViewBag.SearchPaidRentAmount, 2);
            ViewBag.SearchRemaingRentAmount = Math.Round((ViewBag.SearchTotalRentAmount - ViewBag.SearchPaidRentAmount), 2);
            ViewBag.SearchTotalSecurityAmount = Math.Round(ViewBag.SearchTotalSecurityAmount, 2);
            ViewBag.SearchPaidSecurityAmount = Math.Round(ViewBag.SearchPaidSecurityAmount, 2);
            ViewBag.SearchRemaingSecurityAmount = Math.Round((ViewBag.SearchTotalSecurityAmount - ViewBag.SearchPaidSecurityAmount), 2);
            }
            return View(data);
        }

        [HttpGet]
        public ActionResult EditTenant(int TenantId)
        {
            TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = TenantId };
            var data = _tenantBL.GetSingleTenant(de);

            EditTenantVM de1 = new EditTenantVM();
            de1.TenantMaster = data;
            var signletenent = data.FirstOrDefault();
            TenantMaster roomDetails = new TenantMaster()
            {
                PropertyId = signletenent.PropertyId,
                CheckInDate = DateTime.Now
            };
            de1.PGRoomData = _tenantBL.getAvailableRooms(roomDetails);
             
            roomDetails.RoomId = signletenent.RoomId; 
            de1.TenantRoom = _tenantBL.getTenantRooms(roomDetails);

            return View(de1); 
        }

        [HttpPost]
        public ActionResult EditTenant(TenantMaster req)
        {

            TenantMaster de = new TenantMaster()
            {
                TenantId = req.TenantId,
                Name = req.Name,
                Mobile = req.Mobile, 
                RoomNo = req.RoomNo,
                RoomId = req.RoomId,
                Occupancy = req.Occupancy,
                Email = req.Email,
                SecurityAmount = req.SecurityAmount,
                MonthlyRent = req.MonthlyRent,
                CheckOutDate = req.CheckOutDate,
                IsActive = req.IsActive,
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),    
            };

            TenantBL bl = new TenantBL();
            int id = bl.EditTenant(de);
            return JsonResultHelper.Success("Tenant Edited successfully.");
        }


        [HttpPost]
        public ActionResult CollectPayment(CollectCashData req)
        {
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "RENT",
                TenantId = req.TenantId,
                StatusMessage = req.SRemark,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.CollectPayment(data);
            if (result != null)
            {
                return JsonResultHelper.Success("Payment Collected");
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpPost]
        public ActionResult RSendPaymentLink(CollectCashData req)
        {
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "RENT",
                TenantId = req.TenantId,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.RSendPaymentLink(data);

            if (result != null)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = req.TenantId };
                var tanentdta = _tenantBL.GetSingleTenant(de);
                var tanentdata = tanentdta.FirstOrDefault();
                var message = "Dear " + tanentdata.Name + ", Bill from Haltn for Rs. " + req.rentAmount + " for rent. Click2pay haltn.com/user/pbl/" + result.Id.ToString() + " for RENT to continue reliable services";
                //var message = "Dear " + tanentdata.Name + ", %0aBill from Telonestay for Rs. " + req.rentAmount + " (Rent). Click2pay telonestay.com/user/Pbl/" + result.Id.ToString() + " to continue reliable services.";
                string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString(); 
                SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05); 
                 
                return JsonResultHelper.Success(result.Id.ToString());
            } 
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpPost]
        public ActionResult SSendPaymentLink(CollectCashData req)
        { 
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "SECURITY", 
                TenantId = req.TenantId,
                RentForMonthStartDate = DateTime.Now ,
                RentForMonthEndDate = DateTime.Now, 
            };

            var result = _tenantBL.SSendPaymentLink(data);

            if (result != null)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = req.TenantId };
                var tanentdta = _tenantBL.GetSingleTenant(de);
                var tanentdata = tanentdta.FirstOrDefault();

                var message = "Dear " + tanentdata.Name + ", Bill from Haltn for Rs. " + req.rentAmount + " for rent. Click2pay haltn.com/user/pbl/" + result.Id.ToString() + " for SECURITY to continue reliable services";
                //var message = "Dear " + tanentdata.Name + ", %0aBill from Telonestay for Rs. " + req.rentAmount + " (Security). Click2pay telonestay.com/user/Pbl/" + result.Id.ToString() + " to continue reliable services.";
                string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString();
                SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05);

                return JsonResultHelper.Success(result.Id.ToString());
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }


        [HttpPost]
        public ActionResult RefundSecurity(CollectCashData req)
        {
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = -(req.rentAmount),
                PaymentType = "SECREFUND",
                TenantId = req.TenantId,
                StatusMessage = req.SRemark,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.RefundSecurity(data);
            if (result != null)
            {
                return JsonResultHelper.Success("Payment Refund");
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpPost]
        public ActionResult CollectSecurity(CollectCashData req)
        {
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "SECURITY",
                TenantId = req.TenantId,
                StatusMessage = req.SRemark,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.CollectSecurity(data);
            if (result != null)
            {
                return JsonResultHelper.Success("Payment Collected");
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpGet]
        public ActionResult GetTenantPaymentLog(int tenantId=0)
        {            
            if(tenantId>0)
            { 
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),TenantId=tenantId };
                var lstLog= _tenantBL.GetTenantPaymentLog(de);



                //TanentNewPayment - This is Cash Collection Start
                var tenantData = _tenantBL.GetTanentStayDetails(de);

                if (tenantData != null)
                {
                    tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;

                    if (tenantData.CheckInDate.Month == DateTime.Now.Month && tenantData.CheckInDate.Year == DateTime.Now.Year)
                    {
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                        var fromDate = tenantData.CheckInDate;

                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                        int days = (30 - tenantData.FromDate.Day) + 1;
                        //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                        tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                        if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                    } else if (tenantData.CheckOutDate.Month == DateTime.Now.Month && tenantData.CheckOutDate.Year == DateTime.Now.Year) {
                        var lastDay  = new DateTime(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month, DateTime.DaysInMonth(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month));
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate; 
                        tenantData.ToDate = tenantData.CheckOutDate;  
                        int days = tenantData.ToDate.Day; 
                        tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                        if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                    }
                    else  {
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                    }

                    foreach (var item in lstLog)
                    {
                        if (item.RentForMonthEndDate == tenantData.ToDate && item.RentForMonthStartDate == tenantData.FromDate && item.DepositAmount == tenantData.MonthlyRent && item.PaymentStatus == "True")
                        {
                            tenantData.MonthlyRent = 0; break;
                        }
                    }

                }

                //TanentNewPayment - This is Cash Collection Start


                VMPaymentDetails result = new VMPaymentDetails();
                result.TenantPaymentHistory = lstLog;
                result.TanentStayDetails = tenantData;

                if (Request.Browser.IsMobileDevice)
                return PartialView("_Tenant_Payment_Log_Mobile", result);
                else
                    return PartialView("_Tenant_Payment_Log_Desktop", result);
            }

            return JsonResultHelper.Error("Invalid input.");
        }

        [HttpGet]
        public ActionResult DownloadInvoice(int invoiceId)
        {
            PaymentBL _paymentBL = new PaymentBL();
            var currentuse = Validation.DecryptToInt(CurrentUser.User.UserId);
            var data = _paymentBL.GetTenantInvoice(invoiceId, Validation.DecryptToInt(CurrentUser.User.UserId));
            var invoiceHtml = RenderPartialToString(PartialView("_PaymentInvoice", data));
             
            using (MemoryStream stream = new System.IO.MemoryStream()) 
            {  
                StringReader sr = new StringReader(invoiceHtml);
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();  
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                string invoiceName = "Pay Slip-" + invoiceId + ".pdf";
                return File(stream.ToArray(), "application/pdf", invoiceName);
            }
        }

    }
}