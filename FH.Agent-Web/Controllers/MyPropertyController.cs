﻿using System.Collections.Generic;
using FH.User.Entities;
using System.Web.Mvc;
using FH.User.Service;
using System;
using FH.Web.Security;
using FH.User_Web;
using System.Configuration;
using FH.Util;
using FH.Agent.Web.ViewModel;

namespace FH.Agent_Web.Controllers
{
    [CustomAuthorize]
    public class MyPropertyController :BaseController
    {
        [HttpGet]
        public ActionResult Property()
        {            
            return View();
        }

        [HttpPost]
        public PartialViewResult GetProperty(PropertySearchParameter input)
        {
            bool isMobile = Request.Browser.IsMobileDevice;
            int pageSize = Convert.ToInt16(ConfigurationManager.AppSettings["PropertyPageSize"]);
            int totalRecord;
            VMPropertyListing vmProperty = new VMPropertyListing();           
            if (!string.IsNullOrWhiteSpace(input.PropertyStatus))
            {                
                using (var uow = new UnitOfWork())
                {
                    input.PageSize = pageSize;
                    input.PageIndex = input.PageIndex == 0 ? 1 : input.PageIndex;
                    vmProperty.PropertyList = uow.PropertyBL.GetAllProperty(input,Convert.ToInt32(EncreptDecrpt.Decrypt(CurrentUser.User.UserId)),out totalRecord);
                    vmProperty.Paging = new Pager(totalRecord, input.PageIndex == 0 ? 1 : input.PageIndex, pageSize);                    
                }
            }
            vmProperty.propertySearchInput = input;
            return PartialView((isMobile? "_MyProperty_List_Mobile": "_MyProperty_List_Desktop"),vmProperty);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public PartialViewResult GetPropertyLead(int propertyId)
        {
            bool isMobile = Request.Browser.IsMobileDevice;            
            if(propertyId>0)
            {
                using (var uow = new UnitOfWork())
                {
                    return PartialView((isMobile?"_MyProperty_Lead_Mobile":"_MyProperty_Lead_Desktop"),uow.PropertyLeadBL.GetAllLead(propertyId, Validation.DecryptToInt(CurrentUser.User.UserId)));
                }
            }
            return PartialView((isMobile ? "_MyProperty_Lead_Mobile" : "_MyProperty_Lead_Desktop"), new List<PropertyLead>());
        }
       
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult GetPropertyStatusView(int propertyId=0)
        {
            if(propertyId>0)
            {
                using (var uow = new UnitOfWork())
                {
                   string status=uow.PropertyBL.GetPropertyStatus(propertyId);
                    if (!string.IsNullOrEmpty(status) && !Request.Browser.IsMobileDevice)                    
                        return PartialView("_MyProperty_Status", new VMMyPropertyStatus { Status = status, PropertyId = propertyId });
                    if (!string.IsNullOrEmpty(status) && Request.Browser.IsMobileDevice)
                        return PartialView("_MyProperty_Status_Mobile", new VMMyPropertyStatus { Status = status, PropertyId = propertyId });

                }
            }
            return Json(new {Success=false});
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult UpdatePropertyStatus(int propertyId=0,string status="")
        {    
            if (propertyId > 0 && !string.IsNullOrWhiteSpace(status) && PropertyStatus.GetAll().ContainsKey(status))
            {                    
                using (var uow = new UnitOfWork())
                {
                    PropertyMaster de = new PropertyMaster()
                    {
                        PropertyId = propertyId,
                        PropertyStatusByFH = status,
                        CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
                    };

                   bool result=uow.PropertyBL.UpdatePropertyStatus(de);
                    uow.Commit();
                    if (result)
                        return Json(new {Success=true,Message="Property status update successfully."});
                }
            }

            return Json(new { Success = false, Message ="Something went wrong."});
        }
    }
}