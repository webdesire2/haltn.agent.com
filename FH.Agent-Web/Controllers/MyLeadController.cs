﻿using FH.User.Service;
using FH.User_Web;
using FH.Web.Security;
using System.Web.Mvc;
using FH.User.Entities;
using System.Collections.Generic;

namespace FH.Agent_Web.Controllers
{
    [CustomAuthorize]
    public class MyLeadController: BaseController
    {
        [HttpGet]
        public ActionResult Lead()
        {
            using (var uow = new UnitOfWork())
            {
                return View(uow.PropertyLeadBL.GetAllMyLead(Validation.DecryptToInt(CurrentUser.User.UserId)));
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult AddNewLead(PropertyLead input)
        {
            if (string.IsNullOrWhiteSpace(input.VisitorName))
                return ValidationError("Name required.");
            else if (input.VisitorName.Length > 50)
                return ValidationError("Name can't be more than 50 characters.");
            if (string.IsNullOrWhiteSpace(input.VisitorMobile))
                return ValidationError("Mobile no required.");
            else if (Validation.MobileNo(input.VisitorMobile))
                return ValidationError("Invalid mobile no.");

            if (!string.IsNullOrWhiteSpace(input.VisitorEmail) && input.VisitorEmail.Length > 50)
                return ValidationError("Email can't be more than 50 characters.");
            if (!string.IsNullOrWhiteSpace(input.VisitorEmail) && Validation.EmailAddress(input.VisitorEmail))
                return ValidationError("Invalid email.");

            if (string.IsNullOrWhiteSpace(input.VisitorRequirement))
                return ValidationError("Client requirement required.");
            else if (input.VisitorRequirement.Length > 500)
                return ValidationError("Client requirement can't be more than 500 characters.");

            using (var uow = new UnitOfWork())
            {
                input.UserId = Validation.DecryptToInt(CurrentUser.User.UserId);
                string result = uow.PropertyLeadBL.AddNewLead(input);
                if (string.IsNullOrWhiteSpace(result))
                {
                    uow.Commit();
                    return Json(new { Success = true, Message = "Lead added successfully." });
                }
                else
                    return Json(new { Success = false, ErrorMessage = result });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public PartialViewResult GetLeadLog(int lead)
        {
            bool isMobile = Request.Browser.IsMobileDevice;
            if (lead > 0)
            {
                using (var uow = new UnitOfWork())
                {
                    return PartialView((isMobile?"_MyLead_Log_Mobile":"_MyLead_Log_Desktop"), uow.PropertyLeadBL.GetLeadLog(Validation.DecryptToInt(CurrentUser.User.UserId), lead));
                }
            }
            return PartialView((isMobile ? "_MyLead_Log_Mobile" : "_MyLead_Log_Desktop"), new List<PropertyList>());
        }

        private JsonResult ValidationError(string msg)
        {
            return Json(new { Success = false, ErrorMessage = msg });
        }
    }
}