﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.User.Entities;
using FH.User.Service;
using FH.Agent_Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using CCA.Util;
using Razorpay.Api;
using System.Globalization;

namespace FH.Agent_Web.Controllers
{
    public class PaymentController : Controller
    {
        [HttpPost]
        public new ActionResult Response()
        {
            string CCAWorkingKey = ConfigurationManager.AppSettings["CCAWorkingKey"].ToString();

            try
            {
                CCACrypto ccaCrypto = new CCACrypto();
                string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], CCAWorkingKey);
                NameValueCollection Params = new NameValueCollection();
                string[] segments = encResponse.Split('&');
                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        Params.Add(Key, Value);
                    }
                }

                int PaymentStatus = 0;
                string RpaymentStatus = Params["order_status"];
                if (RpaymentStatus == "Success")
                {
                    PaymentStatus = 1;
                } 

                PaymentBL _paymentBL = new PaymentBL();
                PlanMaster de = new PlanMaster()
                {
                    PlanOrderID =  Int32.Parse(Params["order_id"]),  // Int32.TryParse(TextBoxD1.Text, out x);
                    Status = PaymentStatus,  //responseCode == "E000" ? "SUCCESS" : "FAILURE"
                    TracingId = Params["tracking_id"],
                    BankRefNo = Params["bank_ref_no"],  
                    Payment = float.Parse(Params["amount"], CultureInfo.InvariantCulture.NumberFormat),
                    PaymentMode = Params["payment_mode"], //method
                    StatusCode = Params["status_code"], 
                    StatusMessage = Params["status_message"],
                }; 

                var responce = _paymentBL.UpdatePayment(de);
                return RedirectToAction("Response", new { pid = Params["order_id"] });
            } 
            catch (Exception e)
            {
                ViewBag.Error = e.InnerException;
            }
            return RedirectToAction("Response", new { pid = 0 });
        }

        [HttpGet]
        public new ActionResult Response(int pid = 0)
        {
            if (pid > 0)
            {
                PlanBL _PlanBL = new PlanBL();
                PlanMaster de = new PlanMaster()
                {
                    PlanOrderID = pid,
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
                };
                VMMyPlan plan = new VMMyPlan();
                var responce = _PlanBL.FetchOrder(de);
                plan.MyOrders = responce;
                return View(plan);
            }
            return PartialView("_Error");
        }
    }
}