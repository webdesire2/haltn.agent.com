﻿using System.Web.Mvc;
using FH.User.Entities;
using FH.User.Service;
using Newtonsoft.Json;
using System.Web.Security;
using System;
using System.Web;
using FH.Web.Security;
using FH.Util;
using FH.Agent_Web;

namespace FH.User_Web.Controllers
{
    public class UserController : BaseController
    {
        public static void AuthCookie(UserDetail userDetail, HttpRequestBase request, HttpResponseBase response)
        {
            userDetail.UserId = EncreptDecrpt.Encrypt(userDetail.UserId);
            string userData = JsonConvert.SerializeObject(userDetail);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                1,
                userDetail.Mobile,
                DateTime.Now,
                DateTime.Now.AddDays(30),
                false,
                userData);

            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            if (!request.IsLocal)
                faCookie.Domain = ".haltn.com";
            faCookie.Expires = DateTime.Now.AddDays(30);
            response.Cookies.Add(faCookie);
            //formsAuthentication.SetAuthCookie(userDetail);
        }


        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult RegistrationAjax(UserDetail user)
        {
            if (string.IsNullOrWhiteSpace(user.Name))
                return ValidationError("Name required.");
            else if (user.Name.Length > 50)
                return ValidationError("Name can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.Mobile))
                return ValidationError("Mobile no required.");
            else if (Validation.MobileNo(user.Mobile))
                return ValidationError("Invalid mobile no.");

            if (string.IsNullOrWhiteSpace(user.Email))
                return ValidationError("Email required.");
            else if (Validation.EmailAddress(user.Email))
                return ValidationError("Invalid email.");
            else if (user.Email.Length > 50)
                return ValidationError("Email can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.Password))
                return ValidationError("Password required.");
            else if (user.Password.Length < 5)
                return ValidationError("Password should be minimum 5 characters.");
            else if (user.Password.Length > 50)
                return ValidationError("Password can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.UserType))
                return ValidationError("User type required.");
            else if (user.UserType != "Buyer" && user.UserType != "Owner" && user.UserType != "Agent")
                return ValidationError("Invalid user type.");

            using (var unitOfWork = new UnitOfWork())
            {
                UserDetail userDetail = unitOfWork.UserBL.FindByEmailMobile(user);
                if (userDetail != null) {
                    if (userDetail.UserType == "Owner")
                    {
                        if (userDetail.Mobile == user.Mobile && userDetail.Email == user.Email)
                            return ValidationError("Mobile no & email id already exist.");
                        else if (userDetail.Mobile == user.Mobile)
                            return ValidationError("mobile no already exist.");
                        else
                            return ValidationError("email id already exist.");
                    }
                    else
                    {
                        user.UserId = userDetail.UserId; 
                        Random rdmNo = new Random();
                        int OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                        user.OTP = OTP;
                        user.Password = EncreptDecrpt.Encrypt(user.Password);
                        user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");
                        
                        int userId = unitOfWork.UserBL.UpdateUser(user);
                        if (userId > 0)
                        {
                            unitOfWork.Commit();

                            string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
                            SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);
                            return Json(new { Success = true, User = EncreptDecrpt.Encrypt(userId.ToString()) });
                        }
                        return ValidationError("Something went wrong, please try again.");
                    }
                } else
                {
                    Random rdmNo = new Random();
                    int OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                    user.OTP = OTP;
                    user.Password = EncreptDecrpt.Encrypt(user.Password);
                    user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");
                    int userId = unitOfWork.UserBL.AddUser(user);
                    if (userId > 0)
                    {
                        unitOfWork.Commit();

                        string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
                        SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);
                        return Json(new { Success = true, User = EncreptDecrpt.Encrypt(userId.ToString()) });
                    }
                    return ValidationError("Something went wrong, please try again.");
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult VerifyOTP(UserDetail user)
        {
            if (user.OTP == null || user.OTP < 999 || user.OTP > 9999)
                return ValidationError("4 digit OTP required.");

            int userId = Validation.DecryptToInt(user.UserId);
            if (userId <= 0)
                return ValidationError("Something went wrong,Please try again.");

            using (var uow = new UnitOfWork())
            {
                UserDetail userDetail = uow.UserBL.IsValidOTP(userId, Convert.ToInt16(user.OTP), DateTime.Now);
                if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    uow.Commit();
                    if (!string.IsNullOrEmpty(userDetail.Message))
                        return Json(new { Success = false, ErrorMessage = userDetail.Message });
                    else
                    {
                        AddAuthCookie(userDetail);
                        return Json(new { Success = true });
                    }
                }
                else
                    return Json(new { Success = false, ErrorMessage = "Something went wrong,please try again." });
            }
        }



        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult IsMobileExist(string mobile)
        {
           

            if (string.IsNullOrWhiteSpace(mobile))
                return ValidationError("Mobile no required.");
            else if (Validation.MobileNo(mobile))
                return ValidationError("Invalid mobile no.");

            using (var uow = new UnitOfWork())
            {
                UserDetail user = new UserDetail();
                Random rdmNo = new Random();
                user.OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");
                user.Mobile = mobile;
                string userId = uow.UserBL.GetUserByMobile(user);

                if (!string.IsNullOrEmpty(userId) && Convert.ToInt32(userId) > 0)
                {
                    uow.Commit();
                    string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
                    SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + user.OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);

                    return Json(new { Success = true, User = EncreptDecrpt.Encrypt(userId.ToString()) });
                }
                else
                    return Json(new { Success = false, ErrorMessage = "Mobile no not exist." });
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult ResetPasswordAjax(UserDetail user)
        {
            int userId = Validation.DecryptToInt(user.UserId);
            if (userId <= 0)
                return ValidationError("Invalid input");

            if (string.IsNullOrWhiteSpace(user.Password))
                return ValidationError("Password required.");
            else if (user.Password.Length > 50)
                return ValidationError("Password can't be greater than 50 characters.");
            else if (user.Password.Length < 5)
                return ValidationError("Password can't be less than 5 characters.");

            if (string.IsNullOrWhiteSpace(user.ConfirmPassword))
                return ValidationError("Confirm password required.");
            else if (!user.Password.Equals(user.ConfirmPassword))
                return ValidationError("Confirm password not match.");

            if (user.OTP == null || user.OTP < 999 || user.OTP > 9999)
                return ValidationError("4 digit OTP required.");

            using (UnitOfWork uow = new UnitOfWork())
            {
                user.OTPExpiredOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                user.UserId = userId.ToString();
                user.Password = EncreptDecrpt.Encrypt(user.Password);
                UserDetail userDetail = uow.UserBL.ResetPassword(user);
                if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    uow.Commit();
                    if (!string.IsNullOrEmpty(userDetail.Message))
                        return Json(new { Success = false, ErrorMessage = userDetail.Message });
                    else
                    {
                        AddAuthCookie(userDetail);
                        return Json(new { Success = true });
                    }
                }
                else
                    return Json(new { Success = false, ErrorMessage = "Something went wrong,please try again." });
            }

        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);
            return RedirectToAction("postyourproperty", "property");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult LoginAjax(UserDetail entity)
        {
            if (string.IsNullOrWhiteSpace(entity.Mobile))
                return ValidationError("Mobile no required.");
            else if (Validation.MobileNo(entity.Mobile))
                return ValidationError("Invalid mobile no.");

            if (string.IsNullOrWhiteSpace(entity.Password))
                return ValidationError("Password required.");

            using (var unitOfWork = new UnitOfWork())
            {
               entity.Password = EncreptDecrpt.Encrypt(entity.Password);
               //entity.Password = EncreptDecrpt.Decrypt(entity.Password);


                UserDetail userDetail = unitOfWork.UserBL.IsValidLogin(entity);
                if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    UserController.AuthCookie(userDetail, Request, Response);
                    return Json(new { Success = true, userDetail = userDetail });
                }
                else
                    return ValidationError("Invalid Username & Password.");
            }
        }


        [NonAction]
        private void AddAuthCookie(UserDetail userDetail)
        {
            userDetail.UserId = EncreptDecrpt.Encrypt(userDetail.UserId);
            string userData = JsonConvert.SerializeObject(userDetail);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                1,
                userDetail.Mobile,
                DateTime.Now,
                DateTime.Now.AddDays(30),
                false,
                userData);

            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            if (!Request.IsLocal)
                faCookie.Domain = ".haltn.com";
            faCookie.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(faCookie);
        }

        [NonAction]
        private JsonResult ValidationError(string message)
        {
            return Json(new { Suceess = false, ErrorMessage = message });
        }

    }
}

