﻿using FH.User_Web;
using FH.Web.Security;
using System.Web.Mvc;
using FH.User.Entities;
using FH.User.Service;
using FH.Util;
using FH.Agent_Web.ViewModel;
using System.Collections.Generic;
using System;
using System.Linq;

namespace FH.Agent_Web.Controllers
{
    [CustomAuthorize]
    public class PlanController : BaseController
    {
        private readonly PlanBL _planBL;
        public PlanController()
        {
            _planBL = new PlanBL();
        }

        [HttpGet]
        public ActionResult MyPlans(VMMyPlan req)
        {
            PlanMaster de = new PlanMaster() { 
                PropertyId = req.PropertyId,
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), 
            }; 
            var order = _planBL.GetMyOder(de);
            var data = _planBL.GetMyPlans(de);

            req.MyPlans = data;
            req.MyOrders = order;
            return View(req);
        }

        [HttpGet]
        public ActionResult Packages(VMPackage req)
        {
            PlanMaster de = new PlanMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
            };
            var data = _planBL.GetPackages(de);
            var order = _planBL.GetMyOder(de);    //added
            var data1 = _planBL.GetMyPlans(de);  //added
            req.MyOrders = order;                //added
            req.MyPlans = data1;                //added

            req.Plans = data;
            return View(req);
        }

        [HttpGet]
        public ActionResult BuyPackage(VMBuyPackage req)
        {
            if (req.PlanId == 0) 
            {
                return RedirectToAction("Packages");
            }
            PlanMaster de = new PlanMaster()
            {
                PlanId = req.PlanId,
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
            }; 
            var data = _planBL.GetPackages(de);
            de.Category = data.FirstOrDefault().Category;
            var property = _planBL.GetMyPropertyNotByPackage(de);
            req.PropertyList = property;
            req.Plans = data;
            return View(req);
        }


        [HttpPost]
        public ActionResult BuyPackagePost(VMBuyPackage req)
        {
            PlanMaster de = new PlanMaster() 
            {
                PlanId = req.PlanId,
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
            };
            var plan = _planBL.GetPackages(de).First();

            if(plan != null && req.PropertyId.Length > 0) {

                PlanMaster PlanOrder = new PlanMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    // Payment = (plan.Price* req.PropertyId.Length)
                    Payment = req.Price
                };

                var PlanOrderDetails = _planBL.AddPlanOrder(PlanOrder).First();
                if (PlanOrderDetails != null) {
                    
                   double TotalAmount = 0;
                     
                    foreach (string PropertyID in req.PropertyId)
                    {
                        PlanMaster PropertyOrder = new PlanMaster()
                        {
                            CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                            PropertyId = Int32.Parse(PropertyID)
                        };
                        PropertyList PropertyDetails = new PropertyList();
                        PropertyDetails = _planBL.GetMyProperty(PropertyOrder).First();
                         
                        if (PropertyDetails != null) {
                            PlanMaster PlanDetails = new PlanMaster()
                            { 
                                PlanId = plan.PlanId,
                                NumberOfLeads = plan.NumberOfLeads,
                                PropertyId = Int32.Parse(PropertyDetails.PropertyId),
                                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                                VarifiedTag = plan.VarifiedTag,
                                //Payment = plan.Price,
                                Payment = req.Price,
                                Validity = plan.Validity,
                                PlanOrderID = PlanOrderDetails.PlanOrderID
                            };
                            int id = _planBL.AddPlanToVendor(PlanDetails);
                            if (id > 0) {
                                //  TotalAmount = TotalAmount + plan.Price;
                                TotalAmount = req.Price;
                            } else {
                                return JsonResultHelper.Error("Something went wrong.");
                            }
                        } 
                    }
                    if(TotalAmount == PlanOrderDetails.Payment){
                        // Razorpay 
                        
                        string RorderId = _planBL.EPPaymentRequest(TotalAmount, PlanOrderDetails.PlanOrderID);
                        string CCAccessCode = System.Configuration.ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                        VMPayment orderData = new VMPayment();
                        orderData.paymentID = RorderId;
                        orderData.Amount = TotalAmount;
                        orderData.razorpayKey = CCAccessCode;
                        orderData.currency = "INR"; 
                        orderData.name = CurrentUser.User.Name;
                        orderData.email = CurrentUser.User.Email;
                        orderData.contactNumber = CurrentUser.User.Mobile;
                        orderData.address = "";
                        orderData.description = "";

                        return PartialView("Rpayment", orderData);
                    }
                }

            }
            return JsonResultHelper.Error("Something went wrong.");
            //return JsonResultHelper.Success("Your Order successfully.");
        }

        [HttpPost]
        public ActionResult PayPackageAmount(VMBuyPackage req)
        {
            if (req.PlanOrderID > 0) 
            { 
                PlanMaster PlanOrder = new PlanMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    PlanOrderID = req.PlanOrderID
                };

                var PlanOrderDetails = _planBL.FetchOrder(PlanOrder).First();
                if (PlanOrderDetails != null)
                {
                    if (PlanOrderDetails.Payment > 0)
                    {
                        // Razorpay 

                        string RorderId = _planBL.EPPaymentRequest(PlanOrderDetails.Payment, PlanOrderDetails.PlanOrderID);
                        string CCAccessCode = System.Configuration.ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                        VMPayment orderData = new VMPayment();
                        orderData.paymentID = RorderId;
                        orderData.Amount = PlanOrderDetails.Payment;
                        orderData.razorpayKey = CCAccessCode;
                        orderData.currency = "INR";
                        orderData.name = CurrentUser.User.Name;
                        orderData.email = CurrentUser.User.Email;
                        orderData.contactNumber = CurrentUser.User.Mobile;
                        orderData.address = "";
                        orderData.description = "";

                        return PartialView("Rpayment", orderData);
                    }
                }

            }
            return JsonResultHelper.Error("Something went wrong.");
            //return JsonResultHelper.Success("Your Order successfully.");
        }

      
        [HttpPost]
        public ActionResult DeleteOder(int PlanOrderId)
        {
            if (PlanOrderId > 0)
            {
                PlanMaster PlanOrder = new PlanMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    PlanOrderID = PlanOrderId
                };
                _planBL.DeleteOrder(PlanOrder);

            } 
            //return JsonResultHelper.Error("Something went wrong.");
            return JsonResultHelper.Success("Your Order Deleted successfully.");
        }

        [HttpPost]
        public ActionResult PaymentRes(VMBuyPackage req)
        {
            if (req.PlanOrderID > 0)
            {
                PlanMaster PlanOrder = new PlanMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    PlanOrderID = req.PlanOrderID
                };
               

            }
            return JsonResultHelper.Error("Something went wrong.");
            //return JsonResultHelper.Success("Your Order successfully.");
        }


    }
}