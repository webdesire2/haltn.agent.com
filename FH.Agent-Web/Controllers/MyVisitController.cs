﻿using FH.User.Entities;
using FH.User.Service;
using FH.User_Web;
using FH.Web.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace FH.Agent_Web.Controllers
{
    public class MyVisitController : BaseController
    {        
        [HttpGet]
        public ActionResult TodayVisit()
        {
            using (var uow = new UnitOfWork())
            {
                return View(uow.PropertyBL.MyPropertyVisit(Validation.DecryptToInt(CurrentUser.User.UserId)));
            }            
        }

        [HttpPost]
        [AjaxRequestValidation]
        public PartialViewResult GetTodayVisit(int propertyId)
        {
            bool isMobile = Request.Browser.IsMobileDevice;
            if (propertyId > 0)
            {
                using (var uow = new UnitOfWork())
                {
                    return PartialView((isMobile?"_MyVisit_Lead_Mobile": "_MyVisit_Lead_Desktop"), uow.PropertyLeadBL.GetTodayVisitLead(propertyId, Validation.DecryptToInt(CurrentUser.User.UserId)));
                }
            }
            return PartialView((isMobile ? "_MyVisit_Lead_Mobile" : "_MyVisit_Lead_Desktop"), new List<PropertyLead>());
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult ScheduleVisit(PropertyLead input)
        {
            if (input.LeadId <= 0)
                return ValidationError("Something went wrong,Please try again.");

            if (!string.IsNullOrEmpty(input.VisitDate))
            {
                DateTime outVisitDate;
                if (DateTime.TryParseExact(input.VisitDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outVisitDate))
                {
                    input.VisitDate = DateTime.ParseExact(input.VisitDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Incorrect visit date.");
            }
            else
                return ValidationError("Visit date required.");

            if (string.IsNullOrWhiteSpace(input.VisitTime))
                return ValidationError("Visit time required.");
            else
            {
                if (ValidateTime(input.VisitTime) == false)
                    return ValidationError("Incorrect visit time.");
            }

            using (var uow = new UnitOfWork())
            {
                bool result = uow.PropertyLeadBL.UpdateScheduleVisit(input, Validation.DecryptToInt(CurrentUser.User.UserId));
                uow.Commit();
                if (result)
                    return Json(new { Success = true, Message = "Visit schedule successfully." });
                else
                    return Json(new { Success = false, ErrorMessage = "Something went wrong,Please try again." });
            }
        }

        private JsonResult ValidationError(string msg)
        {
            return Json(new { Success = false, ErrorMessage = msg });
        }

        private bool ValidateTime(string visitingTime)
        {
            var aryVisitingTime = visitingTime.Split(':');
            if (aryVisitingTime.Length == 3)
            {
                int[] timeHH = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                string[] timeMM = { "00", "30" };
                string[] timeAM = { "AM", "PM" };

                if (!timeHH.Any(s => s.ToString() == aryVisitingTime[0]))
                    return false;

                if (!timeMM.Any(s => s == aryVisitingTime[1]))
                    return false;

                if (!timeAM.Any(s => s == aryVisitingTime[2]))
                    return false;
            }
            return true;
        }
    }
}