﻿using FH.User_Web;
using FH.Web.Security;
using System.Web.Mvc;
using FH.User.Entities;
using FH.User.Service;
using FH.Util;
using FH.Agent_Web.ViewModel;
using System.Collections.Generic;
using System;
using System.IO;
using Amazon.DynamoDBv2.DocumentModel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Linq;
using Newtonsoft.Json;
using Org.BouncyCastle.Ocsp;
using System.Web.UI.WebControls;

namespace FH.Agent_Web.Controllers
{
    [CustomAuthorize]

    public class MyteamController : BaseController
    {
        private readonly TenantBL _tenantBL;
        public MyteamController()
        {
            _tenantBL = new TenantBL();
        }

        public ActionResult Index()
        {
            return View();

        }
        public ActionResult List()
        {
            TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId) };

            TenantMaster owner = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId) };
            ViewBag.PorpertyList = _tenantBL.getMyProperties(owner);

            var Team = _tenantBL.GetMyTeam(de);
            return View(Team);
        }

        [HttpPost]
        public JsonResult AccountUpdate(UserDetail UserDetail)
        {
            TenantMaster de = new TenantMaster() { 
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Name = UserDetail.Name,
                PropertyId  = UserDetail.PropertyId,
                Mobile  = UserDetail.Mobile,
                UserType = UserDetail.UserType,
                IsActive = UserDetail.IsActive,
                UserId = Int32.Parse(UserDetail.UserId),
            };
            var Response = _tenantBL.UpdateMyTeam(de);
            return Json(new { Success = true, Message = "Team Update Successfully." });
        }

        [HttpPost]
        public JsonResult AccountAdd(UserDetail UserDetail)
        {
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Name = UserDetail.Name,
                PropertyId = UserDetail.PropertyId,
                Mobile = UserDetail.Mobile,
                UserType = UserDetail.UserType,
            };
            var Response = _tenantBL.AddMyTeam(de);
            if (Response.Id != 0) {
                return Json(new { Success = true, Message = "Team Add Successfully." });
            } else
            {
                return Json(new { Success = false, Message = "User Already Exist. Please Contact Admin." });
            }
        }

    }
}