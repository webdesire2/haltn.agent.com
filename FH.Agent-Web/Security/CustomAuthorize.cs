﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FH.Web.Security
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string nonAuthorizeAction ="postyourproperty";
            var rd = filterContext.RequestContext.RouteData;
            if (rd.GetRequiredString("action").ToLower() == nonAuthorizeAction || rd.GetRequiredString("action").ToLower() == "contactus" || rd.GetRequiredString("action").ToLower() == "termscondition" || rd.GetRequiredString("action").ToLower() == "privacypoicy" || rd.GetRequiredString("action").ToLower() == "aboutus")
                return;
            
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                string userRole = (CurrentUser != null && CurrentUser.Roles.Length > 0 ? CurrentUser.Roles[0] : "");
                if (!String.IsNullOrWhiteSpace(userRole))
                {
                    if (userRole.ToLower() !="agent" && userRole.ToLower() !="owner")
                    {
                        filterContext.Result = new ViewResult()
                        {
                            ViewName = "_UnAuthorize"
                        };
                    }
                }
                else
                {
                    filterContext.Result = new ViewResult()
                    {
                        ViewName = "_UnAuthorize"
                    };
                }              

                if (!String.IsNullOrEmpty(Users))
                {
                    if (!Users.Contains(CurrentUser.UserId.ToString()))
                    {                        
                        filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new { controller = "Property", action = "Postyourproperty" }));

                        // base.OnAuthorization(filterContext); //returns to login url
                    }                   
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                      RouteValueDictionary(new { controller = "Property", action = "Postyourproperty" }));
            }

        }
    }
}