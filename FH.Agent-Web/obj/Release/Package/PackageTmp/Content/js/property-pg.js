﻿$('.pg-room-type').click(function () {
    if ($(this).hasClass("active"))
        $(this).removeClass("active");
    else
        $(this).addClass('active');
});

$('.tenant-gender').click(function () {
    $('.tenant-gender').removeClass('active');
    $(this).addClass('active');
});

$('.preferred_tenant').click(function () {
    $('.preferred_tenant').removeClass('active');
    $(this).addClass('active');
});

$('.laundry').click(function () {
    $('.laundry').removeClass('active');
    $(this).addClass('active');
});

$('.fooding').click(function () {
    $('.fooding').removeClass('active');
    $(this).addClass('active');
});

$('.room-cleaning').click(function () {
    $('.room-cleaning').removeClass('active');
    $(this).addClass('active');
});

$('.amenity').click(function () {
    $(this).toggleClass('active');
});
$('.room-amenity').click(function () {
    $(this).toggleClass('active');
});
$('.pg-rule').click(function () {
    $(this).toggleClass('active');
});

$('.agent_contact').click(function () {
    $('.agent_contact').removeClass('active');
    $(this).addClass('active');
});

var propertyPost = {
    RoomRentalDetail: {},
    RoomAmenities:[],    
    PropertyMaster: {},
    PgDetail: {},
    pgRules:{},
    GalleryDetails: [],
    AmenityDetails: []
};

$('#btn_pg_room').click(function () {
    var rent1 = "", rent2 = "", rent3 = "", rent4 = "";
    var deposit1 = "", deposit2 = "", deposit3 = "", deposit4 = "";
    var totalSeat1 = "", totalSeat2 = "", totalSeat3 = "", totalSeat4 = "";
    var availableSeat1 = "", availableSeat2 = "", availableSeat3 = "", availableSeat4 = "";
    var roomAmenities = [];
    var roomDetail = [];

    rent1 =$('#rent1').val(); rent2 = $('#rent2').val(); rent3 = $('#rent3').val(); rent4 = $('#rent4').val();
    deposit1 = $('#deposit1').val(); deposit2 = $('#deposit2').val(); deposit3 = $('#deposit3').val(); deposit4 = $('#deposit4').val();
    totalSeat1 = $('#totalSeat1').val(); totalSeat2 = $('#totalSeat2').val(); totalSeat3 = $('#totalSeat3').val(); totalSeat4 = $('#totalSeat4').val();
    availableSeat1 = $('#availableSeat1').val(); availableSeat2 = $('#availableSeat2').val(); availableSeat3 = $('#availableSeat3').val(); availableSeat4 = $('#availableSeat4').val();

    if(rent1 == "")
    { showError("Please enter Single Room Rent amount."); return false; }

    else if (rent1 != undefined)
    {   
        if (parseInt(totalSeat1) < parseInt(availableSeat1))
        { showError("Total seat should be greater than available seat."); return false; }
    }   
    
    if (rent2 == "")
    { showError("Please enter Double Room Rent amount."); return false; }
    else if (rent2 != undefined) {     
        if (parseInt(totalSeat2) < parseInt(availableSeat2))
        { showError("Total seat should be greater than available seat."); return false; }
    }    

     if (rent3 == "")
    { showError("Please enter Three Room Rent amount."); return false; }
    else if (rent3 != undefined) {        
         if (parseInt(totalSeat3) < parseInt(availableSeat3))
         { showError("Total seat should be greater than available seat."); return false; }
    }
    

    if (rent4 == "") { showError("Please enter Four Room Rent amount."); return false; }
    else if (rent4 != undefined) {        
        if (parseInt(totalSeat4) < parseInt(availableSeat4)) { showError("Total seat should be greater than available seat."); return false; }
    }

    $('.room-amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).attr("data-key");
            roomAmenities.push({ AmenityId: amenity });
        }
    });

    if (rent1 > 0)
        roomDetail.push({ RoomType: "Single", ExpectedRent: rent1, ExpectedDeposit: deposit1, TotalSeat: totalSeat1, AvailableSeat: availableSeat1 });
    if (rent2 > 0)
        roomDetail.push({ RoomType: "Double", ExpectedRent: rent2, ExpectedDeposit: deposit2, TotalSeat: totalSeat2, AvailableSeat: availableSeat2 });
    if (rent3 > 0)
        roomDetail.push({ RoomType: "Three", ExpectedRent: rent3, ExpectedDeposit: deposit3, TotalSeat: totalSeat3, AvailableSeat: availableSeat3 });
    if (rent4 > 0)
        roomDetail.push({ RoomType: "Four", ExpectedRent: rent4, ExpectedDeposit: deposit4, TotalSeat: totalSeat4, AvailableSeat: availableSeat4 });


    propertyPost.RoomRentalDetail = roomDetail;
    propertyPost.RoomAmenities = roomAmenities;

    $('#pg_room_info').hide();
    $('#locality_info').show();
});

$('#btn_pg_room_edit').click(function () {
    var rent1 = "", rent2 = "", rent3 = "", rent4 = "";
    var deposit1 = "", deposit2 = "", deposit3 = "", deposit4 = "";
    var totalSeat1 = "", totalSeat2 = "", totalSeat3 = "", totalSeat4 = "";
    var availableSeat1 = "", availableSeat2 = "", availableSeat3 = "", availableSeat4 = "";
    var roomAmenities = [];
    var roomDetail = [];

    rent1 = $('#rent1').val(); rent2 = $('#rent2').val(); rent3 = $('#rent3').val(); rent4 = $('#rent4').val();
    deposit1 = $('#deposit1').val(); deposit2 = $('#deposit2').val(); deposit3 = $('#deposit3').val(); deposit4 = $('#deposit4').val();
    totalSeat1 = $('#totalSeat1').val(); totalSeat2 = $('#totalSeat2').val(); totalSeat3 = $('#totalSeat3').val(); totalSeat4 = $('#totalSeat4').val();
    availableSeat1 = $('#availableSeat1').val(); availableSeat2 = $('#availableSeat2').val(); availableSeat3 = $('#availableSeat3').val(); availableSeat4 = $('#availableSeat4').val();
    if (rent1!=undefined && rent1 == "")
    { showError("Please enter Single Room Rent amount."); return false; }
    else if (rent1 != undefined) {        
         if(parseInt(totalSeat1)<parseInt(availableSeat1))
        { showError("Single Room Total Seat should be greater than available Seat."); return false; }
    }


    if (rent2 == "")
    { showError("Please enter Double Room Rent amount."); return false; }
    else if (rent2 != undefined) {        
        if (parseInt(totalSeat2) < parseInt(availableSeat2))
        { showError("Double Room Total Seat should be greater than available Seat."); return false; }
    }

    if (rent3 == "")
    { showError("Please enter Three Room Rent amount."); return false; }
    else if (rent3 != undefined) {        
        if (parseInt(totalSeat3) < parseInt(availableSeat3))
        { showError("Three Room Total Seat should be greater than available Seat."); return false; }
    }


    if (rent4 != undefined) {        
        if (parseInt(totalSeat4) < parseInt(availableSeat4))
        { showError("Four Room Total Seat should be greater than available Seat."); return false; }
    }
    else if (rent4 == "")
    { showError("Please enter Four Room Rent amount."); return false; }

    $('.room-amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).attr("data-key");
            roomAmenities.push({ AmenityId: amenity });
        }
    });

    if (rent1 != undefined && deposit1 != undefined)
        roomDetail.push({ RoomType: "Single", ExpectedRent: rent1, ExpectedDeposit: deposit1,TotalSeat:totalSeat1,AvailableSeat:availableSeat1 });
    if (rent2 != undefined && deposit2 != undefined)
        roomDetail.push({ RoomType: "Double", ExpectedRent: rent2, ExpectedDeposit: deposit2, TotalSeat: totalSeat2, AvailableSeat: availableSeat2 });
    if (rent3 != undefined && deposit3 != undefined)
        roomDetail.push({ RoomType: "Three", ExpectedRent: rent3, ExpectedDeposit: deposit3, TotalSeat: totalSeat3, AvailableSeat: availableSeat3 });
    if (rent4 != undefined && deposit4 != undefined)
        roomDetail.push({ RoomType: "Four", ExpectedRent: rent4, ExpectedDeposit: deposit4, TotalSeat: totalSeat4, AvailableSeat: availableSeat4});

    $('#btn_pg_room_edit').html('Please Wait...  <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdatePGRoomDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), rentalDetail: roomDetail, roomAmenity:roomAmenities}),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_pg_room_edit').html('Update').removeAttr("disabled");
        }

    });
    
});

$('#btn_pg_room_next').click(function () {
    $('#pg_room_info').hide();
    $('#locality_info').show();
});

$('#btn_back_locality').click(function () {
    $('#pg_room_info').show();
    $('#locality_info').hide();
});

$('#btn_locality').click(function () {
    debugger;
    var pType = localStorage.getItem('propertyType');
    var propertyType = $('#property_type').val();
    var city = $('#hdnCity').val();
    var PropertyName = $('#PropertyName').val();
    var locality = $('#locality').val().trim();
    var street = $('#street').val().trim();
    var lat = $('#hdnLat').val().trim();
    var lng = $('#hdnLng').val().trim();
    var contactPersonName = $('#contactPersonName').val();
    var contactPersonMobile = $('#contactPersonMobile').val();

    if (contactPersonName === "") { showError("Contact person name required."); return false; }
    else if (contactPersonName.length > 50) { showError("Contact person name can't be more than 50 characters."); return false; }

    if (contactPersonMobile === "") { showError("Contact person mobile no required."); return false; }
    if (!validateMobile(contactPersonMobile)) { showError("Invalid Contact person mobile no."); return false; }

    //if (city == "")
    //{ showError("Please select city."); return false }

    if (locality == "")
    { showError("Locality required."); return false }
    else if (locality.length > 100)
    { showError("Maximum 100 characters allowed for locality."); return false }

    if (lat == "" || lng == "")
    { showError("Please select locality from list."); return false }

    if (street.length > 100)
    { showError("Maximum 100 characters allowed for street/area."); return false }

    var postData = { PropertyType: propertyType, City: city, Locality: locality, Street: street, Latitude: lat, Longitude: lng, ContactPersonName: contactPersonName, ContactPersonMobile: contactPersonMobile, PropertyName: PropertyName };
    propertyPost.PropertyMaster = postData;
    $('#locality_info').hide();
    $('#pg_info').show();
    if (pType === 'room') {
        
        $('.tenant-gender[data-key="Male"], .tenant-gender[data-key="Female"], .tenant-gender[data-key="Co-Living"]').hide();
      
    }
    else if (pType === 'pg') { $('.tenant-gender[data-key="RoomRK"]').hide(); }
   
});




$('#btn_edit_locality').click(function () {
    var city = $('#hdnCity').val();
    var locality = $('#locality').val().trim();
    var street = $('#street').val().trim();
    var lat = $('#hdnLat').val().trim();
    var lng = $('#hdnLng').val().trim();
    var contactPersonName = $('#contactPersonName').val();
    var PropertyName = $('#PropertyName').val();
    var contactPersonMobile = $('#contactPersonMobile').val();

    if (contactPersonName === "") { showError("Contact person name required."); return false; }
    else if (contactPersonName.length > 50) { showError("Contact person name can't be more than 50 characters."); return false; }

    if (contactPersonMobile === "") { showError("Contact person mobile no required."); return false; }
    if (!validateMobile(contactPersonMobile)) { showError("Invalid Contact person mobile no."); return false; }

    if (locality == "")
    { showError("Locality required."); return false }
    else if (locality.length > 100)
    { showError("Maximum 100 characters allowed for locality."); return false }

    if (lat == "" || lng == "")
    { showError("Please select locality from list."); return false }

    if (street.length > 100)
    { showError("Maximum 100 characters allowed for street/area."); return false }

    var postData = { City: city, Locality: locality, Street: street, Latitude: lat, Longitude: lng, ContactPersonName: contactPersonName, ContactPersonMobile: contactPersonMobile, PropertyName: PropertyName };
    $('#btn_edit_locality').html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdateLocalityDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), property: postData }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_edit_locality').html('Update').removeAttr("disabled");
        }

    });
});

$('#btn_next_locality').click(function () {
    $('#locality_info').hide();
    $('#pg_info').show();

});

$('#btn_pg_back').click(function () {
    $('#pg_info').hide();
    $('#locality_info').show();
});

$('#btn_pg_detail').click(function () {    
    var tenantGender = $('.tenant-gender.active').data('key');
    var preferredTenant = $('.preferred_tenant.active').data('key');
    var availableFrom = $('#avaliable_from').val();
    var fooding = $('.fooding.active').data('key');
    var laundry = $('.laundry.active').data('key');
    var roomCleaning = $('.room-cleaning.active').data('key');
    var isAgentAllowed = $('.agent_contact.active').data('key');

    var pgRules = [];

    if (tenantGender == "" || tenantGender == undefined)
    { showError("Please select PG Available For."); return false; }

    $('.pg-rule').each(function () {
        if ($(this).hasClass('active')) {
            pgRules.push({ RuleId: $(this).attr("data-key") });
        }
    });

    propertyPost.PgDetail = {
        TenantGender: tenantGender, PreferredGuest: preferredTenant, AvailableFrom: availableFrom, Fooding: fooding,
        Laundry: laundry, RoomCleaning: roomCleaning,IsAgentAllowed:isAgentAllowed
    };

    propertyPost.pgRules = pgRules;

    $('#pg_info').hide();
    $('#gallery_info').show();
});

$('#btn_pg_detail_edit').click(function () {    
    var tenantGender = $('.tenant-gender.active').data('key');
    var preferredTenant = $('.preferred_tenant.active').data('key');
    var availableFrom = $('#avaliable_from').val();
    var fooding = $('.fooding.active').data('key');
    var laundry = $('.laundry.active').data('key');
    var roomCleaning = $('.room-cleaning.active').data('key');
    var isAgentAllowed = $('.agent_contact.active').data('key');

    var pgRules = [];

    if (tenantGender == "" || tenantGender == undefined)
    { showError("Please select PG Available For."); return false; }

    $('.pg-rule').each(function () {
        if ($(this).hasClass('active')) {
            pgRules.push({ RuleId: $(this).attr("data-key") });
        }
    });

    var pgData = {
        TenantGender: tenantGender, PreferredGuest: preferredTenant, AvailableFrom: availableFrom, Fooding: fooding,
        Laundry: laundry, RoomCleaning: roomCleaning,IsAgentAllowed:isAgentAllowed
    };

    $('#btn_pg_detail_edit').html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdatePGDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), pgDetail: pgData, pgRules: pgRules }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_pg_detail_edit').html('Update').removeAttr("disabled");
        }

    });
});

$('#btn_pg_next').click(function () {
    $('#pg_info').hide();
    $('#gallery_info').show();
});

$('#btn_gallery_back').click(function () {
    $('#pg_info').show();
    $('#gallery_info').hide();
});

$('#gallery_info').on("click", "#btnGallery", function (e) {
    $('#gallery_info').hide();
    $('#amenity_info').show();
});

$('#btn_gallery_next').click(function () {
    $('#gallery_info').hide();
    $('#amenity_info').show();    
});

$('#btn_amenity_back').click(function () {
    $('#gallery_info').show();
    $('#amenity_info').hide();
});

$('#btn_amenity_next').click(function () {
    $('#amenity_info').hide();
    $('#room_info').show();
});

$('#btn_room_back').click(function () {
    $('#amenity_info').show();
    $('#room_info').hide();
});

$('#btn_amenity').click(function () {
    var amenities = [];
    $('.amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).data("key");
            amenities.push({ AmenityId: amenity });
        }
    });

    propertyPost.AmenityDetails = amenities;

    savePropertyDetail();
});

$('#btn_edit_amenity').click(function () {
    var amenities = [];
    $('.amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).data("key");
            amenities.push({ AmenityId: amenity });
        }
    });
    $('#btn_edit_amenity').html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdateAmenity",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), amenities: amenities }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Amenities updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_edit_amenity').html('Update').removeAttr("disabled");
        }
    });
});

function savePropertyDetail() {
    var postData = {
        property: propertyPost
    };

    $('#btn_amenity').html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/PostPG",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(postData),
        success: function (data, status) {
            if (data.Success == true)
                $('#dv_container_row').html(data.Data);
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_amenity').html('Submit').removeAttr("disabled");
        }
    });
}

function removeGalleryPic(imgId) {
    $.ajax({
        url: "/property/DeleteGallery",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), image: imgId }),
        success: function (data, status) {
            if (data.Success == true)
                $('#img' + imgId).remove();
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        }
    });
}

$('.post-property-iamges').on("click", ".remove-picture", function (e) {
    var parentLi = $(this).parent();
    var filePath = $(this).data('file');
    if (filePath !== "" && filePath !== undefined) {
        $.grep(propertyPost.GalleryDetails, function (element, index) {
            if (element.FileName === filePath) {
                $.ajax({
                    url: "/property/RemoveGalleryFile",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ imagePath: filePath }),
                    success: function (data, status) {
                        if (data.Success == true) {
                            parentLi.remove();
                            propertyPost.GalleryDetails.splice(index, 1);
                        }
                        else if (data.Success == false) {
                            showError(data.ErrorMessage);
                        }
                    }
                });
            }
        });
    }
});

$('.rent,.deposit').on('keypress paste', function (e) {
    var txt = String.fromCharCode(e.which);
    if (!isInt(txt))
        return false;
});

function isInt(val) {
    if (val.match(/^-?\d*$/))
        return true;
    else
        return false;
}
