﻿
using System.Collections.Generic;

namespace FH.User_Web
{
    public class ApartmentType
    {
        public static IDictionary<string,string> GetAll()
        {
            IDictionary<string, string> lstApartment = new Dictionary<string, string>();
                lstApartment.Add("AP","Apartment");       
            lstApartment.Add("IH","Independent House/Villa");
            lstApartment.Add("IF","Independent Floor/Builder Floor");        
            return lstApartment;            
        }       
               
    }    
}