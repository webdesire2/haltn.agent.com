﻿
using System.Collections.Generic;

namespace FH.User_Web
{
    public class PropertyType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstType = new Dictionary<string, string>();
            lstType.Add("Flat", "Flat");
            lstType.Add("Room", "Room");
            lstType.Add("Flatmate", "FlatMate");
            lstType.Add("PG", "PG");
            return lstType;
        }

    }
}