﻿
using System.Collections.Generic;

namespace FH.User_Web
{
    public class PGRulesMaster
    {
        public static IDictionary<int, string> GetAll()
        {
            IDictionary<int, string> lstRules = new Dictionary<int, string>();
            lstRules.Add(1, "No Smoking");
            lstRules.Add(2, "No Guardians Stay");           
            lstRules.Add(3, "No Girl's Entry");
            lstRules.Add(4, "No Boy's Entry");
            lstRules.Add(5, "No Drinking");
            lstRules.Add(6, "No Non-veg");
            return lstRules;
        }

    }
}