﻿using System.Collections.Generic;

namespace FH.User_Web
{
    public class PropertyAgeType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lst = new Dictionary<string, string>();            
            lst.Add("NC", "New Construction");
            lst.Add("5Y", "Less than 5 years");
            lst.Add("10Y", "5 to 10 years");
            lst.Add("15Y", "10 to 15 years");
            lst.Add("20Y", "15 to 20 years");
            lst.Add("20+Y", "Above 20 years");
            return lst;
        }
    }
}