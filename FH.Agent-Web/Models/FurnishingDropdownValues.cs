﻿using System.Collections.Generic;

namespace FH.User_Web
{
    public class FurnishingDropdownValues
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstValues = new Dictionary<string, string>();
            lstValues.Add("1", "1");
            lstValues.Add("2", "2");
            lstValues.Add("3", "3");
            lstValues.Add("4", "4");
            lstValues.Add("5", "5");
            lstValues.Add("6", "6");
            lstValues.Add("6+", "6+");            
            return lstValues;
        }

    }
}