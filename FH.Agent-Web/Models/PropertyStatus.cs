﻿using System.Collections.Generic;

namespace FH.User_Web
{
    public class PropertyStatus
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstStatus = new Dictionary<string, string>();
            lstStatus.Add("Pending", "Pending");
            lstStatus.Add("Rejected", "Rejected");
            lstStatus.Add("Approved", "Approved");
            lstStatus.Add("RentOut", "RentOut");
            lstStatus.Add("Cancelled", "Cancelled");
            return lstStatus;
        }
    }
}