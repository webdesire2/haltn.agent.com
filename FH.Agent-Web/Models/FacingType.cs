﻿using System.Collections.Generic;

namespace FH.User_Web
{
    public class FacingType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstFacing = new Dictionary<string, string>();
            lstFacing.Add("NO","North");
            lstFacing.Add("SO","South");
            lstFacing.Add("EA","East");
            lstFacing.Add("WE","West");
            lstFacing.Add("NE", "North-East");
            lstFacing.Add("SE", "South-East");
            lstFacing.Add("NW", "North-West");
            lstFacing.Add("SW", "South-West");
                                    
            return lstFacing;
        }
    }
}