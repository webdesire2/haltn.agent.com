﻿
using System.Collections.Generic;

namespace FH.User_Web
{
    public class RoomType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstType = new Dictionary<string, string>();
            //lstType.Add("PR", "Private Room");
            //lstType.Add("SR", "Shared Room");
            lstType.Add("S", "Single");
            return lstType;
        }

    }
}