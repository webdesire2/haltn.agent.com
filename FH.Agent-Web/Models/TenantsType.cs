﻿using System.Collections.Generic;

namespace FH.User_Web
{
    public class PreferredTenant
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lsttype = new Dictionary<string, string>();
            lsttype.Add("FAM", "Family");
            lsttype.Add("BAC", "Bachelors");
            lsttype.Add("COM", "Company");
            lsttype.Add("ALL", "All");
            return lsttype;
        }
    }
}