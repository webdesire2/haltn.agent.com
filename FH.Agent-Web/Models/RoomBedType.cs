﻿using System.Collections.Generic;

namespace FH.User_Web
{
    public class RoomBedType
    {
        public static IDictionary<int, string> GetAll()
        {
            IDictionary<int, string> lstType = new Dictionary<int, string>();
            lstType.Add(1, "Single");
            lstType.Add(2, "Double");
            lstType.Add(3, "Three");
            lstType.Add(4, "Four");            
            return lstType;
        }
    }
}