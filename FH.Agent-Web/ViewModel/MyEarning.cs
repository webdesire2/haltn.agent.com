﻿using FH.User.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FH.Agent_Web.ViewModel
{
    public class MyEarningListVM
    {
        public int PropertyId { get; set; }
        public int TenantId { get; set; }
        public int PaymentId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public IEnumerable<PaymentCollection> PaymentCollections { get; set; }
        public IEnumerable<TotalPayment> TotalPayment { get; set; }
    }


    public class PaymentDetailsData
    {
        public int PropertyId { get; set; }
        public int TenantId { get; set; }
        public int PaymentId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public IEnumerable<PaymentCollection> PaymentCollections { get; set; }
        public IEnumerable<TotalPayment> TotalPayment { get; set; }
    }

}