﻿using System.Collections.Generic;
using FH.User.Entities;
using FH.Util;
namespace FH.User_Web
{
    public class VMPropertyListing
    {
        public IEnumerable<PropertyList> PropertyList { get; set; }
        public Pager Paging { get; set; }
        public PropertySearchParameter propertySearchInput{get;set;}
    }
}