﻿using FH.User.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FH.Agent_Web.ViewModel
{

    public class VMMyPlan
    {
        public int PropertyId { get; set; }
        public IEnumerable<PlanMaster> MyPlans { get; set; }
        public IEnumerable<PlanMaster> MyOrders { get; set; }
    }

    public class VMPackage
    {
        public IEnumerable<PlanMaster> Plans { get; set; }
        public IEnumerable<PlanMaster> MyOrders { get; set; }   //added
        public IEnumerable<PlanMaster> MyPlans { get; set; }   //added


    }

    public class VMBuyPackage
    {
        public int PlanId { get; set; }
        public int PlanOrderID { get; set; }
        public string Category { get; set; }
        public IEnumerable<PlanMaster> Plans { get; set; }
        public IEnumerable<PropertyList> PropertyList { get; set; }
        public Array PropertyId { get; set; }
        public double Price { get; set; }

    }

    public class VMPayment
    {
        public string paymentID { get; set; }
        public double Amount { get; set; }
        public string razorpayKey { get; set; }
        public string currency { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string contactNumber { get; set; }
        public string address { get; set; }
        public string description { get; set; }
    }
}