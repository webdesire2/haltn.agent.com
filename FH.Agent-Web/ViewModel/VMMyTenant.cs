﻿using FH.User.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FH.Agent_Web.ViewModel
{
    public class AddTenantVM
    {        
        public int PropertyId { get; set;}
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public int RoomNo { get; set; }
        public int RoomId { get; set; }
        public float Occupancy { get; set; }
        public double SecurityAmount { get; set; }
        public double MonthlyRent { get; set; }
        public DateTime CheckInDate { get; set; }
        public double SecurityDeposit { get; set; }

        public IEnumerable<PGRoomData> PGRoomData { get; set; }
    }


    public class EditTenantVM
    {
        public IEnumerable<PGRoomData> PGRoomData { get; set; }
        public PGRoomData TenantRoom { get; set; }
        public IEnumerable<TenantMaster> TenantMaster { get; set; }
    }


    public class PropertyMapVM
    {
        public int Pid { get; set; }
        public int Tid { get; set; }
        public int RoomNo { get; set; }
        public string Mobile { get; set; }
        public IEnumerable<PropertyList> PropertyList { get; set; }
        public IEnumerable<TenantMaster> TenantMaster { get; set; }
        public IEnumerable<PGRoomData> PGRoomData { get; set; }

    }

    public class VMPaymentDetails
    {
        public TanentStayDetails TanentStayDetails { get; set; }
        public IEnumerable<TenantPaymentLogDTO> TenantPaymentHistory { get; set; }
    }

    public class CollectCashData
    {
        public int TenantId { get; set; }
        public double rentAmount { get; set; }
        public DateTime? rentForMonthStartDate { get; set; }
        public DateTime? rentForMonthEndDate { get; set; }
        public string SRemark { get; set; } 
    }

}