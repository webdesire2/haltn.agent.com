﻿using System.Web.Optimization;

namespace FH.User_Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            ScriptBundle scriptBndl = new ScriptBundle("~/bundles/masterjs");
            scriptBndl.Include(
                                 "~/Content/js/jquery-2.1.1.min.js",
                                "~/Content/js/bootstrap.min.js",
                                "~/Content/js/bootstrap-waitingfor.min.js"
                              );

            ScriptBundle scriptBndPageJs = new ScriptBundle("~/bundles/pagejs");
            scriptBndl.Include(
                                 "~/Content/js/custom.js",
                                 "~/Content/js/login.js",
                                 "~/Content/js/property.js"
                              );


            bundles.Add(scriptBndl);
            bundles.Add(scriptBndPageJs);

           // BundleTable.EnableOptimizations = true;
        }
    }
}