$(document).ready(function () {
    $('.menuBtn').click(function () {
        $('.customMenuRight').addClass('customMenuRightActive');
        $('.backDropBg').addClass('backDropShow');
        $('body').css('overflow', 'hidden');
    });
    $('.customMenuClose').click(function () {
        $('.customMenuRight').removeClass('customMenuRightActive');
        $('.backDropBg').removeClass('backDropShow');
        $('body').css('overflow', 'auto');
    });

    $('.backDropBg').click(function () {
        $('.customMenuRight').removeClass('customMenuRightActive');
        $('.backDropBg').removeClass('backDropShow');
        $('body').css('overflow', 'auto');
    });
    $('.menuBtn, .customMenuRight ').click(function (e) {
        e.stopPropagation();
    });
});


$(function () {
    $('.profile-edit-btn').click(function () {
        $('.profile-edit-modal').addClass('active');
        $('html, body').addClass('overflow_hidden');
    });

    $('.edit-business-detail-btn').click(function () {
        $('.edit-business-detail-modal').addClass('active');
        $('html, body').addClass('overflow_hidden');
    });

    $('.view-response-modal-btn').click(function () {
        $('.view-response-modal').addClass('active');
        $('html, body').addClass('overflow_hidden');
    });

    $('.tracking-status-modal-btn').click(function () {
        $('.tracking-status-modal').addClass('active');
        $('html, body').addClass('overflow_hidden');
    });


    $('.add-new-lead-modal-btn').click(function () {
        $('.add-new-lead-modal').addClass('active');
        $('html, body').addClass('overflow_hidden');
    });

    $('.modal-box .back-btn').click(function () {
        $('.modal-box').removeClass('active');
        $('html, body').removeClass('overflow_hidden');
    });

});


//$(function () {
//    $('.form-control').focus(function () {
//        $(this).closest('.form-group').find('label').addClass('active');
//    });
//    $('.form-control').blur(function () {
//        $(this).closest('.form-group').find('label').removeClass('active');
//        ifInputHasValue();
//    });

//    ifInputHasValue();
//    function ifInputHasValue() {
//        $('.form-control').each(function () {
//            if ($(this).val()) {
//                $(this).closest('.form-group').find('label').addClass('active');
//            }
//            else {
//                $(this).closest('.form-group').find('label').removeClass('active');
//            }
//        })
//    }
//})