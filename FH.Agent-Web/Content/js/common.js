﻿$(function () {
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('RequestVerificationToken',$('#RequestVerificationTokenAjax').val());
        },
        error: function (x, status, error) {           
            if (x.status == 403) {
                alert("Sorry, your session has expired. Please login again to continue");
                window.location.href = "/User/Login";
            }
            else if(x.status==406)
            {
                alert("Unauthorized request");
            }
            else if (x.status == 400) {
                showError("Something went wrong.please try again.");
            }
            else {
                showError("Something went wrong.please try again.");
            }
        }
    });
});

function showLoginPopUp(tab) {
    $('.customMenuRight').removeClass('customMenuRightActive');
    $('.backDropBg').removeClass('backDropShow');
    $('body').css('overflow', 'auto');
    $('#loginModal').modal('show');
    $('#loginModal .nav-tabs a[href="#' + tab + '"]').tab('show');
}

function loginBtn() {
    $('#loginModal').modal('show');
}

function showError(text) {
    $('.validation_error').text(text).addClass('active');
    setTimeout(function () { $('.validation_error').text('').removeClass('active'); }, 3000);
    return false;
}

function showSuccessMessage(text) {
    $('.success-message').text(text).addClass('active');
    setTimeout(function () { $('.success-message').text('').removeClass('active'); }, 3000);    
}

function reload(text)
{
    $('.success-message').text(text).addClass('active');
    setTimeout(function () {location.reload()},2000);
}

var loading = {
    show:function(btn)
    {
        $(btn).html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    },
    hide:function(btn,text)
    {
        $(btn).html(text).removeAttr("disabled");
    }
}

function validateEmail(i) {
    if (i == "" || i == null)
        return false;
    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return regex.test(i) ? true : false;
}

function validateMobile(i) {
    var mobilefilter = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/;
    return mobilefilter.test(i) ? true : false;
}

function validateOnlyAlpha(i) {
    var regex = new RegExp("^[a-zA-Z ]+$");
    return regex.test(i) ? true : false;
}

function isAlpha(event) {
    var value = String.fromCharCode(event.which);
    var regex = new RegExp("^[a-zA-Z ]+$");
    return regex.test(value);
}

function isInt(e)
{
    var value = String.fromCharCode(event.which);
    var regex = new RegExp("^[0-9]+$");
    return regex.test(value);
}

$(function () {
    $('.buyer-agree-btn').click(function () {
        $('.all-agree').hide();
        $('.buyer-agree').show();
    });
    $('.agent-agree-btn').click(function () {
        $('.all-agree').hide();
        $('.agent-agree').show();
    });
});

