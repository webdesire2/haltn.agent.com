﻿
////////////////////Post Property////////////////////////////
var propertyPost = {
    PropertyMaster: {},
    PropertyDetail: {},
    GalleryDetails: [],
    AmenityDetails: []    
};

$('.apartment_type').click(function () {
    $('.apartment_type').removeClass('active');
    $(this).addClass('active');
});

$('.bhk-type').click(function () {
    $('.bhk-type').removeClass('active');
    $(this).addClass('active');
});

$('.room-type').click(function () {
    $('.room-type').removeClass('active');
    $(this).addClass('active');
});

$('.tenant-gender').click(function () {
    $('.tenant-gender').removeClass('active');
    $(this).addClass('active');
});

$('.facing').click(function () {
    $('.facing').removeClass('active');
    $(this).addClass('active');
});

$('.bathroom').click(function () {
    $('.bathroom').removeClass('active');
    $(this).addClass('active');
});
$('.balcony').click(function () {
    $('.balcony').removeClass('active');
    $(this).addClass('active');
});
$('.water_supply').click(function () {
    $('.water_supply').removeClass('active');
    $(this).addClass('active');
});
$('.gate_security').click(function () {
    $('.gate_security').removeClass('active');
    $(this).addClass('active');
});
$('.furnish').click(function () {
    $('.furnish').removeClass('active');
    $(this).addClass('active');
    if ($(this).data('key') == 'FF' || $(this).data('key') == 'SF')
    { $('.furnished_item').show(); }
    else
    { $('.furnished_item').hide(); }
});

$('.preferred_tenant').click(function () {
    $('.preferred_tenant').removeClass('active');
    $(this).addClass('active');
});

$('.parking').click(function () {
    $('.parking').removeClass('active');
    $(this).addClass('active');
});

$('.amenity').click(function () {
    $(this).toggleClass('active');
});

$('.agent_contact').click(function () {
    $('.agent_contact').removeClass('active');
    $(this).addClass('active');
});

$('#btn_property_detail').click(function () {
    var roomType = "";
    var tenantGender="";
    var bhkType = "";
    var propertyType=$('#property_type').val().toLowerCase();
    var apartmentType = $('.apartment_type.active').data('key');
     
    if(propertyType=="flat" || propertyType=="flatmate")
    bhkType = $('.bhk-type.active').data('key');
    
    if (propertyType == "flatmate")
      tenantGender= $('.tenant-gender.active').data('key');
    
    if (propertyType == "flatmate" || propertyType == "room")
        roomType = $('.room-type.active').data('key');

    var apartment = $('#apartment_name').val().trim();
    var area = $('#area').val();
    var facing = $('.facing.active').data('key');
    var propAge = $('#property_age').val();
    var floor = $('#floor').val();
    var totalFloor = $('#total_floor').val();
    var bathroom = $('.bathroom.active').data('key');
    var balcony = $('.balcony.active').data('key');
    var waterSupply = $('.water_supply.active').data('key');
    var gateSecurity = $('.gate_security.active').data('key');
    var isAgentAllowed = $('.agent_contact.active').data('key');

    if(propertyType=="" ||propertyType==undefined)
    return showError("Invalid property type.");

    if (apartmentType == "" || apartmentType == undefined)
     return showError("Please select apartment type.");

    if ((propertyType=="flat" || propertyType=="flatmate") && bhkType == "" || bhkType == undefined)
     return showError("Please select BHK type.");

    if ((propertyType== "flatmate" || propertyType== "room") && (roomType == "" || roomType == undefined))    
       return showError("Please select Room Type.");
    
    if (propertyType == "flatmate" && (tenantGender == "" || tenantGender == undefined)) 
        return showError("Please select Available For.");
    
    if (apartment.length > 50)
     return showError("Apartment name shouldn't be more than 50 characters.");

    if (area == "" || area ==undefined)
     return showError("Please enter build up area.");

    if (facing == "" || facing == undefined)
        facing = "";

    if (propAge == "" || propAge == undefined)
        propAge = "";
    if (floor == "" || floor == undefined)
        floor = "";
    else if (totalFloor == "")
        return showError("Total floor required.");

    if (totalFloor == "" || totalFloor == undefined)
        totalFloor = "";
    else if(floor=="")
        return showError("Floor no required.");

    if (floor!="" && totalFloor!="" && parseInt(floor) > parseInt(totalFloor))
     return showError("Floor no can't be greater than total floor.");

    if (bathroom == undefined)
        bathroom = "";
    if (balcony == undefined)
        balcony = "";
    if (waterSupply == undefined)
        waterSupply = "";
    if (gateSecurity == undefined)
        gateSecurity = "";
    
    var postData = {        
        ApartmentType: apartmentType, BhkType: bhkType,RoomType:roomType,TenantGender:tenantGender,ApartmentName: apartment, PropertySize: area, Facing: facing, PropertyAge: propAge,
        FloorNo: floor, TotalFloor: totalFloor, BathRooms: bathroom, Balconies: balcony, WaterSupply: waterSupply, GateSecurity: gateSecurity, IsAgentAllowed: isAgentAllowed
    }

    propertyPost.PropertyDetail = postData;        
    $('#property_detail_info').hide();
    $('#locality_info').show();

});

$('#btn_back_locality').click(function () {
    $('#property_detail_info').show();
    $('#locality_info').hide();
});

$('#btn_locality').click(function () {
    var propertyType = $('#property_type').val();
    var city = $('#hdnCity').val();
    var locality = $('#locality').val().trim();
    var street = $('#street').val().trim();
    var lat = $('#hdnLat').val().trim();
    var lng = $('#hdnLng').val().trim();
    var contactPersonName = $('#contactPersonName').val();
    var contactPersonMobile = $('#contactPersonMobile').val();

    if (contactPersonName === "")
    { showError("Contact person name required."); return false; }
    else if (contactPersonName.length > 50)
    { showError("Contact person name can't be more than 50 characters."); return false; }

    if (contactPersonMobile === "") { showError("Contact person mobile no required."); return false; }
    if (!validateMobile(contactPersonMobile))
    { showError("Invalid Contact person mobile no."); return false; }

    //if (city == "")
    //{ showError("Please select city."); return false }

    if (locality == "")
    { showError("Locality required."); return false }
    else if (locality.length > 100)
    { showError("Maximum 100 characters allowed for locality."); return false }

    if (lat == "" || lng == "")
    { showError("Please select locality from list."); return false }

    if (street.length > 100)
    { showError("Maximum 100 characters allowed for street/area."); return false }

    var postData = { PropertyType: propertyType, City: city, Locality: locality, Street: street, Latitude: lat, Longitude: lng, ContactPersonName: contactPersonName, ContactPersonMobile: contactPersonMobile };
    propertyPost.PropertyMaster = postData;
    $('#locality_info').hide();
    $('#rental_info').show();
});

$('#btn_back_rental').click(function () {
    $('#locality_info').show();
    $('#rental_info').hide();
});

$('#btn_rental').click(function () {
    var rent = $('#rent').val().trim();
    var deposit = $('#deposit').val().trim();
    var availableFrom = $('#avaliable_from').val();
    var rentNegotiable = false;
    if ($("input[name='rent_negotiable']").is(':checked'))
        rentNegotiable = true;
    
    var preferredTenant = $('.preferred_tenant.active').data('key');
    var parking = $('.parking.active').data('key');
    var furnishing = $('.furnish.active').data('key');
    var furnishingDetail = [];
    var description = $('#description').val().trim();

    if (rent == "" || !isInt(rent))
     return showError("Expected rent should be numeric.");
    
    if (preferredTenant == "" || preferredTenant == undefined)
      return showError("Please select Preferred Tenant.");

    if (furnishing == "" || furnishing == undefined)
      return showError("Please select Furnishing.");

    if (furnishing == 'FF' || furnishing == 'SF') {
        $.each($(".ddFurnish option:selected"), function () {
            if ($(this).val() != "") {
                var FurnishingMasterId = $(this).parent().attr('id');
                var Value = $(this).val();
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });

        $('input[name="chkFurnish"]').each(function () {
            if ($(this).is(":checked")) {
                var FurnishingMasterId = $(this).attr("id");
                var Value = "1";
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });
    }

    if (description.length > 500)
     return showError("Description shouldn't be more than 500 characters.");

    var postData = {
        ExpectedRent: rent, ExpectedDeposit: deposit, AvailableFrom: availableFrom, IsRentNegotiable: rentNegotiable, PreferredTenant: preferredTenant,
        Parking: parking, Furnishing: furnishing, FurnishingDetails: furnishingDetail, Description: description
    };

    $.extend(propertyPost.PropertyDetail, postData);
    $('#rental_info').hide();
    $('#gallery_info').show();

});

$('#btn_gallery_back').click(function () {
    $('#rental_info').show();
    $('#gallery_info').hide();
});

$('#gallery_info').on("click", "#btnGallery", function (e) {
    $('#gallery_info').hide();
    $('#amenity_info').show();
});

$('#btn_amenity_back').click(function () {
    $('#gallery_info').show();
    $('#amenity_info').hide();
});

$('#btn_amenity').click(function () {
    var amenities = [];

    $('.amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).data("key");
            amenities.push({ AmenityId: amenity });
        }
    });    

    propertyPost.AmenityDetails = amenities;

    savePropertyDetail("btn_amenity");
});

function savePropertyDetail(btn) {
    var postData = {
        property: propertyPost
    };
    
    $('#' + btn).html('Please wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/PostFlatRoom",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(postData),
        success: function (data, status) {            
            if (data.Success == true)
                $('#dv_container_row').html(data.Data);
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }            
        },
        complete: function(){
            $('#'+ btn).html('Submit').removeAttr("disabled");
        }
    });    
}

$('#area,#property_age,#floor,#total_floor,#rent,#deposit').on('keypress paste', function (e) {
    var txt = String.fromCharCode(e.which);
    if (!isInt(txt))
        return false;
});

function isInt(val) {
    if (val.match(/^-?\d*$/))
        return true;
    else
        return false;
}

$('#btn__edit_property_detail').click(function () {    
    var roomType = "";
    var tenantGender = "";
    var bhkType = "";
    var propertyType = $('#propertyType').val().toLowerCase();
    var apartmentType = $('.apartment_type.active').data('key');

    if (propertyType == "flat" || propertyType == "flatmate")
        bhkType = $('.bhk-type.active').data('key');

    if (propertyType == "flatmate")
        tenantGender = $('.tenant-gender.active').data('key');

    if (propertyType == "flatmate" || propertyType == "room")
        roomType = $('.room-type.active').data('key');

    var apartment = $('#apartment_name').val().trim();
    var area = $('#area').val();
    var facing = $('.facing.active').data('key');
    var propAge = $('#property_age').val();
    var floor = $('#floor').val();
    var totalFloor = $('#total_floor').val();
    var bathroom = $('.bathroom.active').data('key');
    var balcony = $('.balcony.active').data('key');
    var waterSupply = $('.water_supply.active').data('key');
    var gateSecurity = $('.gate_security.active').data('key');
    var isAgentAllowed = $('.agent_contact.active').data('key');

    if (propertyType == "" || propertyType == undefined)
    { showError("Invalid property type."); return false; }

    if (apartmentType == "" || apartmentType == undefined)
    { showError("Please select apartment type."); return false }

    if ((propertyType == "flat" || propertyType == "flatmate") && bhkType == "" || bhkType == undefined)
    { showError("Please select BHK type."); return false }

    if ((propertyType == "flatmate" || propertyType == "room") && (roomType == "" || roomType == undefined)) {
        showError("Please select Room Type."); return false;
    }

    if (apartment.length > 50)
    { showError("Apartment name shouldn't be more than 50 characters."); return false }

    if (area == "")
    { showError("Please enter build up area."); return false }

    if (floor == "" || floor == undefined)
        floor = "";
    else if (totalFloor == "")
        return showError("Total floor required.");

    if (totalFloor == "" || totalFloor == undefined)
        totalFloor = "";
    else if (floor == "")
        return showError("Floor no required.");

    if (floor != "" && totalFloor != "" && parseInt(floor) > parseInt(totalFloor))
        return showError("Floor no can't be greater than total floor.");

    var postData = {
        ApartmentType: apartmentType, BhkType: bhkType, RoomType: roomType, TenantGender: tenantGender, ApartmentName: apartment, PropertySize: area, Facing: facing, PropertyAge: propAge,
        FloorNo: floor, TotalFloor: totalFloor, BathRooms: bathroom, Balconies: balcony, WaterSupply: waterSupply, GateSecurity: gateSecurity,IsAgentAllowed:isAgentAllowed
    };

    $('#btn__edit_property_detail').html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdatePropertyDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({pid:$('#propertyId').val(),ptype:$('#propertyType').val(),property:postData }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn__edit_property_detail').html('Update').removeAttr("disabled");
        }
    });

});

$('#btn_property_detail_next').click(function () {
    $('#property_detail_info').hide();
    $('#locality_info').show();
});

$('#btn_next_locality').click(function () {
    $('#locality_info').hide();
    $('#rental_info').show();
});

$('#btn_edit_locality').click(function () {    
    var city = $('#hdnCity').val();
    var locality = $('#locality').val().trim();
    var street = $('#street').val().trim();
    var lat = $('#hdnLat').val().trim();
    var lng = $('#hdnLng').val().trim();
    var contactPersonName = $('#contactPersonName').val();
    var contactPersonMobile = $('#contactPersonMobile').val();

    if (contactPersonName === "") { showError("Contact person name required."); return false; }
    else if (contactPersonName.length > 50) { showError("Contact person name can't be more than 50 characters."); return false; }

    if (contactPersonMobile === "") { showError("Contact person mobile no required."); return false; }
    if (!validateMobile(contactPersonMobile)) { showError("Invalid Contact person mobile no."); return false; }

    //if (city == "")
    //  return showError("Please select city.");

    if (locality == "")
     return showError("Locality required.");
    else if (locality.length > 100)
     return showError("Maximum 100 characters allowed for locality.");

    if (lat == "" || lng == "")
     return showError("Please select locality from list.");

    if (street.length > 100)
    { showError("Maximum 100 characters allowed for street/area."); return false }

    var postData = { City: city, Locality: locality, Street: street, Latitude: lat, Longitude: lng, ContactPersonName: contactPersonName, ContactPersonMobile: contactPersonMobile };
    
    $('#btn_edit_locality').html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdateLocalityDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), property: postData }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_edit_locality').html('Update').removeAttr("disabled");
        }
    });
});

$('#btn_next_rental').click(function () {
    $('#rental_info').hide();
    $('#gallery_info').show();
});

$('#btn_edit_rental').click(function () {
    var rent = $('#rent').val().trim();
    var deposit = $('#deposit').val().trim();
    var availableFrom = $('#avaliable_from').val();
    var rentNegotiable = false;
    if ($("input[name='rent_negotiable']").is(':checked'))
        rentNegotiable = true;

    var preferredTenant = $('.preferred_tenant.active').data('key');
    var parking = $('.parking.active').data('key');
    var furnishing = $('.furnish.active').data('key');
    var furnishingDetail = [];
    var description = $('#description').val().trim();

    if (rent == "" || !isInt(rent))
       return showError("Expected rent should be numeric.");
    
    if (preferredTenant == "" || preferredTenant == undefined)
     return showError("Please select Preferred Tenant.");

    if (furnishing == "" || furnishing == undefined)
      return showError("Please select Furnishing.");

    if (furnishing == 'FF' || furnishing == 'SF') {
        $.each($(".ddFurnish option:selected"), function () {
            if ($(this).val() != "") {
                var FurnishingMasterId = $(this).parent().attr('id');
                var Value = $(this).val();
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });

        $('input[name="chkFurnish"]').each(function () {
            if ($(this).is(":checked")) {
                var FurnishingMasterId = $(this).attr("id");
                var Value = "1";
                furnishingDetail.push({ FurnishingMasterId, Value });
            }
        });
    }

    if (description.length > 500)
     return showError("Description shouldn't be more than 500 characters.");

    var postData = {
        ExpectedRent: rent, ExpectedDeposit: deposit, AvailableFrom: availableFrom, IsRentNegotiable: rentNegotiable, PreferredTenant: preferredTenant,
        Parking: parking, Furnishing: furnishing, FurnishingDetails: furnishingDetail, Description: description
    };

    $('#btn_edit_rental').html('Please Wait...  <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdateRentalDetail",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), ptype: $('#propertyType').val(), property: postData }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Details updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_edit_rental').html('Update').removeAttr("disabled");
        }
    });
    
});

$('#btn_gallery_next').click(function () {
    $('#gallery_info').hide();
    $('#amenity_info').show();
});

function removeGalleryPic(imgId) {
    $.ajax({
        url: "/property/DeleteGallery",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), image: imgId }),
        success: function (data, status) {
            if (data.Success == true)
                $('#img' + imgId).remove();
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        }
    });
}

$('.post-property-iamges').on("click", ".remove-picture", function (e) {
    var parentLi = $(this).parent();
    var filePath = $(this).data('file');
    if (filePath !== "" && filePath !== undefined) {
        $.grep(propertyPost.GalleryDetails, function (element, index) {
            if (element.FileName === filePath) {
                $.ajax({
                    url: "/property/RemoveGalleryFile",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({ imagePath: filePath }),
                    success: function (data, status) {
                        if (data.Success == true) {
                            parentLi.remove();
                            propertyPost.GalleryDetails.splice(index, 1);
                        }
                        else if (data.Success == false) {
                            showError(data.ErrorMessage);
                        }
                    }
                });
            }
        });
    }
});

$('#btn_edit_amenity').click(function () {
    var amenities = [];
    $('.amenity').each(function () {
        if ($(this).hasClass('active')) {
            var amenity = $(this).data("key");
            amenities.push({ AmenityId: amenity });
        }
    });

    $('#btn_edit_amenity').html('Please Wait...  <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.ajax({
        url: "/property/UpdateAmenity",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ pid: $('#propertyId').val(), amenities: amenities }),
        success: function (data, status) {
            if (data.Success == true)
                showSuccessMessage("Amenities updated successfully.");
            else if (data.Success == false) {
                showError(data.ErrorMessage);
            }
        },
        complete: function () {
            $('#btn_edit_amenity').html('Update').removeAttr("disabled");
        }
    });

});
