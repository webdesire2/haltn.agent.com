﻿
$('#btnPostProperty').click(function () {
    debugger;
    
    if ($('#hdnIsLoggedIn').val() == "no") { showLoginPopUp('login'); return false; }

    var propertyType = $('.property-type.active').data('key');
    var bhkType = ""; var roomType = ""; var pgRoom = "";
    bhkType = $('.bhk-type.active').data('key');
    roomType = $('.room-type.active').data('key');
    localStorage.setItem('propertyType', propertyType);
    if (propertyType == "" || propertyType == undefined)
        return showError('Please select property type.');

    if (propertyType == 'flat') {
        if (bhkType == "" || bhkType == undefined)
            return showError('Please select bhk type.');
        else
            window.location.href = "/property/post?ptype=" + propertyType + "&ht=" + bhkType;
    }
    //else if (propertyType == 'room' || propertyType == 'flatmate') {
    //    if (roomType == "" || roomType == undefined)
    //     return showError('Please select room type.');
    //    else
    //        window.location.href = "/property/post?ptype=" + propertyType + "&rt=" + roomType;
    //}
    else if (propertyType == 'room') {
        if (roomType == "" || roomType == undefined)
            return showError('Please select room type.');
        else
            window.location.href = "/property/postpg?rt=" + roomType;
        //window.location.href = "/property/postroom?rt=" + pgRoom;
    }
    else if (propertyType == 'pg') {
        $.each($(".pg-room-type.active"), function () {
            if (pgRoom == "")
                pgRoom = $(this).data('key');
            else
                pgRoom = pgRoom + '' + $(this).data('key');
        });

        if (pgRoom == "" || pgRoom == undefined) {
            return showError('Please select room type.');
        }
        else
            window.location.href = "/property/postpg?rt=" + pgRoom;
    }
});

$('.property-type').click(function () {
    debugger;
    $('.property-type').removeClass('active');
    $(this).addClass('active');
    var propertyType = $(this).data('key');
    $('.sub-type').addClass('d_none');
    if (propertyType == 'flat')
        $('#dv_bhk_type').removeClass('d_none');
    /* else if (propertyType == 'room' || propertyType == 'flatmate') { }*/
    //it is comment because we directly send as room type shared. 
    //$('#dv_room_type').removeClass('d_none');
    else if (propertyType == 'room') { }
    // $('#dv_room_type').removeClass('d_none');

    else if (propertyType == 'pg')
        $('#dv_pg_room').removeClass('d_none');
});






$('.bhk-type').click(function () {
    $('.bhk-type').removeClass('active');
    $(this).addClass('active');
});

$('.room-type').click(function () {
    $('.room-type').removeClass('active');
    $(this).addClass('active');
});

$('.pg-room-type').click(function () {
    if ($(this).hasClass("active"))
        $(this).removeClass("active");
    else
        $(this).addClass('active');
});


//Javascript for default shared rooms
//$(document).ready(function () {
//    $(".room-type[data-key='sr']").addClass('active');
//});
$(document).ready(function () {
    $(".room-type[data-key='s']").addClass('active');
});

