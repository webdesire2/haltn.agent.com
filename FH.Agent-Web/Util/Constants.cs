﻿namespace FH.Util
{
    public class Constants
    {
        public const string success = "Operation Successfull.";
        public const string failure = "Operation Failed.Try Again.";
        public const string alreadyExist = "Data Already Exists";
        public const string loginFailure = "Incorrect Username and Password.";
        public const string secusfullyRegistered = "Your registration is succesfully done.";
    }
}
