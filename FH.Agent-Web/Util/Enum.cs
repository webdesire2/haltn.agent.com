﻿
namespace FH.Util
{
    enum EnPropertyType
    {
     Flat=1,
     Room=2,
     Flatmate=3,
     PG=4                  
    }

   public enum EnPropertyStatusByFH
    {
        Pending,
        Rejected,
        Approved,
        RentOut,
        Cancelled
    }

    enum EnPropertyStatusByUser
    {
        Active,
        Deactive
    }
}