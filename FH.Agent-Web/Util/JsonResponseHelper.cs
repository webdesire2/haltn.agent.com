﻿using System.Web.Mvc;

namespace FH.Util
{
    public class JsonResultHelper
    {
        public static JsonResult Success(string message)
        {
           return new JsonResult(){
               Data = new{Success=true,Message=message},              
           };
        }

        public static JsonResult Error(string message)
        {
            return new JsonResult()
            {
                Data = new { Success = false, Message = message },
            };
        }
    }
}