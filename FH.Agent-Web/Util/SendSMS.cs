﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using System.Configuration;

namespace FH.Util
{
    public class SendSMS
    {
        public static string Send(string mobileNo, string message, string templateID)
        {
            try
            {
                String sMessage = HttpUtility.UrlEncode(message);
                using (var wb = new WebClient())
                {
                    string URI = "http://5.9.97.88/api/sendhttp.php?";
                    string myParameters = "authkey=32366861726d6138333698&sender=HALTN&route=2&country=91&DLT_TE_ID=" + templateID  + "&";

                    myParameters += "mobiles=91" + mobileNo + "&message=" + message;

                    myParameters = string.Concat(URI, myParameters);
                    string result = wb.DownloadString(myParameters);
                    return result;
                }
            }
            catch
            {
                return "";
            }

        }
    }
}