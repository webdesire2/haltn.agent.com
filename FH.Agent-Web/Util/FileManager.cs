﻿using System;
using System.Configuration;
using System.IO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
namespace FH.Util
{   
        public class FileManager
        {
            private static readonly bool isProd = Convert.ToBoolean(ConfigurationManager.AppSettings["IsProductionMode"]);
            private static readonly string BUCKET_NAME = "haltn"; //(Convert.ToBoolean(ConfigurationManager.AppSettings["IsProductionMode"]) ? "telonestay" : "flathalttest");
            private static readonly string AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
            private static readonly string AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();
            public static string FilePath(string fileName)
            {
                Random generator = new Random();
                String randomNumber = generator.Next(0, 999999).ToString("D6");
                DateTime currentDate = DateTime.Now;
                string directoryPath = "Uploads/" + currentDate.Year.ToString() + "/" + currentDate.Month.ToString();
                string fullDirectoryPath = ConfigurationManager.AppSettings["GalleryFolderPath"].ToString() + directoryPath;
                // string fullDirectoryPath = AppDomain.CurrentDomain.BaseDirectory + directoryPath;
                string filePath = directoryPath + "/" + randomNumber + fileName;

                if (!Directory.Exists(fullDirectoryPath))
                {
                    Directory.CreateDirectory(fullDirectoryPath);
                }

                return filePath;
            }
            public static bool RemoveFile(string filePath)
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + filePath))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + filePath);
                    return true;
                }
                return false;
            }

            public static S3Response Upload(Stream inputFile, S3Folder folder, string fileName, string ext)
            {
                String randomNumber = new Random().Next(0, 999999).ToString("D6");
                fileName = fileName.Length > 10 ? fileName.Substring(0, 10) : fileName;
                fileName = string.Format("{0}{1}{2}", randomNumber, fileName, ext);

                string filePath = string.Empty;
                if (isProd)
                    filePath = (folder == S3Folder.Property ? "Property/" + fileName : fileName);
                else                    
                    filePath = "Test/" + fileName;
                try
                {
                    AWSConfigs.S3Config.UseSignatureVersion4 = true;
                    using (IAmazonS3 s3Client = new AmazonS3Client(AWSAccessKey, AWSSecretKey, RegionEndpoint.GetBySystemName("ap-south-1")))
                    {
                        PutObjectRequest request = new PutObjectRequest
                        {
                            BucketName = BUCKET_NAME,
                            Key = filePath,
                            InputStream = inputFile
                        };

                        s3Client.PutObject(request);
                    }
                    return new S3Response() { Success = true, FileName = filePath, FileUrl = string.Format("https://{0}.s3.ap-south-1.amazonaws.com/{1}", BUCKET_NAME, filePath) };
                }
                catch (Exception e)
                {
                    return new S3Response() { Success = false, FileName = "" };
                }
            }
            public static bool Delete(string filePath)
            {
                try
                {
                    //fileName = (folder == S3Folder.PropertyImage ? "Property/" + fileName : fileName);
                    using (IAmazonS3 s3Client = new AmazonS3Client(AWSAccessKey, AWSSecretKey, RegionEndpoint.GetBySystemName("ap-south-1")))
                    {
                        DeleteObjectRequest request = new DeleteObjectRequest
                        {
                            BucketName = BUCKET_NAME,
                            Key = filePath
                        };
                        DeleteObjectResponse response = s3Client.DeleteObject(request);
                        return true;
                    }
                }
                catch (Exception e)
                {
                    return false;
                }
            }

            public enum S3Folder
            {
                Property                
            }


            public class S3Response
            {
                public bool Success { get; set; }
                public string FileName { get; set; }
                public string FileUrl { get; set; }
            }
        }
}