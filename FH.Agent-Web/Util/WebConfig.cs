﻿using System.Configuration;

namespace FH.User_Web
{
    public class WebConfig
    {
        public static string Logo = ConfigurationManager.AppSettings["LogoUrl"];
    }
}