﻿using System.Data;
using System.Data.Common;
using System.Configuration;
namespace FH.User.Service
{
    public class ConnectionFactory
    {         
       private readonly static string connectionString = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
        public static IDbConnection GetConnection
        {
            get
            {
                var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                var conn = factory.CreateConnection();
                conn.ConnectionString = connectionString;               
                return conn;
            }            
        }
    }
}
