﻿using FH.User.Entities;
using FH.User.Data;
using System.Data;
using System.Collections.Generic;

namespace FH.User.Service
{
    public class AmenitiesMasterBL : IAmenitiesMasterBL
    {
        private readonly IAmenitiesMasterDA amenityMasterDA;
        public AmenitiesMasterBL(IDbTransaction transaction)
        {
            amenityMasterDA = new AmenitiesMasterDA(transaction);
        }
        public IEnumerable<AmenityMaster> GetAll(string amenityType=null)
        {
            return amenityMasterDA.GetAll(amenityType);
        }
    }

    public interface IAmenitiesMasterBL
    {
        IEnumerable<AmenityMaster> GetAll(string amenityType=null);
    }
}
