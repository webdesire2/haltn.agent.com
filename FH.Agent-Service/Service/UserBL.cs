﻿using System.Data;
using FH.User.Data;
using FH.User.Entities;

namespace FH.User.Service
{
    public class UserBL : IUserBL
    {
        private readonly IUserDA _IUserDA;
        public UserBL(IDbTransaction transaction)
        {
            _IUserDA = new UserDA(transaction);
        }

        public UserDetail FindByEmailMobile(UserDetail entity)
        {
            return _IUserDA.FindByEmailMobile(entity);
        }

        public int AddUser(UserDetail entity)
        {
            return _IUserDA.AddUser(entity);
        }
        public int UpdateUser(UserDetail entity)
        {
            return _IUserDA.UpdateUser(entity);
        } 
        public UserDetail IsValidLogin(UserDetail entity)
        {
            return _IUserDA.IsValidLogin(entity);
        }

        public UserDetail ResetPassword(UserDetail user)
        {
            return _IUserDA.ResetPassword(user);
        }
        public UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn)
        {
            return _IUserDA.IsValidOTP(userId, otp, otpExpiredOn);
        }

        public string GetUserByMobile(UserDetail entity)
        {
            return _IUserDA.GetUserByMobile(entity);
        }
        //public UserDetail GetUserByUserId(int userId)
        //{
        //    return _IUserDA.GetUserByUserId(userId);
        //}
    }

    public interface IUserBL
    {
        UserDetail FindByEmailMobile(UserDetail entity);
        int AddUser(UserDetail entity);
        int UpdateUser(UserDetail entity);
        UserDetail IsValidLogin(UserDetail entity);
        UserDetail ResetPassword(UserDetail user);
        UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn);
        string GetUserByMobile(UserDetail entity);
        //UserDetail GetUserByUserId(int userId);
    }
}
