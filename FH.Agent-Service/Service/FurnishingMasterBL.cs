﻿using FH.User.Entities;
using FH.User.Data;
using System.Data;
using System.Collections.Generic;

namespace FH.User.Service
{
   public class FurnishingMasterBL:IFurnishingMasterBL
    {
        private readonly IFurnishingMasterDA furnishingMasterDA;
        public FurnishingMasterBL(IDbTransaction transaction)
        {
            furnishingMasterDA = new FurnishingMasterDA(transaction);
        }

        public IEnumerable<FurnishingMaster> GetAll()
        {
            return furnishingMasterDA.GetAll();
        }
    }

    public interface IFurnishingMasterBL
    {
        IEnumerable<FurnishingMaster> GetAll();
    }
}
