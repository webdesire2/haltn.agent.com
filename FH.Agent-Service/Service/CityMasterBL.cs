﻿using FH.User.Entities;
using FH.User.Data;
using System.Collections.Generic;

namespace FH.User.Service
{
    public class CityMasterBL
    {
        private readonly CityMasterDA _cityDA;

        public CityMasterBL()
        {
            _cityDA = new CityMasterDA();
        }

        public IEnumerable<CityMaster> GetAllCity()
        {
            return _cityDA.Execute<CityMaster>(CityMasterCallValue.GetAllCity);
        }
    }
}
