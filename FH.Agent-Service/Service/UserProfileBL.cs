﻿using FH.User.Entities;
using FH.User.Data;
using System.Data;
using System.Collections.Generic;

namespace FH.User.Service
{
    public class UserProfileBL: IUserProfileBL
    {
        private readonly IUserProfileDA _IUserProfileDA;
        public UserProfileBL(IDbTransaction transaction)
        {
            _IUserProfileDA = new UserProfileDA(transaction);
        }
        public UserProfile GetUserDetail(int userId)
        {
            return _IUserProfileDA.GetUserDetail(userId);
        }

        public IEnumerable<UAccount> GetUserAccount(int userId)
        {
            return _IUserProfileDA.GetUserAccount(userId);
        }
        public bool UpdatePersonalDetail(UserProfile entity)
        {
            return _IUserProfileDA.UpdatePersonalDetail(entity);
        }        
        public bool UpdateBusinessDetail(UserProfile entity)
        {
            return _IUserProfileDA.UpdateBusinessDetail(entity);
        }
        public bool AccountAdd(UAccount entity, string[] uploadedFilePaths)
        {
            return _IUserProfileDA.AccountAdd(entity, uploadedFilePaths);
        }
        public bool AccountUpdate(UAccount entity)
        {
            return _IUserProfileDA.AccountUpdate(entity);
        }
    }

    public interface IUserProfileBL
    {
        UserProfile GetUserDetail(int userId);
        IEnumerable<UAccount> GetUserAccount(int userId);
  
        bool UpdatePersonalDetail(UserProfile entity);
        bool AccountAdd(UAccount entity, string[] uploadedFilePaths); 
        bool AccountUpdate(UAccount entity);
        bool UpdateBusinessDetail(UserProfile entity);
    }
}

