﻿using FH.User.Entities;
using FH.User.Data;
using System.Collections.Generic;
using System.Linq;

namespace FH.User.Service
{
    public class TenantBL
    {
        private readonly TenantDA _tenantDA;

        public TenantBL()
        {
            _tenantDA = new TenantDA();
        }

        public int AddTenant(TenantMaster de)
        {
            return _tenantDA.ExecuteWithTransaction<int>(de, TenantCallValue.AddTenant);
        }

        public int EditTenant(TenantMaster de)
        {
            return _tenantDA.ExecuteWithTransaction<int>(de, TenantCallValue.EditTenant);
        }

        public QueryResponse CollectPayment(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.CollectPayment).FirstOrDefault();
        }

        public QueryResponse RSendPaymentLink(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.RSendPaymentLink).FirstOrDefault();
        }

        public QueryResponse SSendPaymentLink(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.SSendPaymentLink).FirstOrDefault();
        } 

        public QueryResponse CollectSecurity(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.CollectSecurity).FirstOrDefault();
        }

        public QueryResponse RefundSecurity(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.RefundSecurity).FirstOrDefault();
        }

        public TanentStayDetails GetTanentStayDetails(TenantMaster de)
        {
            return _tenantDA.Execute<TanentStayDetails>(de, TenantCallValue.GetTanentStayDetails).FirstOrDefault(); 
        }

        public IEnumerable<TenantMaster> GetAllTenant(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetAllTenant);
        }
        public IEnumerable<UserDetail> GetMyTeam(TenantMaster de)
        {
            return _tenantDA.Execute<UserDetail>(de, TenantCallValue.GetMyTeam);
        }


        public QueryResponse UpdateMyTeam(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.UpdateMyTeam).FirstOrDefault();
        }
        public QueryResponse AddMyTeam(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.AddMyTeam).FirstOrDefault();
        }

        public IEnumerable<TenantMaster> GetAllTenantDetails(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetAllTenantDetails);
        } 

        public IEnumerable<PGRoomData> getAvailableRooms(TenantMaster de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.getAvailableRooms);
        }

        public IEnumerable<PropertyList> getMyProperties(TenantMaster de)
        {
            return _tenantDA.Execute<PropertyList>(de, TenantCallValue.getMyProperties);
        } 
        public PGRoomData getTenantRooms(TenantMaster de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.getTenantRooms).FirstOrDefault();
        } 

        public IEnumerable<TenantMaster> GetRentPaidTanent(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetRentPaidTanent);
        }
        public IEnumerable<TenantMaster> GetRentPaidTanentTest(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetRentPaidTanentTest);
        }


        public IEnumerable<TenantMaster> GetSingleTenant(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetSingleTenant);
        }

        public IEnumerable<TenantPaymentLogDTO> GetTenantPaymentLog(TenantMaster de)
        {
            return _tenantDA.Execute<TenantPaymentLogDTO>(de, TenantCallValue.GetTenantPaymentLog);            
        }
    }
}
