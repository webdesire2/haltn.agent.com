﻿using FH.User.Entities;
using FH.User.Data.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace FH.User.Service
{
    public class PaymentBL
    {
        private readonly PaymentDA _paymentDA;

        public PaymentBL()
        {
            _paymentDA = new PaymentDA();
        }

        public IEnumerable<PaymentCollection> GetCurrentMonthReceivedPayment(PaymentDE de)
        {
            return _paymentDA.Execute<PaymentCollection>(de, PaymentCallValue.GetCurrentMonthReceivedPayment);
        }

        public IEnumerable<PaymentCollection> GetCurrentMonthReceivedPaymentPen(PaymentDE de)
        {
            return _paymentDA.Execute<PaymentCollection>(de, PaymentCallValue.GetCurrentMonthReceivedPaymentPen);
        }


        public IEnumerable<TotalPayment> GetTotalofPayment(PaymentDE de)
        {
            return _paymentDA.Execute<TotalPayment>(de, PaymentCallValue.GetCurrentMonthReceivedPaymentTotal);
        }

        public IEnumerable<QueryResponse> UpdatePayment(PlanMaster de)
        {
            return _paymentDA.Execute<QueryResponse>(de, PaymentCallValue.UpdatePayment);
        }

        public IEnumerable<QueryResponse> UpdatePaymentUser(PaymentDetail de)
        {
            return _paymentDA.Execute<QueryResponse>(de, PaymentCallValue.UpdatePaymentUser);
        } 

        public InvoiceDTO GetTenantInvoice(int paymentId, int userId)
        {
            PaymentDE de = new PaymentDE()
            {
                PaymentId = paymentId,
                CreatedBy = userId
            };
 
            return _paymentDA.Execute<InvoiceDTO>(de, PaymentCallValue.GetTenantInvoice).FirstOrDefault();
        } 

        public string ErrorCode(string code)
        {
            string desc = "";
            switch (code)
            {
                case "E000": desc = "Transaction Success"; break;
                case "E001": desc = "Unauthorized Payment Mode"; break;
                case "E006": desc = "Transaction Already Paid, Received Confirmation from the Bank, Yet to Settle the transaction with the Bank"; break;
                case "E007": desc = "Transaction Failed"; break;
                case "E008": desc = "Failure from Third Party due to Technical Error"; break;
                case "E0039": desc = "Mandatory value Email in wrong format"; break;
                case "E00310": desc = "Mandatory value mobile number in wrong format"; break;
                case "E00331": desc = "UPI Transaction Initiated Please Accept or Reject the Transaction"; break;
                case "E0801": desc = "FAIL"; break;
                case "E0803": desc = "Canceled by user"; break;
                case "E0815": desc = "Not sufficient funds"; break;

                default: desc = code; break;
            }

            return desc;
        }

    }
}
