﻿
using FH.User.Entities;
using FH.User.Data;
using System.Data;
using System.Collections.Generic;

namespace FH.User.Service
{
   public class PropertyBL:IPropertyBL
    {
        private readonly IPropertyDA propertyDA;        
        public PropertyBL(IDbTransaction transaction)
        {
            propertyDA = new PropertyDA(transaction);       
        }
        public IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, int userId, out int totalRecord)
        {
            return propertyDA.GetAllProperty(input,userId,out totalRecord);
        }

        public Property GetProperty(int propertyId, int UserId)
        {
            return propertyDA.GetProperty(propertyId, UserId);
        }
        public int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity)
        {
           return propertyDA.AddPropertyFlatRoomFlatMate(entity);
        }

        public int AddPropertyPG(PropertyPG entity)
        {
            return propertyDA.AddPropertyPG(entity);
        }
        public string UpdatePropertyDetail(int propertyId, string propertyType, int UserId, PropertyDetailFlatRoom entity)
        {
            return propertyDA.UpdatePropertyDetail(propertyId, propertyType, UserId, entity);
        }

        public string UpdateLocalityDetail(int propertyId, int UserId, PropertyMaster entity)
        {
            return propertyDA.UpdateLocalityDetail(propertyId, UserId, entity);
        }
        public string UpdateRentalDetail(int propertyId, string propertyType, int UserId, PropertyDetailFlatRoom entity)
        {
            return propertyDA.UpdateRentalDetail(propertyId, propertyType, UserId, entity);
        }
        public string AddNewGallery(int propertyId, int UserId, string file)
        {
            return propertyDA.AddNewGallery(propertyId, UserId, file);
        }
        public string DeleteGallery(int propertyId, int UserId, int imageId)
        {
            return propertyDA.DeleteGallery(propertyId, UserId, imageId);
        }
        public string UpdatePropertyAmenity(int propertyId, int UserId, IEnumerable<PropertyAmenitiesDetails> entity)
        {
           return propertyDA.UpdatePropertyAmenity(propertyId, UserId, entity);
        }        
        public string UpdatePGRoomDetail(int propertyId, int UserId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities)
        {
            return propertyDA.UpdatePGRoomDetail(propertyId, UserId, roomRentalDetail, roomAmenities);
        }

        public string EditRoom(PGRoomData PGRoomData)
        {
            return propertyDA.EditRoom(PGRoomData);
        }

        public string AddRoom(PGRoomData PGRoomData)
        {
            return propertyDA.AddRoom(PGRoomData);
        }

        //EditRoom( PGRoomData PGRoomData);
        public string UpdatePGDetail(int propertyId, int UserId, PGDetail pgDetail, IEnumerable<PGRule> pgRules)
        {
            return propertyDA.UpdatePGDetail(propertyId, UserId, pgDetail, pgRules);
        }
        public IEnumerable<PropertyList> MyPropertyVisit(int userId)
        {
            return propertyDA.MyPropertyVisit(userId);
        }

        public string GetPropertyStatus(int propertyId)
        {
            return propertyDA.GetPropertyStatus(propertyId);
        }

        public bool UpdatePropertyStatus(PropertyMaster de)
        {
            return propertyDA.UpdatePropertyStatus(de);
        }
    }

    public interface IPropertyBL
    {
        IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, int userId, out int totalRecord);
        Property GetProperty(int propertyId, int UserId);
        int AddPropertyFlatRoomFlatMate(PropertyFlatRoom entity);
        int AddPropertyPG(PropertyPG entity);
        string UpdatePropertyDetail(int propertyId, string propertyType, int UserId, PropertyDetailFlatRoom entity);
        string UpdateLocalityDetail(int propertyId, int UserId, PropertyMaster entity);
        string UpdateRentalDetail(int propertyId, string propertyType, int UserId, PropertyDetailFlatRoom entity);
        string AddNewGallery(int propertyId, int UserId, string file);
        string DeleteGallery(int propertyId, int UserId, int imageId);
        string UpdatePropertyAmenity(int propertyId, int UserId, IEnumerable<PropertyAmenitiesDetails> entity);
        string UpdatePGRoomDetail(int propertyId, int UserId, IEnumerable<PGRoomRentalDetail> roomRentalDetail, IEnumerable<PGRoomAmenity> roomAmenities);
        string EditRoom( PGRoomData PGRoomData);
        string AddRoom(PGRoomData PGRoomData);
        string UpdatePGDetail(int propertyId, int UserId, PGDetail pgDetail, IEnumerable<PGRule> pgRules);
        IEnumerable<PropertyList> MyPropertyVisit(int userId);
        string GetPropertyStatus(int propertyId);
        bool UpdatePropertyStatus(PropertyMaster de);
    }
}
