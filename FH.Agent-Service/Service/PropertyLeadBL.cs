﻿using FH.User.Data;
using System.Collections.Generic;
using System.Data;
using FH.User.Entities;
using System;

namespace FH.User.Service
{
    public class PropertyLeadBL:IPropertyLeadBL
    {
        private readonly IPropertyLeadDA leadDA;
        public PropertyLeadBL(IDbTransaction transaction)
        {
            leadDA = new PropertyLeadDA(transaction);
        }
        public IEnumerable<PropertyLead> GetAllLead(int propertyId, int userId)
        {
            return leadDA.GetAllLead(propertyId,userId);
        }
        public IEnumerable<PropertyLead> GetTodayVisitLead(int propertyId, int userId)
        {
            return leadDA.GetAllLead(propertyId, userId,DateTime.Now);
        }

        public bool UpdateScheduleVisit(PropertyLead entity, int userId)
        {
            return leadDA.UpdateScheduleVisit(entity, userId);
        }
        public IEnumerable<PropertyLead> GetAllMyLead(int userId)
        {
            return leadDA.GetAllMyLead(userId);
        }
        public IEnumerable<PropertyLead> GetLeadLog(int agentId, int visitorId)
        {
            return leadDA.GetLeadLog(agentId, visitorId);
        }
        public string AddNewLead(PropertyLead entity)
        {
            return leadDA.AddNewLead(entity);
        }
    }

    public interface IPropertyLeadBL
    {
        IEnumerable<PropertyLead> GetAllLead(int propertyId, int userId);
        bool UpdateScheduleVisit(PropertyLead entity, int userId);
        IEnumerable<PropertyLead> GetTodayVisitLead(int propertyId, int userId);
        IEnumerable<PropertyLead> GetAllMyLead(int userId);
        IEnumerable<PropertyLead> GetLeadLog(int agentId, int visitorId);
        string AddNewLead(PropertyLead entity);
    }
}
