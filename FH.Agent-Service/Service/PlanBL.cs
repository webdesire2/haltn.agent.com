﻿using FH.User.Entities;
using FH.User.Data;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Net;
using System;
using CCA.Util;
using System.Xml.Linq;

namespace FH.User.Service
{
    public class PlanBL
    {
        private readonly PlanDA _PlanDA;

        public PlanBL()
        {
            _PlanDA = new PlanDA();
        }
         
        public IEnumerable<PlanMaster> GetMyPlans(PlanMaster de)
        {
            return _PlanDA.Execute<PlanMaster>(de, PlanCallValue.GetMyPlans);
        }

        public IEnumerable<PlanMaster> FetchOrder(PlanMaster de)
        {
            return _PlanDA.Execute<PlanMaster>(de, PlanCallValue.FetchOrder);
        }

        public IEnumerable<PropertyList> GetMyProperty(PlanMaster de)
        {
            return _PlanDA.Execute<PropertyList>(de, PlanCallValue.GetProperty);
        }

        public IEnumerable<PropertyList> GetMyPropertyNotByPackage(PlanMaster de)
        {
            return _PlanDA.Execute<PropertyList>(de, PlanCallValue.GetMyPropertyNotByPackage);
        }

        

        public IEnumerable<PlanMaster> AddPlanOrder(PlanMaster de)
        {
            return _PlanDA.Execute<PlanMaster>(de, PlanCallValue.AddPlanOrder);
        }

        //

        public IEnumerable<PlanMaster> GetPackages(PlanMaster de)
        {
            return _PlanDA.Execute<PlanMaster>(de, PlanCallValue.GetPackages);
        }

        public IEnumerable<PlanMaster> GetMyOder(PlanMaster de)
        { 
            return _PlanDA.Execute<PlanMaster>(de, PlanCallValue.GetMyOder);
        }

        public int AddPlanToVendor(PlanMaster de)
        {
            return _PlanDA.ExecuteWithTransaction<int>(de, PlanCallValue.AddPlanToVendor);
        }

        public int DeleteOrder(PlanMaster de)
        {
            return _PlanDA.ExecuteWithTransaction<int>(de, PlanCallValue.DeleteOrder);
        }


        public string EPPaymentRequest(double amount, int orderId)
        {
            try
            {
                CCACrypto ccaCrypto = new CCACrypto();
                string CCAMerchentID = ConfigurationManager.AppSettings["CCAMerchentID"].ToString();
                string CCAccessCode = ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                string CCAWorkingKey = ConfigurationManager.AppSettings["CCAWorkingKey"].ToString();
                string CCARedirectUrl = ConfigurationManager.AppSettings["CCARedirectUrl"].ToString();
                string CCAProdUrl = ConfigurationManager.AppSettings["CCAProdUrl"].ToString();
                string ccaRequest = "";
                string strEncRequest="";
                ccaRequest = "merchant_id=" + CCAMerchentID + "&order_id=" + orderId + "&amount=" + amount + "&currency=INR&redirect_url=" + CCARedirectUrl + "&cancel_url=" + CCARedirectUrl + "";
                strEncRequest = ccaCrypto.Encrypt(ccaRequest, CCAWorkingKey);

                return strEncRequest;
            }
            catch (Exception ex)
            {
                throw new ApplicationException();
            }
        }

    }
}
