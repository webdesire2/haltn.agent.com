﻿using System;
using System.Data;

namespace FH.User.Service
{
    public class UnitOfWork : IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IUserBL _User;
        private IPropertyBL _property;
        private IFurnishingMasterBL _furnishingMaster;
        private IAmenitiesMasterBL _amenityMaster;
        private IUserProfileBL _userProfile;
        private IPropertyLeadBL _leadBL;
       
        public UnitOfWork()
        {
            _connection = ConnectionFactory.GetConnection;
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IUserBL UserBL
        {
            get
            {
                return _User ?? (_User = new UserBL(_transaction));
            }
        }

        public IPropertyBL PropertyBL
        {

            get
            {
                return _property ?? (_property = new PropertyBL(_transaction));
            }
        }

        public IFurnishingMasterBL FurnishingMaster
        {
            get
            {
                return _furnishingMaster ?? (_furnishingMaster = new FurnishingMasterBL(_transaction));
            }
        }
        public IAmenitiesMasterBL AmenityMaster
        {
            get
            {
                return _amenityMaster ?? (_amenityMaster = new AmenitiesMasterBL(_transaction));
            }
        }

        public IUserProfileBL UserProfileBL
        {
            get
            {
                return _userProfile ?? (_userProfile = new UserProfileBL(_transaction));
            }
        }

        public IPropertyLeadBL PropertyLeadBL
        {
            get
            {
                return _leadBL ?? (_leadBL = new PropertyLeadBL(_transaction));
            }
        }
        
        private void Reset()
        {
            _User = null;
            _property = null;
            _furnishingMaster = null;
            _amenityMaster = null;
            _userProfile = null;
            _leadBL = null;

        }
        public void Commit()
        {
            try
            {
                _transaction.Commit();               
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }

                Reset();
            }
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }

            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }

    }
}
