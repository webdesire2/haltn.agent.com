﻿using System;

namespace FH.User.Entities
{
   public class PropertyList
    {
        public string PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyType { get; set; }
        public string PropertyStatus { get; set; }
        public string Locality { get; set; }
        public string Street { get; set; }
        public string ApartmentType { get; set; }
        public string BhkType { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public string PropertySize { get; set; }
        public string PreferredTenants { get; set; }
        public string TenantGender { get; set; }        
        public string Furnishing { get; set; }
        public string ImageUrl { get; set; }
        public int PropertyLead { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonMobile { get; set; }
    }
}
