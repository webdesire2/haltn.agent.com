﻿
using System.Collections.Generic;

namespace FH.User.Entities
{
    public class PropertyDetailFlatRoom
    {
        public string ApartmentType { get; set; }
        public string ApartmentName { get; set; }
        public string BhkType { get; set; }
        public string RoomType { get; set; }
        public string TenantGender { get; set; }       
        public string PropertySize { get; set; }
        public string Facing { get; set; }
        public string PropertyAge { get; set; }
        public int? FloorNo { get; set; }
        public int? TotalFloor { get; set; }
        public int BathRooms { get; set; }
        public int Balconies { get; set; }
        public string WaterSupply { get; set; }
        public string GateSecurity { get; set; }
        public string NonVegAllowed { get; set; }
        public int Cupboard { get; set; }
        public bool AvaliableForLease { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public bool IsRentNegotiable { get; set; }
        public string AvailableFrom { get; set; }
        public string PreferredTenant { get; set; }
        public string Parking { get; set; }
        public string Furnishing { get; set; }
        public string Description { get; set; }
        public bool IsAgentAllowed { get; set; }
        public IEnumerable<PropertyFurnishingDetails> FurnishingDetails { get; set; }
    }
}
