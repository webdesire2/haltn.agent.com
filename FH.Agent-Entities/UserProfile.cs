﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace FH.User.Entities
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public string BankAccountHolderName { get; set; }
        public string BankAccountNo { get; set; }
        public string ConfirmAccountNo { get; set; }
        public string BankIFSCCode { get; set; }
        public string CompanyName { get; set; }
        public string OwnerName { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficialEmail { get; set; }
        public string GSTNo { get; set; }
        public string PANNo { get; set; }
    }


    public class UAccount
    {
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Ifsccode { get; set; }
        public string BankName { get; set; }
        public string AccountHolderName { get; set; }
        public string PaytmMID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int IsActive { get; set; }
        public string AccountPan {  get; set; }
        public string PaidToMID { get; set; }   
        public string FilePath { get; set; }

        public string AccountType { get; set;  }
    }


    public class UserData
    {
        public UserProfile UserProfile { get; set; }
        public IEnumerable<UAccount> UAccount { get; set; }
    }
}
