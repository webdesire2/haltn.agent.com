﻿using System;
namespace FH.User.Entities
{
    public class TenantPaymentLogDTO
    {
        public int PaymentId { get; set; }
        public double DepositAmount { get; set; }
        public string PaymentType { get; set; }
        public DateTime? RentForMonthStartDate { get; set; }        
        public DateTime? RentForMonthEndDate { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentMode { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
