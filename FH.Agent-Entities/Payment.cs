﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.User.Entities
{
    public class PaymentDE
    {
        public int TenantId { get; set; }
        public int UserId { get; set; }
        public int PaymentId { get; set; }
        public int CreatedBy { get; set; }
        public int PropertyId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }


    public class TotalPayment
    {
        public double TotalofPayment { get; set; }
    }

    public class PaymentCollection
    {
        public int PaymentId { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public string TenantMobile { get; set; }
        public int PropertyId { get; set; }
        public string PaymentReceivedOn { get; set; }
        public double Amount { get; set; }
        public string RentFrom { get; set; }
        public string RentTo { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMode { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public int PaymentStatus { get; set; }
        public int RoomNo { get; set; }
    }

    public class InvoiceDTO
    {
        public int PaymentId { get; set; }
        public string TenantName { get; set; }
        public string TenantMobile { get; set; }
        public string OwnerName { get; set; }
        public string StatusMessage { get; set; }
        public string CompanyName { get; set; }
        public string OwnerMobile { get; set; }
        public string PropertyId { get; set; } 
        public string Street { get; set; }
        public string PaymentType { get; set; }
        public DateTime? RentFromDate { get; set; }
        public DateTime? RentToDate { get; set; }
        public double Amount { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentMode { get; set; }
        public string GSTNo { get; set; }
        public string PANNo { get; set; }
        public string PropertyName { get; set; }
    }
}
