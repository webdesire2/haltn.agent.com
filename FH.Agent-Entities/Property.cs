﻿using System.Collections.Generic;
namespace FH.User.Entities
{
public class Property
    {   
        public PropertyFlatRoom PropertyFlatRoomFlatmate { get; set; } 
        public PropertyPG PropertyPG { get; set; }
        public IEnumerable<AmenityMaster> AmenityMaster { get; set; }
        public IEnumerable<FurnishingMaster> FurnishMaster { get; set; }

        public string lavel {  get; set; }

    }
}
