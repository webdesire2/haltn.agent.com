﻿
namespace FH.User.Entities
{
   public class FurnishingMaster
    {
        public int FurnishingMasterId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
