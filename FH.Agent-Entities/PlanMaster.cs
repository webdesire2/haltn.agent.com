﻿using System;
using System.Collections.Generic;

namespace FH.User.Entities
{
    public class PlanMaster
    {
        public int PlanId { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public int Validity { get; set; }
        public int NumberOfLeads { get; set; }
        public int Status { get; set; }
        public int VarifiedTag { get; set; }
        public string Color { get; set; }
        public string SEOData { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int PropertyId { get; set; }
        public double Payment { get; set; }
        public int PlanOrderID { get; set; }
        public string PaymentMode { get; set; }
        public DateTime? PaidOn { get; set; }
        public int PaymentStatus { get; set; }
        public int PlanAssignID { get; set; }
        public string RorderId { get; set; }
        public string RPaymentId { get; set; }
        public string StatusCode { get; set; }
        public string TracingId { get; set; }
        public string BankRefNo { get; set; }
        public string StatusMessage { get; set; }
        public DateTime? PlanEndDate { get; set; }
        public string Locality { get; set; }
     
    } 
     
   


    public class PlanSellMaster
    {
        public int PlanAssignID { get; set; }
        public int PlanID { get; set; }
        public int PropertyId { get; set; }
        public int CreatedBy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public string SEOData { get; set; }
        public double Payment { get; set; }
        public int PaymentStatus { get; set; }
        public string PaymentMethod { get; set; }
        public int NumberOfLeads { get; set; }
        public int VarifiedTag { get; set; }
        public DateTime? PlanEndDate { get; set; }
        public DateTime? PaidOn { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

    }

}
