﻿namespace FH.User.Entities
{
    public class PGRoomRentalDetail
    {
        public string RoomType { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public int TotalSeat { get; set; }
        public int AvailableSeat { get; set; }
    }
}
