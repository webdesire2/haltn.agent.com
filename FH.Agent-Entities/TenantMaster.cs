﻿using System;
using System.Collections.Generic;

namespace FH.User.Entities
{
    public class TenantMaster
    {
        public int TenantId { get; set; }
        public int UserId { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public int RoomNo { get; set; }
        public int RoomId { get; set; }
        public string UserType {  get; set; }
        public float Occupancy { get; set; }
        public double ThisMonthRentPaid { get; set; }
        public double ThisMonthRent { get; set; }
        public int PaymentId { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }        
        public double SecurityAmount { get; set; }
        public double MonthlyRent { get; set; }        
        public double SecurityDeposit { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public bool IsActive { get; set; }
        public int IsActive1 { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int RentPaid { get; set; }
        public double Payment { get; set; }
        public string PaymentType { get; set; }
        public string StatusMessage { get; set; }
        public DateTime? RentForMonthStartDate { get; set; }
        public DateTime? RentForMonthEndDate { get; set; }
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
        public virtual List<TenantPaymentDetail> TenantPaymentDetails { get; set; }
        public virtual List<PaymentDetail> PaymentDetails { get; set; }
    }

    public class QueryResponse
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }

    public class PGRoomData
    {
        public int PGRoomInventoryId { get; set; }
        public string Pid { get; set; }
        public int PropertyId { get; set; }
        public int RoomNo { get; set; }
        public int AccountId {  get; set; } 
        public string RoomType { get; set; }
        public string FloorNo { get; set; }
        public string RoomTenantDetails { get; set; }
        public float Occupancy { get; set; } 
        public float OccupancyC { get; set; }
        public int Person { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class TanentStayDetails
    {
        public int TenantId { get; set; }
        public double MonthlyRent { get; set; }
        public double SecurityAmount { get; set; }
        public double SecurityDeposit { get; set; }
        public double SecurityAmountDue { get; set; }
        public int PropertyId { get; set; }
        public string OwnerName { get; set; }
        public string OwnerMobile { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RoomNo { get; set; }
 
    }

}
