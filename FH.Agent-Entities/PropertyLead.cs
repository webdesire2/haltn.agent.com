﻿
using System;

namespace FH.User.Entities
{
    public class PropertyLead
    {
        public int LeadId { get; set; }
        public int UserId { get; set; }
        public string LeadName { get; set; }
        public string LeadMobile { get; set; }
        public string LeadSource { get; set; }
        public string Locality { get; set; }
        public int PropertyId { get; set; }
        public string PropertyType { get; set; }
        public string TenantGender { get; set; }
        public DateTime LeadDate { get; set; }
        public DateTime CreatedOn { get; set; }    
        public string VisitorName { get; set; }
        public string VisitorEmail { get; set; }
        public string VisitorMobile { get; set; }
        public string VisitorRequirement { get; set; }
        public string VisitDate { get; set; }
        public string VisitTime { get; set; }
        public string LeadStatus { get; set; }        
        public Nullable<DateTime> LeadCreatedOn { get; set; }
    }
}
