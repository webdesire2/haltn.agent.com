﻿
using System.Collections.Generic;

namespace FH.User.Entities
{
    public class PGDetail
    {  
        public string TenantGender{ get; set; }
        public string PreferredGuest { get; set; }
        public string AvailableFrom { get; set; }
        public bool Fooding { get; set; }       
        public bool Laundry { get; set; }
        public bool RoomCleaning { get; set; }        
        public string Description { get; set; }   
        public bool IsAgentAllowed { get; set; }  
    }
}
