﻿
namespace FH.User.Entities
{
    public class PropertyFurnishingDetails
    {
        // public int FurnishingDetailId { get; set; }
        public int PropertyId { get; set; }
        public int FurnishingMasterId { get; set; }        
        public string Value { get; set; }       
        
    }
}
