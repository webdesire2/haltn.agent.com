﻿namespace FH.User.Entities
{
   public class PropertySearchParameter
    {
        public string PropertyStatus { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
