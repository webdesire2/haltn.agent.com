﻿
namespace FH.User.Entities
{
    public class PropertyMaster
    {
        public string ContactPersonName { get; set; }
        public string PropertyName { get; set; }
        public string ContactPersonMobile { get; set; }
        public int PropertyId { get; set; }
        public string PropertyType { get; set; }
        public string City { get; set; }
        public string Locality { get; set; }
        public string Street { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PropertyStatusByUser { get; set; }
        public string PropertyStatusByFH { get; set; }
        public int CreatedBy { get; set; }
    }
}
