﻿using System.Collections.Generic;

namespace FH.User.Entities
{
    public class PropertyPG
    {
        public List<PGRoomRentalDetail> RoomRentalDetail { get; set; }
        public IEnumerable<PGRoomAmenity> RoomAmenities { get; set; }
        public PropertyMaster PropertyMaster { get; set; }
        public PGDetail PgDetail { get; set; }
        public IEnumerable<PGRule> pgRules { get; set; }
        public IEnumerable<PropertyGalleryDetails> GalleryDetails { get; set; }
        public IEnumerable<UAccount> UAccount {  get; set; }
        public IEnumerable<PGRoomData> PGRoomData { get; set; } 
        public IEnumerable<RoomAccount> RoomAccount { get; set; }
        public IEnumerable<PropertyAmenitiesDetails> AmenityDetails { get; set; }
    }
    
    public class RoomAccount
    {
        public IEnumerable<PGRoomData> PGRoomData { get; set; }
        public IEnumerable<UAccount> UAccount { get; set; }
    }
}
